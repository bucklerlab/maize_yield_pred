#!/usr/bin/env python
# coding: utf-8

# In[2]:


import os, sys
import pandas as pd
import geopandas
import numpy as np
import urllib.request
from dateutil.parser import parse
from SSURO_soil_data_functions import retrieve_soil_info_by_ID, get_table_headers, get_ssurgo_inventory, impute_data
from fancyimpute import KNN
#much of this script is based on the design found within the FedData R package


# In[2]:


soil_dir="../data/Soil_data/"
os.system("mkdir "+soil_dir+"tmp_soil/")
#load county coordinate data from working phenotype table (produced in script 1)
soil_areas = pd.read_csv("../data/Phenotype_data/work_set_yields_by_county.csv", index_col=0)
soil_areas.drop_duplicates(subset="ID", inplace=True)
soil_areas = soil_areas[["ID","County","State","Latitude","Longitude"]]
soil_areas.dropna(inplace=True)
#place random county from each state at the top of the list so that one county per state is run first as a test
soil_areas = soil_areas.sample(frac=1, random_state=54321)
soil_areas["tmp"] = soil_areas.duplicated("State")
soil_areas = soil_areas.sort_values("tmp").reset_index(drop=True)
#for the moment we only care abotu downloading entire countys (in most cases the same thing as a single 
#SSURGO soil survey area). For that reason we will use an area of 0.001 degreas in all directions around
#the provided county center data. This is just an area which we are confident lies within the county and
#is used to pull down the data for the area of the entire county (assumed here to be the same as the
#soil survey area)
degs_to_add=0.001
soil_areas["North"] = soil_areas["Latitude"]+degs_to_add
soil_areas["South"] = soil_areas["Latitude"]-degs_to_add
soil_areas["East"] = soil_areas["Longitude"]+degs_to_add
soil_areas["West"] = soil_areas["Longitude"]-degs_to_add

#CSV Patrick made containing names of important soil categories
#TabRead = pd.read_csv("../data/General/TableColumnDescriptionsReport.csv", encoding="ISO-8859-1", skiprows=1)
TabRead = pd.read_csv("../data/General/Revised Table Selections Soil Table.csv", encoding="ISO-8859-1", skiprows=1)
#remove tables that we don't care about
TabRead = TabRead[TabRead["Usable"]=="yes"]
desired_tables = TabRead["Table Physical Name"].unique().tolist()
#import SSURGO table connections
table_relationships = pd.read_csv("../data/General/SSURGO_table_relationships.csv")


# In[82]:


#get table headers
failed = {}
all_processed_soils=[]
tablesHeaders = get_table_headers()
count=0
for ID in soil_areas["ID"].unique()[:45]:
    count+=1
    print(count,"/",len(soil_areas["ID"].unique()), ID)
    #Get SSURGO inventory data for desired area of interest
    try:
        MergedCsv, spatial, tabular, majcomp = retrieve_soil_info_by_ID(ID, soil_dir, soil_areas, table_relationships,
                                                               TabRead, desired_tables, verbose=True)
        if len(MergedCsv)!=0:
            MergedCsv["ID"]=ID
            all_processed_soils.append(MergedCsv)
    except Exception as e:
        print(ID, "failed.  Added to list for future attempts.")
        failed[ID] = str(e)
all_processed_soils = pd.concat(all_processed_soils, sort=True)


# In[85]:


def impute_data(soildata):
    #removing ID column before imputing, giving a label to add later.
    all_processed_soils1a = soildata["ID"]
    soildata = soildata.drop("ID", axis = 1)
    
    #Imputing & relabeling---k=3 was chosen at random
    imputedtable = KNN(k=3).fit_transform(soildata)
    
    #KNN returns as an array, must be turned back into a pandas df.
    imputedtable = pd.DataFrame(imputedtable)
    
    #must convert df to list to return it as a column.
    imputedtable["ID"] = list(all_processed_soils1a)
     
    #Table headers removed with KNN, added back in. 
    imputedtable.columns = ["awc", "caco3", "cec7", "claytotal", "dbovendry", 
                     "ec", "freeiron?", "gypsum", "horizons", "ksat", "om", "partdensity", "ph1to1h2o", "sandtotal", "silttotal", "slope",
                      "ID"]
    return imputedtable


# In[89]:


impute_data(all_processed_soils)


# In[ ]:


all_processed_soils.pivot_table(index=["ID"], values="hzdepthrange", aggfunc=np.count_nonzero)

