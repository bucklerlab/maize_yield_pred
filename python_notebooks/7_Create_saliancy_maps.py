#!/usr/bin/env python
# coding: utf-8

# In[1]:


import pandas as pd
import numpy as np
import os, sys
#'''
os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID" 
os.environ["CUDA_VISIBLE_DEVICES"] = "0"
from tensorflow.keras import backend as K
import gc
import tensorflow as tf
gpus = tf.config.experimental.list_physical_devices('GPU')
'''
if gpus:
  # Restrict TensorFlow to only allocate 1GB of memory on the first GPU
    try:
        tf.config.experimental.set_virtual_device_configuration(
            gpus[0],
            [tf.config.experimental.VirtualDeviceConfiguration(memory_limit=4096)])
        logical_gpus = tf.config.experimental.list_logical_devices('GPU')
        print(len(gpus), "Physical GPUs,", len(logical_gpus), "Logical GPUs")
    except RuntimeError as e:
        print(e)
'''
np_load_old = np.load


# In[2]:


from tf_keras_vis.utils import num_of_gpus

_, gpus = num_of_gpus()
print('{} GPUs'.format(gpus))


# In[3]:


from tf_keras_vis.saliency import Saliency
from tf_keras_vis.utils import normalize

from scipy.stats import pearsonr
from scipy.stats import spearmanr
from scipy.stats import linregress
#from tensorflow.keras.models import model_from_json
import json
from tqdm.notebook import tqdm
import pickle


# In[4]:


def get_performance_states(results):
    performance_stats=[]
    for split in results["split"].unique():
        tmp = results[results["split"]==split].copy()
        if len(tmp)<2:continue
        real = tmp["Observed"]
        PR = tmp["Predicted"]
        prsn = pearsonr(np.array(real), np.array(PR))
        sprmn = spearmanr(np.array(real), np.array(PR))
        prsn = prsn[0]
        sprmn = sprmn[0]
        slope = linregress(real,PR)[0]
        r2 = prsn**2
        mae = np.mean(np.abs(np.array(PR) - np.array(real)))
        rMAE = mae / real.mean()
        normMAE = mae / (real.max()-real.min())
        RMSE = np.sqrt(np.mean(np.square(np.array(PR) - np.array(real))))
        rRMSE = RMSE / np.mean(np.array(real))
        normRMSE = RMSE/(real.max()-real.min())
        #based on https://en.wikipedia.org/wiki/Root-mean-square_deviation
        performance_stats.append([split, prsn, sprmn, slope, r2, mae, rMAE, normMAE, RMSE, rRMSE, normRMSE, len(PR)])
    performance_stats = pd.DataFrame(performance_stats, columns=["split", "prsn", "sprmn", "slope", "r2", "mae", "rMAE",
                                                                 "normMAE", "RMSE", "rRMSE", "normRMSE","n"])
    return performance_stats

def get_performance_stats_overall(results):
    performance_stats=[]
    tmp = results.copy()
    #if len(tmp)<2:continue
    real = tmp["Observed"]
    PR = tmp["Predicted"]
    prsn = pearsonr(np.array(real), np.array(PR))
    sprmn = spearmanr(np.array(real), np.array(PR))
    prsn = prsn[0]
    sprmn = sprmn[0]
    slope = linregress(real,PR)[0]
    r2 = prsn**2
    mae = np.mean(np.abs(np.array(PR) - np.array(real)))
    rMAE = mae / real.mean()
    normMAE = mae / (real.max()-real.min())
    RMSE = np.sqrt(np.mean(np.square(np.array(PR) - np.array(real))))
    rRMSE = RMSE / np.mean(np.array(real))
    normRMSE = RMSE/(real.max()-real.min())
    #based on https://en.wikipedia.org/wiki/Root-mean-square_deviation
    performance_stats.append([np.nan, prsn, sprmn, slope, r2, mae, rMAE, normMAE, RMSE, rRMSE, normRMSE, len(PR)])
    performance_stats = pd.DataFrame(performance_stats, columns=["split", "prsn", "sprmn", "slope", "r2", "mae", "rMAE",
                                                                 "normMAE", "RMSE", "rRMSE", "normRMSE","n"])
    return performance_stats


# In[5]:


#calculate stats for all sets for a given method
def ensmble_and_rep_stats(results_CNN):
    if len(results_CNN["method"].unique()) > 1:
        print("Error: You have provided results from multiple methods.")
    by_split_rep=[]
    combined_stats=[]
    combined_stats_std=[]
    combined_stats_overall=[]
    combined_stats_sng_reps=[]
    for tset in results_CNN["set"].unique():
        print(tset)
        sng_set = results_CNN[(results_CNN["set"]==tset)].copy()
        #average across reps
        results_sng_pvt = sng_set.pivot_table(index=["split","index"]).reset_index()
        #get performance data
        perf_stats_reps = get_performance_states(results_sng_pvt)
        perf_stats_reps_overall = get_performance_stats_overall(results_sng_pvt[results_sng_pvt["split"]!="S_Historical"])
        combined_stats.append(pd.DataFrame(perf_stats_reps[perf_stats_reps["split"]=="S_Historical"].mean(),
                                           columns=[tset+"_Hist"]))
        combined_stats.append(pd.DataFrame(perf_stats_reps[perf_stats_reps["split"]!="S_Historical"].mean(),
                                           columns=[tset+"_Final"]))
        combined_stats_std.append(pd.DataFrame(perf_stats_reps[perf_stats_reps["split"]=="S_Historical"].std(),
                                           columns=[tset+"_Hist"]))
        combined_stats_std.append(pd.DataFrame(perf_stats_reps[perf_stats_reps["split"]!="S_Historical"].std(),
                                           columns=[tset+"_Final"]))
        
        combined_stats_overall.append(pd.DataFrame(perf_stats_reps_overall[perf_stats_reps_overall["split"]!="S_Historical"].mean(),
                                           columns=[tset+"_Final"]))
        
        #get stats by rep
        for rep in sng_set["rep"].unique():
            #print(rep)
            sng_set_rep = sng_set[sng_set["rep"]==rep].copy()
            perf_stats_sng_rep = get_performance_states(sng_set_rep)
            combined_stats_sng_reps.append(pd.DataFrame(perf_stats_sng_rep[perf_stats_sng_rep["split"]=="S_Historical"].mean(),
                                                        columns=[tset+"_rep"+str(rep)+"_Hist"]))
            combined_stats_sng_reps.append(pd.DataFrame(perf_stats_sng_rep[perf_stats_sng_rep["split"]!="S_Historical"].mean(),
                                                        columns=[tset+"_rep"+str(rep)+"_Final"]))
            perf_stats_sng_rep["set"]=tset
            perf_stats_sng_rep["rep"]=rep
            by_split_rep.append(perf_stats_sng_rep)
    combined_stats = pd.concat(combined_stats, axis=1)
    combined_stats_std = pd.concat(combined_stats_std, axis=1)
    combined_stats_overall = pd.concat(combined_stats_overall, axis=1)
    combined_stats_sng_reps = pd.concat(combined_stats_sng_reps, axis=1)
    mult_ind = pd.DataFrame(combined_stats_sng_reps.columns)
    mult_ind = mult_ind[0].str.split("_", expand=True)
    mult_ind.columns = ["set","rep","model"]
    mult_ind["rep"] = mult_ind["rep"].str.replace("rep","").astype(int)
    combined_stats_sng_reps.columns = pd.MultiIndex.from_frame(mult_ind)
    combined_stats_sng_reps = combined_stats_sng_reps.T.reset_index()
    by_split_rep = pd.concat(by_split_rep)
    return combined_stats, combined_stats_std, combined_stats_sng_reps, by_split_rep, combined_stats_overall


# In[6]:


def readData(method):
    np.load = lambda *a, **k: np_load_old(*a, allow_pickle=True, **k)
    
    if "_G_only_" in method:
        include = ["G"]
    elif "_NO_SOIL_" in method:
        include = ["G", "general", "weather", "fertility"]
    elif "_NO_G_" in method:
        include = ["general", "soil", "weather", "fertility"]
    elif "_NO_WEATH_" in method:
        include = ["G", "general", "soil", "fertility"]
    else:
        include = ["G", "general", "soil", "weather", "fertility"]
    
    #always load phenotypes
    phenotypes=pd.read_csv(dataFolder+"index_file.csv", index_col=0, low_memory=False)
    rowIndexes=phenotypes.index.values
    myyield = phenotypes["BU / ACRE"].values
    
    general = phenotypes[['Year', 'Latitude', 'Longitude', 'Altitude', 'Plant Density']].fillna(-1).values
    
    if "soil" in include:
        soils = np.load(dataFolder+"Soils.npy")
        soils = soils.reshape(soils.shape[0], soils.shape[1] * soils.shape[2])
        if "general" in include:
            ConsData = np.concatenate([soils, general], axis=1)
        else:
            ConsData = soils
    elif "general" in include:
        ConsData = general
    else:
        ConsData = pd.DataFrame()
    
    if "weather" in include:
        weather = np.load(dataFolder+"Weather_season_only.npy")
        weather = np.transpose(weather, (0, 2, 1))
    else:
        weather = pd.DataFrame()
    
    fertility = phenotypes[['% Clay', '% Sand', '% Silt', '%Ca Sat', '%H Sat','%K Sat', '%Mg Sat', '%Na Sat',
                            '1:1 S Salts mmho/cm', '1:1 Soil pH','Calcium ppm Ca', 'Magnesium ppm Mg',
                            'Mehlich P-III ppm P','Nitrate-N ppm N', 'Organic Matter LOI %', 'Potassium ppm K',
                            'Sodium ppm Na', 'Sulfate-S ppm S', 'WDRF Buffer pH', 'lbs N/A', 'Total K lbs/acre',
                            'Total N lbs/acre', 'Total P lbs/acre', 'Irrigation amount (inches)', 'Irrigated']].fillna(-1).values
    if "G" in include:
        pcs = np.load(dataFolder+"PCs.npy")[:,0:30]
        if "fertility" in include:
            pcs = np.concatenate([pcs, fertility], axis=1)
    elif "fertility" in include:
        pcs = fertility
    else:
        pcs = pd.DataFrame()
    
    np.load = np_load_old
    return weather, ConsData, pcs, rowIndexes, myyield, phenotypes


# In[7]:


def get_best_reps_validation(method):
    #upload results from chosen method and use them to determine best replicate(s) for sal maps 
    #find out what files and methods are available
    results_dir="../data/Results/"
    res_file_details = pd.DataFrame([x for x in os.listdir(results_dir) if x[:19]=="Output_predictions_"], columns=["File"])
    res_file_details["file_short"] = res_file_details["File"].str[19:-4]
    res_file_details["method"] = res_file_details["File"].str[19:-4]
    res_file_details["method"] = res_file_details["method"].str.replace("\d+-\d+_","")
    res_file_details["method"] = res_file_details["method"].str.replace("\d+_\d+_","")
    #res_file_details["method"] = res_file_details["method"].str.replace("reps_val_training_","")

    results_files=[]
    for file in res_file_details[res_file_details["method"]==method]["File"].tolist():
        tmp = pd.read_csv(results_dir+file)
        tmp = tmp[tmp["index"]!="index"]
        tmp["split"] = tmp["split"].astype("str")
        tmp["rep"] = tmp["rep"].astype("int")
        #print(file, len(tmp["split"].unique()), len(tmp["rep"].unique()))
        results_files.append(tmp.copy())
    results_CNN = pd.concat(results_files)
    results_CNN = results_CNN.apply(pd.to_numeric, errors='ignore')
    results_CNN["split"] = "S_"+ results_CNN["split"].astype("str")

    #combine replicates from the same experiment
    results_CNN["method"] = results_CNN["method"].str.replace("\d+-\d+_","")
    #results_CNN["method"].unique()
    
    #determine best replicate based on validation set
    model="Final"
    set1="validation"
    #get stats based on validation set
    tmp = results_CNN[(results_CNN["method"]==method) & (results_CNN["set"].isin(["validation"]))].copy()
    combined_stats, combined_stats_std, combined_stats_sng_reps, by_split_rep, combined_stats_overall = ensmble_and_rep_stats(tmp)
    #examine validation stats and determine what to drop (if any)
    subset = combined_stats_sng_reps[(combined_stats_sng_reps["model"]==model) & (combined_stats_sng_reps["set"]==set1)]
    ordered = subset.sort_values("RMSE").reset_index(drop=True)
    #ordered = subset.sort_values("prsn", ascending=False)
    #ordered

    #best_rep = int(ordered["rep"].iloc[0])
    return ordered


# In[9]:


#set input and output details
dataFolder="../data/"

#pick training set scenario desired
pre_file="Historical_"
#pre_file="NO_HIST_"

#prefix="GEM_reps_val_training_"
prefix="reps_val_training_"
#prefix="reps_val_training_NO_HIST_"


#train_test_sets="Train_val_test_sets_13_Dec2019.json"
train_test_sets="Train_val_test_sets_Practical_GEM_26Feb2020.json"
#train_test_sets="Train_val_test_sets_E_dwnSample293_24Apr2020.json"
#train_test_sets="Train_val_test_sets_G_dwnSample12_23Apr2020.json"

method=prefix+train_test_sets[20:-5]
print(method)
#get all model file names from this scenario
models_avil = pd.DataFrame([x for x in os.listdir("../models/") if method in x and
                            #prefix in x and
                            x[:len(pre_file)]==pre_file and
                            x[-3:]==".h5"],
                           columns=["file_name"])

#filter out other unwanted files
models_avil = models_avil[models_avil["file_name"].str.contains(pre_file+"\d")]
models_avil = models_avil[models_avil["file_name"].str.contains("_NoG_")==False]
models_avil["method"]=method
#extract replicate and split
tmp = models_avil["file_name"].str.replace(pre_file+"\d+-\d+_"+method+"_","").copy()
tmp = tmp.str.replace(pre_file+"\d+-\d+_"+"GEM_"+method+"_","")
#models_avil = models_avil[tmp.str[:11] != "Historical_"]
#tmp = tmp[tmp.str[:11] != "Historical_"]
tmp = tmp.str.replace(pre_file+"\d+_\d+_"+method+"_","").str[:-3]
models_avil["rep"] = tmp.str.split("_", expand=True)[0].astype(int)
models_avil["split"] = tmp.str.replace("\d+_","")

#fix splits that are from historical model but are labeled with replicate
#models_avil.loc[models_avil["split"]==models_avil["rep"].astype(str), "split"]="Historical"
models_avil.loc[tmp.str.split("_", expand=True)[1].isnull(),"split"]="Historical"


# In[10]:


print(len(models_avil["split"].unique()), len(models_avil["rep"].unique()),
      len(models_avil["split"].unique())*len(models_avil["rep"].unique()))


# In[18]:


models_avil


# In[11]:


#import data for running saliancy map
#load in data
weather, ConsData, pcs, rowIndexes, myyield, phenotypes = readData(method)
print(weather.shape, ConsData.shape, pcs.shape, myyield.shape)

#get training set indicies
with open(dataFolder+ train_test_sets, "r") as fp:
    sets = json.load(fp)
    print(train_test_sets)


# In[12]:


#upload results from chosen method and use them to determine best replicate(s) for sal maps
ordered = get_best_reps_validation(method)


# In[13]:


def loadModel(name):
    json_file = open("../models/"+name+".json", 'r')
    loaded_model_json = json_file.read()
    json_file.close()
    loaded_model = tf.keras.models.model_from_json(loaded_model_json)
    # load weights into new model
    loaded_model.load_weights("../models/"+name+".h5")
    #print("Loaded model from disk")
    return loaded_model

def get_tensors_sng_set(weather, ConsData, pcs, myyield, train_index):
    if len(weather)==0:
        trainSetweather = weather
    else:
        trainSetweather = weather[train_index, :, :].astype('float32')
    
    if len(ConsData)==0:
        trainSetCons = ConsData
    else:
        trainSetCons = ConsData[train_index, :].astype('float32')
        
    if len(pcs)==0:
        trainsetPCs = pcs
    else:
        trainsetPCs = pcs[train_index,:].astype('float32')
    
    trainYield = myyield[train_index].astype('float32')
    return trainSetweather, trainSetCons, trainsetPCs, trainYield

def get_train_val_test_sets(set_inds, weather, ConsData, pcs, myyield):
    train_index = set_inds["train"]
    val_index = set_inds["val"]
    test_index = set_inds["test"]

    train_tensors = get_tensors_sng_set(weather, ConsData, pcs, myyield, train_index)
    val_tensors = get_tensors_sng_set(weather, ConsData, pcs, myyield, val_index)
    test_tensors = get_tensors_sng_set(weather, ConsData, pcs, myyield, test_index)
    return train_tensors, val_tensors, test_tensors, [train_index, val_index, test_index]


# In[14]:


def run_post_proccess_sal_map(train_tensors, split):
    #setup saliancy algorithum
    saliency = Saliency(model, clone=False)

    def loss(output):
        return(output)
    
    #create saliancy map based on inputs.  Because there are 3 input tensors this will result in a list of 3 output arrays
    if split == "Historical":
        saliency_map = saliency(loss, train_tensors[:2], keepdims=True)
    else:
        saliency_map = saliency(loss, train_tensors[:3], keepdims=True)
    #print(train_tensors[0].shape, train_tensors[1].shape, train_tensors[2].shape)
    #print(saliency_map[0].shape, saliency_map[1].shape, saliency_map[2].shape)

    #post process salmap outputs
    sal_weather = np.array(saliency_map[0])
    sal_cons = np.array(saliency_map[1])
    if split == "Historical":
        sal_pcs = pd.DataFrame()
    else:
        sal_pcs = np.array(saliency_map[2])
    
    return sal_weather, sal_cons, sal_pcs

def perp_sal_weather(sal_weather, rep, split, tset, inds):
    #format and prepare weather data
    #reshape to make flat file for easy saving and manipulation
    if len(sal_weather)==0:
        return sal_weather
    sal_weather_flat = pd.DataFrame(sal_weather.reshape(sal_weather.shape[0], sal_weather.shape[1]*sal_weather.shape[2]))

    #create and add identification information
    id_info = pd.DataFrame(inds, columns=["index"])
    id_info["rep"]=rep
    id_info["split"]=split
    id_info["set"]=tset
    id_info = id_info.drop(columns="index")

    sal_weather_flat = pd.concat([id_info, sal_weather_flat], axis=1)
    return sal_weather_flat


# In[15]:


def run_post_proccess_sal_map_G_only(train_tensors, split, cutoff):
    #setup saliancy algorithum
    saliency = Saliency(model, clone=False)

    def loss(output):
        return(output)
    
    #run saliancy map in batches to conserve memory
    sal_pcs=[]
    for num in range(0, len(train_tensors[2]), cutoff):
        strt = num
        end = num+cutoff
        pcs=train_tensors[2][strt:end]
        #print(strt, end, weath.shape, cons.shape, pcs.shape)

        #run saliency
        t_pcs = saliency(loss, pcs , keepdims=True)
        sal_pcs.append(t_pcs)
    saliency_map = np.concatenate(sal_pcs)
    
    #post process salmap outputs
    sal_weather=pd.DataFrame()
    sal_cons=pd.DataFrame()
    sal_pcs = np.array(saliency_map)
    
    return sal_weather, sal_cons, sal_pcs


# In[16]:


def run_post_proccess_sal_map_NO_WEATH(train_tensors, split, cutoff):
    #setup saliancy algorithum
    saliency = Saliency(model, clone=False)

    def loss(output):
        return(output)
    
    #run saliancy map in batches to conserve memory
    sal_cons=[]
    sal_pcs=[]
    for num in range(0, len(train_tensors[1]), cutoff):
        strt = num
        end = num+cutoff
        cons=train_tensors[1][strt:end]
        pcs=train_tensors[2][strt:end]
        #print(strt, end, weath.shape, cons.shape, pcs.shape)

        #run saliency
        t_cons, t_pcs = saliency(loss, [cons , pcs] , keepdims=True)
        sal_cons.append(t_cons)
        sal_pcs.append(t_pcs)
    saliency_map = [np.concatenate(sal_cons),np.concatenate(sal_pcs)]
    
    #print(train_tensors[0].shape, train_tensors[1].shape, train_tensors[2].shape)
    #print(saliency_map[0].shape, saliency_map[1].shape, saliency_map[2].shape)

    #post process salmap outputs
    sal_weather = pd.DataFrame()
    sal_cons = np.array(saliency_map[0])
    sal_pcs = np.array(saliency_map[1])
    
    return sal_weather, sal_cons, sal_pcs


# In[17]:


def run_post_proccess_sal_map(train_tensors, split, cutoff, method):
    #setup saliancy algorithum
    if "_G_only_" in method:
        sal_weather, sal_cons, sal_pcs = run_post_proccess_sal_map_G_only(train_tensors, split, cutoff)
        return sal_weather, sal_cons, sal_pcs
    
    if "_NO_WEATH_" in method:
        sal_weather, sal_cons, sal_pcs = run_post_proccess_sal_map_NO_WEATH(train_tensors, split, cutoff)
        return sal_weather, sal_cons, sal_pcs
    
    
    saliency = Saliency(model, clone=False)

    def loss(output):
        return(output)
    
    #create saliancy map based on inputs.  Because there are 3 input tensors this will result in a list of 3 output arrays
    #if split == "Historical":
    #    saliency_map = saliency(loss, train_tensors[:2], keepdims=True)
    #else:
    #    saliency_map = saliency(loss, train_tensors[:3], keepdims=True)
    
    #run saliancy map in batches to conserve memory
    sal_weath=[]
    sal_cons=[]
    sal_pcs=[]
    for num in range(0, len(train_tensors[0]), cutoff):
        strt = num
        end = num+cutoff
        weath=train_tensors[0][strt:end]
        cons=train_tensors[1][strt:end]
        pcs=train_tensors[2][strt:end]
        #print(strt, end, weath.shape, cons.shape, pcs.shape)

        #run saliency
        t_weath, t_cons, t_pcs = saliency(loss, [weath, cons , pcs] , keepdims=True)
        sal_weath.append(t_weath)
        sal_cons.append(t_cons)
        sal_pcs.append(t_pcs)
    saliency_map = [np.concatenate(sal_weath),np.concatenate(sal_cons),np.concatenate(sal_pcs)]
    
    #print(train_tensors[0].shape, train_tensors[1].shape, train_tensors[2].shape)
    #print(saliency_map[0].shape, saliency_map[1].shape, saliency_map[2].shape)

    #post process salmap outputs
    sal_weather = np.array(saliency_map[0])
    sal_cons = np.array(saliency_map[1])
    if split == "Historical":
        sal_pcs = pd.DataFrame()
    else:
        sal_pcs = np.array(saliency_map[2])
    
    return sal_weather, sal_cons, sal_pcs


# In[18]:


def summarize_sal_data(split_rep_weather):
    #find average, std, median, min, and max of each split and save that data only
    #split_rep_weather["index"] = split_rep_weather["index"].astype(str)
    if len(split_rep_weather)==0:
        return [pd.DataFrame()]*5
    else:
        mean = split_rep_weather.pivot_table(index=["set","rep","split"]).reset_index()
        std = split_rep_weather.pivot_table(index=["set","rep","split"], aggfunc=np.std).reset_index()
        median = split_rep_weather.pivot_table(index=["set","rep","split"], aggfunc=np.median).reset_index()
        minimum = split_rep_weather.pivot_table(index=["set","rep","split"], aggfunc=np.min).reset_index()
        maximum = split_rep_weather.pivot_table(index=["set","rep","split"], aggfunc=np.max).reset_index()
        return [mean, std, median, minimum, maximum]


# In[19]:


#model = loadModel("../models/Historical_0-16_reps_val_training_NO_G_HIST_G_dwnSample12_23Apr2020_0_1")
#model.summary()


# In[20]:


#find saliancy map values for the best 10 reps (as used in reported results) and accross all splits
cutoff=10000
sum_sal_weather=[]
sum_sal_cons=[]
sum_sal_pcs=[]
for rep in tqdm(ordered.iloc[:10]["rep"]):
    for split in tqdm(models_avil[models_avil["rep"]==rep]["split"]):
        if split=="Historical":continue #running historical is too large for GPU, need to add ability to process in smaller chuncks
        #import keras model
        K.clear_session()
        saved_model_file = models_avil.loc[(models_avil["rep"]==rep) & (models_avil["split"]==split), "file_name"].iloc[0][:-3]
        model = loadModel("../models/"+saved_model_file)
        #get training and testing tensors
        train_tensors, val_tensors, test_tensors, indices = get_train_val_test_sets(sets[split], weather, ConsData,
                                                                                    pcs, myyield)

        #run sal maps for all data sets
        split_rep_weather=[]
        split_rep_cons=[]
        split_rep_pcs=[]
        for tset in ["train","val","test"]:
            #print(tset)
            if tset=="train":
                sal_weather, sal_cons, sal_pcs = run_post_proccess_sal_map(train_tensors,split, cutoff, method)
                inds = indices[0]
            elif tset=="val":
                sal_weather, sal_cons, sal_pcs = run_post_proccess_sal_map(val_tensors,split, cutoff, method)
                inds = indices[1]
            elif tset=="test":
                sal_weather, sal_cons, sal_pcs = run_post_proccess_sal_map(test_tensors,split, cutoff, method)
                inds = indices[2]

            #create identification information
            id_info = pd.DataFrame(inds, columns=["index"])
            id_info["rep"]=rep
            id_info["split"]=split
            id_info["set"]=tset
            id_info = id_info.drop(columns="index")
            
            split_rep_weather.append(perp_sal_weather(sal_weather, rep, split, tset, inds).copy())
            
            if len(sal_cons)==0:
                split_rep_cons.append(sal_cons)
            else:
                split_rep_cons.append(pd.concat([id_info.copy(),pd.DataFrame(sal_cons)], axis=1))
            if split != "Historical":
                split_rep_pcs.append(pd.concat([id_info.copy(),pd.DataFrame(sal_pcs)], axis=1))
            
        #concatenate and process data at the end of every split in order to avoid memory issues.
        split_rep_weather = pd.concat(split_rep_weather).reset_index(drop=True)
        split_rep_cons = pd.concat(split_rep_cons).reset_index(drop=True)
        split_rep_pcs = pd.concat(split_rep_pcs).reset_index(drop=True) 
        #data is too large to keep all in memory or save in a timly mannor. Better to process as desired and keep only results
        sum_sal_weather.append(summarize_sal_data(split_rep_weather))
        sum_sal_cons.append(summarize_sal_data(split_rep_cons))
        sum_sal_pcs.append(summarize_sal_data(split_rep_pcs))


# In[22]:


def post_process_sal_stats(sum_sal_cons):
    sal_cons_out=[]
    for stat in range(0,len(sum_sal_cons[0])):
        tmp_cons=[]
        for split_rep in range(0, len(sum_sal_cons)):
            #print(stat, split_rep)
            tmp_cons.append(sum_sal_cons[split_rep][stat])
        sal_cons_out.append(pd.concat(tmp_cons))
    return sal_cons_out


# In[23]:


#post process and save results
sal_weath_out = post_process_sal_stats(sum_sal_weather)
sal_cons_out = post_process_sal_stats(sum_sal_cons)
sal_pcs_out = post_process_sal_stats(sum_sal_pcs)
print(sal_weath_out[0].shape, sal_cons_out[0].shape, sal_pcs_out[0].shape)


# In[24]:


sal_summary_stat_file = method+"_sal_sammary_stats.p"
print(sal_summary_stat_file)

pickle.dump([sal_weath_out, sal_cons_out, sal_pcs_out], open(dataFolder+sal_summary_stat_file, "wb" ))


# In[ ]:





# In[ ]:




