#!/usr/bin/env python
# coding: utf-8

# In[1]:


import os, sys
import pandas as pd
import numpy as np
from scipy.stats import pearsonr
import seaborn as sns; sns.set_theme()
import matplotlib.pyplot as plt
#scipy.stats.pearsonr


# In[2]:


#Bring in all data except PCs

dataFolder = "../data/"
#np.load = lambda *a, **k: np_load_old(*a, allow_pickle=True, **k)
    
soils = np.load(dataFolder+"Soils.npy")
#average across soil horizons
soils = pd.DataFrame(soils.mean(axis=1), columns=['awc', 'caco3', 'cec7', 'claytotal', 'dbovendry', 'ec', 
                                                  'gypsum', 'ksat', 'om', 'ph1to1h2o', 'sandtotal', 'silttotal','slope'])#.shape
#soils = soils.reshape(soils.shape[0], soils.shape[1] * soils.shape[2])
    
weather = np.load(dataFolder+"Weather_season_only.npy")
#weather = np.transpose(weather, (0, 2, 1))

phenotypes=pd.read_csv(dataFolder+"index_file.csv", index_col=0, low_memory=False)
rowIndexes=phenotypes.index.values
    #myyield = phenotypes["BU / ACRE"].values
    
    #for historical data we want to include 'Year', 'Latitude', 'Longitude', 'Altitude', 'Irrigated', 'Plant Density'
general = phenotypes[['Year', 'Latitude', 'Longitude', 'Altitude', 'Plant Density']]#.fillna(-1)
    #put together for output
    #ConsData = np.concatenate([soils, general], axis=1)
    
fertility = phenotypes[['% Clay', '% Sand', '% Silt', '%Ca Sat', '%H Sat','%K Sat', '%Mg Sat', '%Na Sat',
                            '1:1 S Salts mmho/cm', '1:1 Soil pH','Calcium ppm Ca', 'Magnesium ppm Mg',
                            'Mehlich P-III ppm P','Nitrate-N ppm N', 'Organic Matter LOI %', 'Potassium ppm K',
                            'Sodium ppm Na', 'Sulfate-S ppm S', 'WDRF Buffer pH', 'lbs N/A', 'Total K lbs/acre',
                            'Total N lbs/acre', 'Total P lbs/acre', 'Irrigation amount (inches)', 'Irrigated']]#.fillna(-1)

print(soils.shape, weather.shape, general.shape, fertility.shape)


# In[3]:


#thought it might make more sense to drop duplicates before performing correlation but decided not to
def pearson_corr(x1,x2):
    #print(x1,x2)
    tmp = pd.DataFrame(x1,x2).reset_index().drop_duplicates()
    x1 = tmp.iloc[:,0]
    x2 = tmp.iloc[:,1]
    r = pearsonr(x1,x2)[0]
    return r


# In[4]:


weath_avg = weather.mean(axis=2)
weath_avg = pd.DataFrame(weath_avg, columns= ["Day of year", "Day Length", "Maximum Temperature", "Minimum Temperature",
                                              "Precipitation", "Radiation","Vapor Pressure", "Cumulative thermal time"])

all_avg = pd.concat([general, soils, fertility, weath_avg], axis=1)
all_avg_corr = all_avg.corr()
#all_avg_corr = all_avg.corr(method=pearson_corr)


# In[5]:


def create_heatmap(all_avg_corr, cent_zero):
    cols_sorted = all_avg_corr.mean().sort_values(ascending=False).index.to_list()
    corr = all_avg_corr.loc[cols_sorted][cols_sorted].copy()
    
    fig, ax = plt.subplots(figsize=(16,15))
    mask = np.zeros_like(corr)
    mask[np.triu_indices_from(mask)] = True
    with sns.axes_style("white"):
        if cent_zero:
            ax = sns.heatmap(corr,mask=mask, square=True, center=0)
        else:
            ax = sns.heatmap(corr,mask=mask, square=True)


# In[6]:


#create_heatmap(all_avg_corr, cent_zero=True)


# In[7]:


create_heatmap(all_avg_corr.abs(), cent_zero=False)


# In[12]:


#create ordered list of correlations
def create_ordered_corr_list(all_avg_corr, cut_off = 0.5):
    corr_list = all_avg_corr.stack().reset_index()
    corr_list.columns = ["Feature1","Feature2","Correlation"]
    corr_list["Abs_corr"] = corr_list["Correlation"].abs()
    corr_list = corr_list[corr_list["Feature1"]!=corr_list["Feature2"]] #remove comparisons to self
    #remove duplicates
    corr_list = corr_list[corr_list.apply(lambda x: '-'.join(sorted([x['Feature1'],x['Feature2']])),axis=1).duplicated()]
    corr_list = corr_list.sort_values("Abs_corr", ascending=False).reset_index(drop=True)
    high_corrs = corr_list[corr_list["Abs_corr"]>=cut_off].copy()
    return corr_list, high_corrs


# In[15]:


corr_list, high_corrs = create_ordered_corr_list(all_avg_corr, cut_off = 0.5)
corr_list.to_csv("../data/Results/Feature_correlations_all.csv")


# In[ ]:





# In[26]:


#create correlations for just G2F data
g2f_avg = all_avg.loc[phenotypes[phenotypes["G2F"]].index.to_list()].copy()
g2f_avg_corr = g2f_avg.corr()


# In[27]:


create_heatmap(g2f_avg_corr.abs(), cent_zero=False)


# In[28]:


corr_list, high_corrs = create_ordered_corr_list(g2f_avg_corr, cut_off = 0.5)
corr_list.to_csv("../data/Results/Feature_correlations_G2F.csv")
high_corrs


# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:





# In[21]:


soils_simple = pd.read_csv("../data/Soil_data/G2F_all_soils_no_impute.csv", index_col=0)
soils_simple.drop(columns=["horizons"]).corr()


# In[50]:


ax = sns.heatmap(all_but_weath_corr.loc[cols_sorted])


# In[8]:


soils_simple.drop(columns=["horizons"]).corr()


# In[191]:


ax = sns.heatmap(soils.drop(columns=["horizons"]).corr())#, annot=True)


# In[ ]:





# In[55]:


weath = pd.read_csv("../data/Weather_data/G2F_all_met_data_365.csv", index_col=0)
weath["ID_Year"]=weath["ID"]


# In[56]:


weath = weath[weath["year"].isin([2014,2015,2016,2017])]
weath = weath[weath["ID_Year"].str.split("@", expand=True)[1].astype(int) == weath["year"]].reset_index(drop=True)
#weath


# In[137]:


weath["mean"] = weath[[str(x) for x in range(1,366)]].mean(axis=1)
tmp = weath[["ID_Year","level_1","mean"]].copy()
#tmp.index = tmp["level_1"]
#tmp = tmp.reset_index(drop=True)
#tmp.index = pd.MultiIndex.from_arrays([tmp.index.tolist(),tmp["level_1"].tolist()])

tmp = tmp.pivot(index="ID_Year", columns="level_1", values="mean")


# In[138]:


tmp.corr()


# In[183]:


start = 1
end = 365
corr_by_day = []
avg_corr = []
skipped = []
for day in [str(x) for x in range(start,end+1)]:
    corr_tbl = weath[["ID_Year", "level_1", day]].pivot(index="ID_Year", columns="level_1", values=day).corr().copy()
    corr_by_day.append(corr_tbl)
    #print(day)
    if len(corr_tbl.dropna()) != 6:
        skipped.append(day)
        continue
    if day == str(start):
        avg_corr = corr_tbl
    else:
        avg_corr = (avg_corr + corr_tbl)
print(skipped)
avg_corr = avg_corr / (end+1-start-len(skipped))


# In[187]:


avg_corr


# In[189]:


ax = sns.heatmap(avg_corr, annot=True)


# In[ ]:





# In[ ]:


#June 1 = 152
#Oct 1 = 274

