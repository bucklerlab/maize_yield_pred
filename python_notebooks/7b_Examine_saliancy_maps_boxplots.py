#!/usr/bin/env python
# coding: utf-8

# In[1]:


import pandas as pd
import numpy as np
import os, sys

#from tf_keras_vis.utils import normalize

import json
import matplotlib.pyplot as plt
from matplotlib.ticker import FormatStrFormatter
import matplotlib.ticker as ticker
import seaborn as sns
from tqdm.notebook import tqdm
import pickle
import itertools

pd.set_option('display.max_rows', 300)


# In[2]:


def normalize_using_provided_min_max(df_sum, min_val, max_val):
    #normalize based on provided min and max values
    if len(df_sum.shape)>1:
        vals = ((df_sum.iloc[:,3:]-min_val) / (max_val-min_val)).copy()
        normalized = pd.concat([df_sum.iloc[:,:3],vals], axis=1)
    else:
        vals = ((df_sum-min_val) / (max_val-min_val)).copy()
        normalized = vals#pd.concat([df_sum,vals], axis=1)
        
    if normalized.shape != df_sum.shape:
        print("ERROR: number values in does not match number of values out")
    return normalized

def normalize(sal_weather_sum, sal_cons_sum, sal_pcs_sum):
    #normalize across all data
    #find min and max accross all data
    if len(sal_weather_sum.shape)>1:
        all_values = sal_weather_sum.iloc[:,3:].values.flatten().tolist() + sal_cons_sum.iloc[:,3:].values.flatten().tolist() + sal_pcs_sum.iloc[:,3:].values.flatten().tolist()
    else:
        all_values = sal_weather_sum.values.tolist() + sal_cons_sum.values.tolist() + sal_pcs_sum.values.tolist()
    max_val = max(all_values)
    min_val = min(all_values)
    print(max_val, min_val)

    #run normalization
    weath_norm = normalize_using_provided_min_max(sal_weather_sum, min_val, max_val)
    cons_norm = normalize_using_provided_min_max(sal_cons_sum, min_val, max_val)
    pcs_norm = normalize_using_provided_min_max(sal_pcs_sum, min_val, max_val)
    return weath_norm, cons_norm, pcs_norm


# In[3]:


def create_plots_summary_all(sal_weather, sal_soil, sal_field, sal_fert, sal_pcs, normalize):
    #sum data across days for weather
    sal_weather2 = sal_weather.sum(axis=1)
    tmp_ind = sal_weather2.index.tolist()
    sal_weather2.index = pd.MultiIndex.from_arrays([["Weather"]*len(tmp_ind), tmp_ind])

    #sum data across soil layers
    sal_soil2 = sal_soil.sum()
    tmp_ind = sal_soil2.index.tolist()
    sal_soil2.index = pd.MultiIndex.from_arrays([["Soil"]*len(tmp_ind), tmp_ind])

    #put all together in one df
    all_factors = pd.concat([sal_weather2, sal_soil2, sal_field.unstack(), sal_fert.unstack(), sal_pcs.unstack()])
    #normalize for figure
    if normalize:
        min_val = all_factors.min()
        max_val = all_factors.max()
        all_factors = normalize_using_provided_min_max(all_factors, min_val, max_val)
    
    fig, ax = plt.subplots(figsize=(30,5))
    ax = all_factors.sort_values().plot.bar()
    plt.savefig("../figures/"+method+"sal_all_factors_norm"+str(normalize)+".pdf")
    plt.show()
    
    all_factors_sum = pd.Series([sal_weather.values.flatten().sum(), sal_soil.values.flatten().sum(), sal_field.sum()[0],
                                sal_fert.sum()[0], sal_pcs.sum()[0]], index=["Weather", "Soil", "Field", "Fertility", "Genetics"])
    #normalize
    if normalize:
        min_val = all_factors_sum.min()
        max_val = all_factors_sum.max()
        all_factors_sum = normalize_using_provided_min_max(all_factors_sum, min_val, max_val)
    
    fig, ax = plt.subplots(figsize=(30,5))
    ax = all_factors_sum.sort_values().plot.bar()
    plt.savefig("../figures/"+method+"sal_all_factors_sum_norm"+str(normalize)+".pdf")
    plt.show()
    
    all_factors.to_csv("../figures/"+method+"all_factors_norm"+str(normalize)+".csv")
    all_factors_sum.to_csv("../figures/"+method+"all_factors_sum_norm"+str(normalize)+".csv")
    
    return all_factors, all_factors_sum


# In[4]:


def mean_std_all_factors(all_factors, method):
    #take mean and std for each based on rep first then split.
    #calculate mean and std on per split bases
    splits_mean = all_factors.T.reset_index().drop(columns="rep").pivot_table(index=["split"]).T
    splits_std = all_factors.T.reset_index().drop(columns="rep").pivot_table(index=["split"], aggfunc=np.std).T

    #calculate mean and std across all
    all_mean = splits_mean.mean(axis=1)
    all_mean = pd.DataFrame(all_mean, columns=["mean"])
    all_mean["std"] = splits_mean.std(axis=1)
    all_mean["stdErr"] = all_mean["std"]/np.sqrt(len(all_factors.T.reset_index()["split"].unique()))
    all_mean = all_mean.sort_values(["mean"])

    #save data to file
    all_mean.to_csv("../figures/"+method+"_sal_all_factors.csv")

    #create summary of the larger category impacts
    #sum across lower level categories
    tmp = all_factors.copy()
    tmp.reset_index(inplace=True)
    tmp = tmp.pivot_table(index=["level_0"], aggfunc=np.sum)
    tmp.index.name=""
    tmp = tmp.T.reset_index()

    #average within splits
    cat_split_mean = tmp.pivot_table(index=["split"])
    cat_split_std = tmp.pivot_table(index=["split"], aggfunc=np.std)

    #averge across average splits
    cat_mean = pd.DataFrame(cat_split_mean.mean(), columns=["mean"])
    cat_mean["std"] = cat_split_mean.std()
    cat_mean["stdErr"] = cat_mean["std"]/np.sqrt(len(all_factors.T.reset_index()["split"].unique()))
    cat_mean = cat_mean.sort_values(["mean"])

    #save data to file
    cat_mean.to_csv("../figures/"+method+"_sal_sum_large_categories.csv")
    
    return all_mean, cat_mean, splits_mean, splits_std, cat_split_mean, cat_split_std


# In[5]:


def re_formate_and_add_labels(weath_test_sum, cons_test_sum, pcs_test_sum, method):
    
    # Weather
    if len(weath_test_sum)!=0:
        #transpose weather data back to original grid.
        #weather data was previously flatened and needs to be moved back to original shape 
        weather_labels = ["Day of year", "Day Length", "Maximum Temperature", "Minimum Temperature", "Precipitation", "Radiation",
                          "Vapor Pressure", "Cumulative thermal time"]
        sal_weather = pd.DataFrame(weath_test_sum.values.reshape(145,8), columns=weather_labels).T
        #turn columns into days after planting, planting date = 0
        days_aft_planting = [-9,-8,-7,-6,-5,-4,-3,-2,-1]+list(range(0,len(sal_weather.columns)-9))
        sal_weather.columns = days_aft_planting
    else:
        sal_weather=weath_test_sum

    #soil and other concatenated data
    if len(cons_test_sum)!=0:
        soil_labels =  ["awc","caco3","cec7","claytotal","dbovendry","ec","gypsum","ksat","om","ph1to1h2o","sandtotal",
                        "silttotal","slope"]
        #expand soil labels over all three horizons used
        new_labels=[]
        for horizon in ["0cm","15cm","30cm"]:
            #print(horizon)
            new_labels = new_labels+[horizon+"_"+x for x in soil_labels]
        soil_labels = new_labels

        general_labels = ['Year', 'Latitude', 'Longitude', 'Altitude', 'Plant Density'] #general data related to the field/location

        #create average for soils
        if "_NO_SOIL_" in method:
            sal_soil=pd.DataFrame(index=soil_labels)
        else:
            sal_soil = pd.DataFrame(cons_test_sum[:-5].values, index=soil_labels, columns=["Soils"])
            sal_soil.index = sal_soil.index.str.split("_",expand=True)
            sal_soil = sal_soil.unstack()
            sal_soil = sal_soil.droplevel(0, axis=1)

        #create average for gerneral/field data (location specif)
        sal_field = pd.DataFrame(cons_test_sum[-5:].values, index=general_labels, columns=["Field"])
    else:
        sal_soil=cons_test_sum
        sal_field=cons_test_sum
        
    #pcs and G2F fertility data
    if len(pcs_test_sum)!=0:
        sal_pcs = pd.DataFrame(pcs_test_sum[:30].values, index=["PC_"+str(x) for x in list(range(1,31))], columns=["Genetics"])
        #pcs_avg

        fert_labels = ['% Clay', '% Sand', '% Silt', '%Ca Sat', '%H Sat','%K Sat', '%Mg Sat', '%Na Sat',
                                    '1:1 S Salts mmho/cm', '1:1 Soil pH','Calcium ppm Ca', 'Magnesium ppm Mg',
                                    'Mehlich P-III ppm P','Nitrate-N ppm N', 'Organic Matter LOI %', 'Potassium ppm K',
                                    'Sodium ppm Na', 'Sulfate-S ppm S', 'WDRF Buffer pH', 'lbs N/A', 'Total K lbs/acre',
                                    'Total N lbs/acre', 'Total P lbs/acre', 'Irrigation amount (inches)', 'Irrigated']
        if len(pcs_norm) > 30:
            sal_fert = pd.DataFrame(pcs_test_sum[30:].values, index=fert_labels, columns=["Fertility"])
        else:
            sal_fert=pd.DataFrame()
    else:
        sal_pcs=pcs_test_sum
        sal_fert=pcs_test_sum
        
    return sal_weather, sal_soil, sal_field, sal_pcs, sal_fert


# In[6]:


def summarize_in_and_across_splits(sal_cons_sum, tset):
    #return empty data series in input is empty
    if len(sal_cons_sum) == 0:
        return pd.Series(), pd.Series(), pd.Series(), pd.Series()

    #get desitred set
    sngl_set = sal_cons_sum[sal_cons_sum["set"]==tset].copy()
    sngl_set.drop(columns=["set","rep"], inplace=True)

    #summarize set within split using desired statistic
    sngl_set_split_sum = sngl_set.pivot_table(index=["split"], aggfunc=np.mean)
    sngl_set_split_std = sngl_set.pivot_table(index=["split"], aggfunc=np.std)
    
    #summarize accross split
    sngl_set_sum_all = sngl_set_split_sum.mean(axis=0)
    sngl_set_sum_all_std = sngl_set_split_sum.std(axis=0)
    return sngl_set_sum_all, sngl_set_sum_all_std, sngl_set_split_sum, sngl_set_split_std

def summarize_in_and_across_reps(sal_cons_sum, tset, aggfunct):
    #get desitred set
    sngl_set = sal_cons_sum[sal_cons_sum["set"]==tset].copy()

    #summarize set within reps using desired statistic
    sngl_set_rep_sum = sngl_set.pivot_table(index=["rep"], aggfunc=aggfunct)

    #summarize accross reps
    sngl_set_sum_all = sngl_set_rep_sum.mean(axis=0)

    return sngl_set_sum_all, sngl_set_rep_sum


# In[7]:


def fill_missing_data(sal_weather_sum, idx_info, len_stats, weath_len, fill_value):
    sal_weather_sum=[]
    tmp = pd.DataFrame(fill_value, index=range(0, idx_info.shape[0]), columns=range(0, weath_len-3))
    tmp = pd.concat([idx_info, tmp], axis=1)
    for x in range(0,len_stats):
        sal_weather_sum.append(tmp.copy())
    return sal_weather_sum

def check_fill_missing_data(sal_weather_sum, sal_cons_sum, sal_pcs_sum, fill_value):
    #if data is missing (i.e., run with no weather data) then create a place holder for the data based 
    #on the data that is not missing. Final data shape should look like this (?, 1163) (?, 47) (?, 58)
    weath_len=1163
    cons_len=47
    pcs_len=58
    #first, find one that has data and copy the set,rep, and split info
    if len(sal_weather_sum[0]) !=0:
        idx_info = sal_weather_sum[0][["set","rep","split"]].copy().reset_index(drop=True)
        len_stats = len(sal_weather_sum)
    elif len(sal_cons_sum[0]) !=0:
        idx_info = sal_cons_sum[0][["set","rep","split"]].copy().reset_index(drop=True)
        len_stats = len(sal_cons_sum)
    else:
        idx_info = sal_pcs_sum[0][["set","rep","split"]].copy().reset_index(drop=True)
        len_stats = len(sal_pcs_sum)
    
    #if any are equal to zero then add in NaN or -1 or 0?
    if len(sal_weather_sum[0])==0:
        sal_weather_sum = fill_missing_data(sal_weather_sum, idx_info, len_stats, weath_len, fill_value)
    if len(sal_cons_sum[0])==0:
        sal_cons_sum = fill_missing_data(sal_cons_sum, idx_info, len_stats, cons_len, fill_value)
    if len(sal_pcs_sum[0])==0:
        sal_pcs_sum = fill_missing_data(sal_pcs_sum, idx_info, len_stats, pcs_len, fill_value)
    
    #if any are partial then add to front or back
    if sal_pcs_sum[0].shape[1]==33: #PCs included but not fertility add empty fertility to back
        tmp = pd.DataFrame(fill_value, index=range(0, sal_pcs_sum[0].shape[0]), columns=range(30, pcs_len-3))
        for x in range(0,len(sal_pcs_sum)):
            sal_pcs_sum[x] = pd.concat([sal_pcs_sum[x].reset_index(drop=True),tmp], axis=1)
    
    #this part needs some work to make NO_G function
    #elif sal_pcs_sum[0].shape[1]==28: #PCs not included but fertility is add empty PCs to front
    #    tmp = pd.DataFrame(fill_value, index=range(0, sal_pcs_sum[0].shape[0]), columns=range(0, 30))
    #    for x in range(0,len(sal_pcs_sum)):
    #        sal_pcs_sum[x] = pd.concat([tmp, sal_pcs_sum[x].reset_index(drop=True)], axis=1)
    #        sal_pcs_sum[x].columns = ["set", "rep", "split"] + list(range(0, pcs_len-3))
    return sal_weather_sum, sal_cons_sum, sal_pcs_sum


# In[8]:


def plot_individual_weather_factor(sal_weather, sal_weather_std, factor):
    y = sal_weather.loc[factor].values
    x = sal_weather.columns.values
    y_err = sal_weather_std.loc[factor].values
    fig, ax = plt.subplots()
    ax.plot(x,y)
    ax.fill_between(x, y - y_err, y + y_err, alpha=0.2)
    ax.set_title(factor)
    ax.set_xlabel("Days After Planting")
    ax.set_ylabel("Score")
    plt.show()
#for factor in sal_weather.index:
#    plot_individual_weather_factor(sal_weather, sal_weather_std, factor)


# In[9]:


def create_plots_figures(sal_weather, sal_soil, sal_field, sal_fert, sal_pcs, weath_only=False):
    if len(sal_weather)!=0:
        sns.set(font_scale=2)
        fig, ax = plt.subplots(figsize=(30,4))
        ax = sns.heatmap(sal_weather, cmap='viridis')
        plt.savefig("../figures/"+method+"sal_weather.pdf")
        plt.show()
    
        #sort by largest factor
        ax = sal_weather.loc[sal_weather.sum(axis=1).sort_values(ascending=False).index].T.plot()
        ax.legend(bbox_to_anchor=(1.04, 1))
        ax.set_xlabel("Days After Planting")
        ax.set_ylabel("Score")
        plt.show()
    
    if weath_only==False:
        if len(sal_soil)!=0:
            if sal_soil.shape[1] != 0:
                sns.set(font_scale=2)
                fig, ax = plt.subplots(figsize=(30,2))
                ax = sns.heatmap(sal_soil, cmap='viridis')
                plt.savefig("../figures/"+method+"sal_soil.pdf")
                plt.show()

        if len(sal_field)!=0:
            sns.set(font_scale=2)
            fig, ax = plt.subplots(figsize=(30,3))
            ax = sns.heatmap(sal_field.T, cmap='viridis')
            plt.savefig("../figures/"+method+"sal_field.pdf")
            plt.show()

        if len(sal_fert)!=0:
            sns.set(font_scale=2)
            fig, ax = plt.subplots(figsize=(30,3))
            ax = sns.heatmap(sal_fert.T, cmap='viridis')
            plt.savefig("../figures/"+method+"sal_fert.pdf")
            plt.show()

        if len(sal_pcs)!=0:
            sns.set(font_scale=2)
            fig, ax = plt.subplots(figsize=(30,3))
            ax = sns.heatmap(sal_pcs.T, cmap='viridis')
            plt.savefig("../figures/"+method+"sal_pcs.pdf")
            plt.show()


# In[10]:


def sum_values_within_sample(sal_weather_sum, sal_cons_sum, sal_pcs_sum, method):
    #go through each split/rep in the designated set (train, test, val)
    #1, add labels
    #2, sum across desired values
    #3, record
    #determine set to be used
    tset="train"
    train_weath_sum=pd.DataFrame() #set to empty data frames to start. If data exists for them then it will be added
    train_cons_sum=pd.DataFrame()
    train_pcs_sum=pd.DataFrame()
    if len(sal_weather_sum)!=0:
        train_weath_sum = sal_weather_sum[sal_weather_sum["set"]==tset].copy()
    if len(sal_cons_sum)!=0:
        train_cons_sum = sal_cons_sum[sal_cons_sum["set"]==tset].copy()
    if len(sal_pcs_sum)!=0:
        train_pcs_sum = sal_pcs_sum[sal_pcs_sum["set"]==tset].copy()
    
    all_factors=[]
    for sample in range(0, max([len(train_weath_sum), len(train_cons_sum), len(train_pcs_sum)])):
        #print(sample)
        #add labels
        #if data dosn't exist then send empty df
        weath_sngl_tmp=pd.DataFrame()
        cons_sngl_tmp=pd.DataFrame()
        pc_sngl_tmp=pd.DataFrame()
        if len(sal_weather_sum)!=0: weath_sngl_tmp = train_weath_sum.iloc[sample,3:].copy()
        if len(sal_cons_sum)!=0: cons_sngl_tmp = train_cons_sum.iloc[sample,3:].copy()
        if len(sal_pcs_sum)!=0:pc_sngl_tmp = train_pcs_sum.iloc[sample,3:].copy()

        sngl_weather, sngl_soil, sngl_field, sngl_pcs, sngl_fert = re_formate_and_add_labels(weath_sngl_tmp,
                                                                                             cons_sngl_tmp,
                                                                                             pc_sngl_tmp,
                                                                                             method)

        #print(sngl_weather.shape, sngl_soil.shape, sngl_field.shape, sngl_pcs.shape, sngl_fert.shape)
        #sum across desired values
        #sum data across days for weather
        sngl_weather = sngl_weather.sum(axis=1)
        tmp_ind = sngl_weather.index.tolist()
        sngl_weather.index = pd.MultiIndex.from_arrays([["Weather"]*len(tmp_ind), tmp_ind])

        #sum data across soil layers
        sngl_soil = sngl_soil.sum()
        tmp_ind = sngl_soil.index.tolist()
        sngl_soil.index = pd.MultiIndex.from_arrays([["Soil"]*len(tmp_ind), tmp_ind])

        #don't sum field, fert, or pcs, just unstack
        #put all summed values into single df
        #print(sngl_weather.shape, sngl_soil.shape, sngl_field.shape, sngl_pcs.shape, sngl_fert.shape)
        sngl_all_factors = pd.concat([sngl_weather, sngl_soil, sngl_field.unstack(), sngl_fert.unstack(), sngl_pcs.unstack()])

        #record data in list
        all_factors.append(sngl_all_factors)
    all_factors = pd.concat(all_factors, axis=1)
    all_factors = all_factors.astype('float').copy()
    all_factors.columns = pd.MultiIndex.from_frame(train_cons_sum[["rep","split"]])
    return all_factors


# In[11]:


#save csv files for plots
def save_csv_data_summary(sal_weather, sal_weather_std, sal_weather_sum, name):
    sal_weather_stdErr = sal_weather_std/np.sqrt(len(sal_weather_sum[0]["split"].unique()))
    weath_out = pd.concat([pd.DataFrame(sal_weather.stack(), columns=["mean"]),
                           pd.DataFrame(sal_weather_std.stack(), columns=["std"]),
                           pd.DataFrame(sal_weather_stdErr.stack(), columns=["stdErr"])], axis=1)
    weath_out.to_csv("../figures/"+method+"_"+name+".csv")


# In[12]:


def import_sal_data_by_method(method):

    #load previosly made data for saliancy maps.
    pickle_data = pickle.load(open(dataFolder+method+"_sal_sammary_stats.p","rb"))

    #split pickle_data into weather, constants, and pcs
    sal_weather_sum, sal_cons_sum, sal_pcs_sum = pickle_data
    print(sal_weather_sum[0].shape, sal_cons_sum[0].shape, sal_pcs_sum[0].shape)
    #each of the three lists above contains 5 dataframes of summary statistics (run across split, rep, and set):
    #Mean, Std, Median, Minimum, Maximum

    sal_weather_sum, sal_cons_sum, sal_pcs_sum = check_fill_missing_data(sal_weather_sum, sal_cons_sum, sal_pcs_sum, fill_value=0)
    print(sal_weather_sum[0].shape, sal_cons_sum[0].shape, sal_pcs_sum[0].shape)

    #summarize within training set accross replicates
    #sngl_set_sum_all, sngl_set_sum_all_std, sngl_set_split_sum, sngl_set_split_std
    weath_train_sum, weath_train_sum_std, weath_train_splits, weath_train_splits_std = summarize_in_and_across_splits(sal_weather_sum[0], tset="train")
    cons_train_sum, cons_train_sum_std, cons_train_splits, cons_train_splits_std = summarize_in_and_across_splits(sal_cons_sum[0], tset="train")
    pcs_train_sum, pcs_train_sum_std, pcs_train_splits, pcs_train_splits_std = summarize_in_and_across_splits(sal_pcs_sum[0], tset="train")
    print(weath_train_splits.shape, cons_train_splits.shape, pcs_train_splits.shape)
    
    return weath_train_splits, cons_train_splits, pcs_train_splits


# In[13]:


def divide_and_label_data_keeping_splits(weath_train_splits, cons_train_splits, pcs_train_splits):
    #setup weather summed accross all days
    weather_labels = ["Day of year", "Day Length", "Maximum Temperature", "Minimum Temperature", "Precipitation", "Radiation",
                              "Vapor Pressure", "Cumulative thermal time"]
    sal_weath = weath_train_splits.copy()
    sal_weath.columns = weather_labels*145
    sal_weath = sal_weath.T.reset_index().pivot_table(index=["index"], aggfunc=sum).T
    sal_weath.index.name = "Weather"
    
    #setup soils
    soil_labels =  ["awc","caco3","cec7","claytotal","dbovendry","ec","gypsum","ksat","om","ph1to1h2o","sandtotal",
                            "silttotal","slope"]
    #expand soil labels over all three horizons used
    new_labels=[]
    for horizon in ["0cm","15cm","30cm"]:
        #print(horizon)
        new_labels = new_labels+[horizon+"_"+x for x in soil_labels]
    soil_labels = new_labels

    sal_soil = cons_train_splits.iloc[:,:-5].copy() #, index=soil_labels, columns=["Soils"])
    sal_soil.columns = soil_labels
    sal_soil.index.name = "Soil"
    #sal_soil_sum = sal_soil.sum(axis=1)

    #setup field
    general_labels = ['Year', 'Latitude', 'Longitude', 'Altitude', 'Plant Density'] #general data related to the field/location
    sal_field = cons_train_splits.iloc[:,-5:].copy()
    sal_field.columns = general_labels
    gen_labels = ['Total K lbs/acre', 'Total N lbs/acre', 'Total P lbs/acre', 'Irrigation amount (inches)', 'Irrigated']
    sal_field2 = pcs_train_splits.iloc[:,-5:].copy()
    sal_field2.columns = gen_labels
    sal_field = pd.concat([sal_field, sal_field2], axis=1)
    sal_field.index.name = "Field"
    
    #setup fertility
    fert_labels = ['% Clay', '% Sand', '% Silt', '%Ca Sat', '%H Sat','%K Sat', '%Mg Sat', '%Na Sat',
                                        '1:1 S Salts mmho/cm', '1:1 Soil pH','Calcium ppm Ca', 'Magnesium ppm Mg',
                                        'Mehlich P-III ppm P','Nitrate-N ppm N', 'Organic Matter LOI %', 'Potassium ppm K',
                                        'Sodium ppm Na', 'Sulfate-S ppm S', 'WDRF Buffer pH', 'lbs N/A']
    sal_fert = pcs_train_splits.iloc[:,30:-5].copy()
    sal_fert.columns = fert_labels
    sal_fert.index.name = "Fertility"
    
    #setup pcs
    sal_pcs = pcs_train_splits.iloc[:,:30].copy()
    sal_pcs.columns = [x for x in range(1,len(sal_pcs.columns)+1)]
    sal_pcs.index.name = "Genetics"
    
    return sal_weath, sal_soil, sal_field, sal_fert, sal_pcs


# In[14]:


def convert_to_percentage(main_cats_splts):
    #convert to percentage of total
    sum_all = main_cats_splts.sum(axis=1).copy()
    for col in main_cats_splts.columns:
        main_cats_splts[col] = main_cats_splts[col] / sum_all
    return main_cats_splts

def sum_and_devide_for_for_perct(sal_weath, sal_soil, sal_field, sal_fert, sal_pcs):
    #create boxplots for sal map sums averaged within splits
    main_cats_splts = []
    for tmp in [sal_weath, sal_soil, sal_field, sal_fert, sal_pcs]:
        tmp1 = tmp.copy()
        tmp1 = tmp1.sum(axis=1)
        tmp1.name = tmp1.index.name
        main_cats_splts.append(tmp1.copy())
    main_cats_splts = pd.DataFrame(main_cats_splts).T
    main_cats_splts = convert_to_percentage(main_cats_splts)
    return main_cats_splts


def create_box_plot(main_cats_splts, figsize=(8,2), tick_space=0.1):
    col_order = main_cats_splts.mean().sort_values().index.to_list()
    fig, ax = plt.subplots(figsize=figsize)
    sns.boxplot(ax=ax, data=main_cats_splts, order=col_order)
    for i,box in enumerate(ax.artists):
        box.set_edgecolor('black')
        box.set_facecolor('gray')
    ax.set_xlabel("")
    ax.set_ylabel("Proportion of Total", fontsize=12, color="black")
    for label in (ax.get_xticklabels() + ax.get_yticklabels()):
        label.set_fontsize(12)
        label.set_color("black")
    ax.set_title(method)
    ax.set(facecolor = "white")
    #ax.yaxis.set_major_formatter(FormatStrFormatter('%.1f'))
    ax.yaxis.set_major_locator(ticker.MultipleLocator(tick_space))
    #ax.grid(color="lightgray", axis='y')
    plt.setp(ax.artists, edgecolor = 'k', facecolor='gray')
    plt.setp(ax.lines, color='k')
    plt.setp(ax.get_xticklabels(), ha="right", rotation=45)
    
    #make all have same scale
    plt.ylim(0, 0.55)
    plt.show()
    #return ticks


# In[ ]:





# In[15]:


#set input and output details
dataFolder="../data/"
#avail = pd.DataFrame([x for x in os.listdir(dataFolder) if x[-20:]=="_sal_sammary_stats.p"], columns=["file_name"])
#avail["details"] = avail["file_name"].str.split("reps_val_training_", expand=True)[1].str[:-20]
#avail[["details","file_name"]].sort_values(["details"])


# In[16]:


#####Box plots by major catigories#####

prefix="reps_val_training_"
train_test_sets="Train_val_test_sets_Practical_GEM_26Feb2020.json"
method=prefix+train_test_sets[20:-5]

weath_train_splits, cons_train_splits, pcs_train_splits = import_sal_data_by_method(method)
sal_weath, sal_soil, sal_field, sal_fert, sal_pcs = divide_and_label_data_keeping_splits(weath_train_splits, cons_train_splits, pcs_train_splits)
print(sal_weath.shape, sal_soil.shape, sal_field.shape, sal_fert.shape, sal_pcs.shape)

main_cats_splts = sum_and_devide_for_for_perct(sal_weath, sal_soil, sal_field, sal_fert, sal_pcs)
create_box_plot(main_cats_splts, figsize=(8,4))
print(main_cats_splts)

prefix="reps_val_training_"
train_test_sets="Train_val_test_sets_NO_HIST_Practical_GEM_26Feb2020.json"
method=prefix+train_test_sets[20:-5]

weath_train_splits, cons_train_splits, pcs_train_splits = import_sal_data_by_method(method)
sal_weath, sal_soil, sal_field, sal_fert, sal_pcs = divide_and_label_data_keeping_splits(weath_train_splits, cons_train_splits, pcs_train_splits)
print(sal_weath.shape, sal_soil.shape, sal_field.shape, sal_fert.shape, sal_pcs.shape)

main_cats_splts = sum_and_devide_for_for_perct(sal_weath, sal_soil, sal_field, sal_fert, sal_pcs)
create_box_plot(main_cats_splts, figsize=(8,4))
print(main_cats_splts)


# In[17]:


#####create combined weather box plots#####

prefix="reps_val_training_"
train_test_sets="Train_val_test_sets_Practical_GEM_26Feb2020.json"
method=prefix+train_test_sets[20:-5]

weath_train_splits, cons_train_splits, pcs_train_splits = import_sal_data_by_method(method)
sal_weath, sal_soil, sal_field, sal_fert, sal_pcs = divide_and_label_data_keeping_splits(weath_train_splits, cons_train_splits, pcs_train_splits)
#print(sal_weath.shape, sal_soil.shape, sal_field.shape, sal_fert.shape, sal_pcs.shape)

prefix="reps_val_training_"
train_test_sets="Train_val_test_sets_NO_HIST_Practical_GEM_26Feb2020.json"
method=prefix+train_test_sets[20:-5]

weath_train_splits, cons_train_splits, pcs_train_splits = import_sal_data_by_method(method)
sal_weath_NO_hist, sal_soil, sal_field, sal_fert, sal_pcs = divide_and_label_data_keeping_splits(weath_train_splits, cons_train_splits, pcs_train_splits)
#print(sal_weath.shape, sal_soil.shape, sal_field.shape, sal_fert.shape, sal_pcs.shape)

#get percentage for each
sal_weath_pct = convert_to_percentage(sal_weath)
sal_weath_NO_hist_pct = convert_to_percentage(sal_weath_NO_hist)

sal_weath_pct = sal_weath_pct.stack().reset_index()
sal_weath_pct["Hist"]="Historical"

sal_weath_NO_hist_pct = sal_weath_NO_hist_pct.stack().reset_index()
sal_weath_NO_hist_pct["Hist"]="No Historical"

col_order = sal_weath.mean().sort_values().index
fig, ax = plt.subplots(figsize=(10,6))
sns.boxplot(ax=ax, x="index", y=0, hue="Hist", order=col_order, data= pd.concat([sal_weath_pct, sal_weath_NO_hist_pct]))
ax.set_ylabel("Proportion of Total Weather", fontsize=12, color="black")
ax.set_xlabel("")
for label in (ax.get_xticklabels() + ax.get_yticklabels()):
    label.set_fontsize(12)
    label.set_color("black")
num_cols = len(col_order) #number of labels across the bottm
hatches = itertools.cycle(['///', '\\\\\\']) #, '---', '|||', '////', '*', 'o', 'O', '.', '+']) # patterns to use
for i,box in enumerate(ax.artists):
    hatch = next(hatches)
    #print(i, i % 2, hatch)
    box.set_hatch(hatch)
for i, bar in enumerate(ax.patches):  #go through each bar and add appropriate patch
    hatch = next(hatches)
    bar.set_hatch(hatch)
ax.set(facecolor = "white")
ax.legend(bbox_to_anchor=(1.04, 1))
#ax.yaxis.set_major_locator(ticker.MultipleLocator(tick_space))
plt.setp(ax.artists, edgecolor = 'k')
plt.setp(ax.lines, color='k')
plt.setp(ax.get_xticklabels(), ha="right", rotation=45)
plt.show()


# In[18]:


sal_weath_pct.pivot_table(index=["index"])


# In[19]:


sal_weath_NO_hist_pct.pivot_table(index=["index"])


# In[20]:


def prop_of_total_plot(avg_sum_large_cats, title, figsize=(8,2),
                       error_kw={"elinewidth":2, "capsize":10, "capthick":1.5}, fmt='%.1f'):
    #sns.set(font_scale=3)
    #create plot as proportion of total.
    cat_mean_percnt = (avg_sum_large_cats/avg_sum_large_cats.sum()["mean"])
    cat_mean_percnt = cat_mean_percnt.sort_values("mean")
    #ax = cat_mean_percnt.plot.bar(y="mean", yerr="stdErr", figsize=(30,5), error_kw={"elinewidth":5, "capsize":30, "capthick":5},
    #                              legend=False)#, color=plt.rcParams['axes.prop_cycle'].by_key()['color'][:5])
    ax = cat_mean_percnt.plot.bar(y="mean", yerr="stdErr", 
                                  error_kw=error_kw,
                                  legend=False, edgecolor='black',
                                  color="gray", figsize=figsize)
    ax.set_ylabel("Proportion of Total")
    ax.set_xlabel("")
    ax.set_title(title)
    ax.set(facecolor = "white")
    #ax.grid(color="lightgray")
    ax.yaxis.set_major_formatter(FormatStrFormatter(fmt))
    plt.show()
    #print(cat_mean_percnt)
    return cat_mean_percnt


# In[21]:


####combined bar plots of for all internal categories

prefix="reps_val_training_"
train_test_sets="Train_val_test_sets_Practical_GEM_26Feb2020.json"
method=prefix+train_test_sets[20:-5]

weath_train_splits, cons_train_splits, pcs_train_splits = import_sal_data_by_method(method)
sal_weath, sal_soil, sal_field, sal_fert, sal_pcs = divide_and_label_data_keeping_splits(weath_train_splits, cons_train_splits, pcs_train_splits)
sal_weath.columns.name="level_1"
#print(sal_weath.shape, sal_soil.shape, sal_field.shape, sal_fert.shape, sal_pcs.shape)
#combine sal_soil across depths
sal_soil.columns = sal_soil.columns.str.split("_",expand=True)
sal_soil = sal_soil.unstack().reset_index().pivot_table(index=["level_1"], columns="Soil", values=0, aggfunc=sum).T

all_factors_mean = []
all_factors_std = []
for df in [sal_weath, sal_soil, sal_field, sal_fert, sal_pcs]:
    all_factors_mean.append(pd.DataFrame(df.mean(), columns=[df.index.name]).unstack().reset_index())
    all_factors_std.append(pd.DataFrame(df.std(), columns=[df.index.name]).unstack().reset_index())
all_factors_mean = pd.concat(all_factors_mean)
all_factors_mean.columns = ["broad_cat", "specific_cat","mean"]
all_factors_mean.index = all_factors_mean[["broad_cat","specific_cat"]]

all_factors_std = pd.concat(all_factors_std)
all_factors_std.columns = ["broad_cat", "specific_cat","std"]
all_factors_std.index = all_factors_std[["broad_cat","specific_cat"]]
all_factors_mean["std"] = all_factors_std["std"]
all_factors_mean["stdErr"] = all_factors_mean["std"]/np.sqrt(len(sal_weath))

prop_of_total_plot(all_factors_mean[["mean","std","stdErr"]], method,
                                      figsize=(15,3), error_kw={"elinewidth":1, "capsize":2, "capthick":1}, fmt='%.2f')


# In[23]:


prefix="reps_val_training_"
train_test_sets="Train_val_test_sets_NO_HIST_Practical_GEM_26Feb2020.json"
method=prefix+train_test_sets[20:-5]

weath_train_splits, cons_train_splits, pcs_train_splits = import_sal_data_by_method(method)
sal_weath, sal_soil, sal_field, sal_fert, sal_pcs = divide_and_label_data_keeping_splits(weath_train_splits, cons_train_splits, pcs_train_splits)
sal_weath.columns.name="level_1"
#print(sal_weath.shape, sal_soil.shape, sal_field.shape, sal_fert.shape, sal_pcs.shape)
#combine sal_soil across depths
sal_soil.columns = sal_soil.columns.str.split("_",expand=True)
sal_soil = sal_soil.unstack().reset_index().pivot_table(index=["level_1"], columns="Soil", values=0, aggfunc=sum).T

all_factors_mean = []
all_factors_std = []
for df in [sal_weath, sal_soil, sal_field, sal_fert, sal_pcs]:
    all_factors_mean.append(pd.DataFrame(df.mean(), columns=[df.index.name]).unstack().reset_index())
    all_factors_std.append(pd.DataFrame(df.std(), columns=[df.index.name]).unstack().reset_index())
all_factors_mean = pd.concat(all_factors_mean)
all_factors_mean.columns = ["broad_cat", "specific_cat","mean"]
all_factors_mean.index = all_factors_mean[["broad_cat","specific_cat"]]

all_factors_std = pd.concat(all_factors_std)
all_factors_std.columns = ["broad_cat", "specific_cat","std"]
all_factors_std.index = all_factors_std[["broad_cat","specific_cat"]]
all_factors_mean["std"] = all_factors_std["std"]
all_factors_mean["stdErr"] = all_factors_mean["std"]/np.sqrt(len(sal_weath))

prop_of_total_plot(all_factors_mean[["mean","std","stdErr"]], method,
                                      figsize=(15,3), error_kw={"elinewidth":1, "capsize":2, "capthick":1}, fmt='%.2f')


# In[ ]:





# In[24]:


def get_summarize_data_into_specific_cats(method):
    weath_train_splits, cons_train_splits, pcs_train_splits = import_sal_data_by_method(method)
    sal_weath, sal_soil, sal_field, sal_fert, sal_pcs = divide_and_label_data_keeping_splits(weath_train_splits, cons_train_splits, pcs_train_splits)
    sal_weath.columns.name="level_1"
    #print(sal_weath.shape, sal_soil.shape, sal_field.shape, sal_fert.shape, sal_pcs.shape)
    #combine sal_soil across depths
    sal_soil.columns = sal_soil.columns.str.split("_",expand=True)
    sal_soil = sal_soil.unstack().reset_index().pivot_table(index=["level_1"], columns="Soil", values=0, aggfunc=sum).T

    all_factors_mean = []
    all_factors_std = []
    for df in [sal_weath, sal_soil, sal_field, sal_fert, sal_pcs]:
        all_factors_mean.append(pd.DataFrame(df.mean(), columns=[df.index.name]).unstack().reset_index())
        all_factors_std.append(pd.DataFrame(df.std(), columns=[df.index.name]).unstack().reset_index())
    all_factors_mean = pd.concat(all_factors_mean)
    all_factors_mean.columns = ["broad_cat", "specific_cat","mean"]
    all_factors_mean.index = all_factors_mean[["broad_cat","specific_cat"]]

    all_factors_std = pd.concat(all_factors_std)
    all_factors_std.columns = ["broad_cat", "specific_cat","std"]
    all_factors_std.index = all_factors_std[["broad_cat","specific_cat"]]
    all_factors_mean["std"] = all_factors_std["std"]
    all_factors_mean["stdErr"] = all_factors_mean["std"]/np.sqrt(len(sal_weath))

    all_factors_mean.index = pd.MultiIndex.from_tuples(all_factors_mean.index.tolist(), names=["Broad_cat","Specific_cat"])
    all_factors_mean = all_factors_mean[["mean","std","stdErr"]]
    return all_factors_mean


# In[28]:


def barplot_sng_cat(all_fact_mean, cat, tick_space, figsize=(10,4)):
    means = all_fact_mean.loc[cat].reset_index().pivot(index="scenario", columns="Specific_cat", values="mean").T.copy()
    means = means.sort_values(["GEM Practical"]).T
    std_Err = all_fact_mean.loc[cat].reset_index().pivot(index="scenario", columns="Specific_cat", values="stdErr").T.copy().T
    means["sum"] = means.sum(axis=1)
    for col in means.columns[:-1]:
        means[col] = means[col]/means["sum"]
        std_Err[col] = std_Err[col]/means["sum"]
        #std[col] = std[col]/means["sum"]
    means = means.drop(columns=["sum"])

    ax2 = means.T.plot(kind="bar", yerr=std_Err.T, figsize=figsize, edgecolor="white")
    num_cols = len(means.T.index) #number of labels across the bottm
    hatches = itertools.cycle(['///', '\\\\\\', '---', '|||', '////', '*', 'o', 'O', '.', '+']) # patterns to use
    for i, bar in enumerate(ax2.patches):  #go through each bar and add appropriate patch
        #print(i, bar, i % num_cols)
        if i % num_cols==0:  #if remainder is zero then we are on a new series (G, E, etc. in this case)
            hatch = next(hatches)
        bar.set_hatch(hatch)

    ax2.set_xlabel("")
    ax2.set_ylabel("Proportion of Total")
    ax2.yaxis.set_major_locator(ticker.MultipleLocator(tick_space))
    ax2.legend(loc='upper left')#bbox_to_anchor=(0.04, 1))
    ax2.set(facecolor = "white")
    #ax2.yaxis.set_major_formatter(FormatStrFormatter('%.1f'))
    #ax2.grid(color="lightgray", axis='y')
    plt.show()
    print(means.T)


# In[29]:


#Import data from several scenarios, get average and std, concatinat, plot

#####with historical data#####

methods_list = ["GEM_reps_val_training_13_Dec2019", 
                "reps_val_training_Practical_GEM_26Feb2020",
                "reps_val_training_E_dwnSample293_24Apr2020",
                "reps_val_training_G_dwnSample12_23Apr2020"]
all_fact_mean = []
for method in methods_list:
    all_factors_mean = get_summarize_data_into_specific_cats(method)
    all_factors_mean["method"]=method
    all_fact_mean.append(all_factors_mean.copy())
all_fact_mean = pd.concat(all_fact_mean)

#add extra column with testing scenario
all_fact_mean["scenario"] = all_fact_mean["method"].str[-10:]
repl = {'_23Apr2020':"G holdout", '13_Dec2019':"GEM hard", '_26Feb2020':"GEM Practical", '_24Apr2020':"E holdout"}
for key in repl:
    #print(key)
    all_fact_mean["scenario"] = all_fact_mean["scenario"].str.replace(key, repl[key])

#with historical data
barplot_sng_cat(all_fact_mean, cat="Weather", tick_space=0.05, figsize=(10,4))
barplot_sng_cat(all_fact_mean, cat="Soil", tick_space=0.05, figsize=(12,4))
barplot_sng_cat(all_fact_mean, cat="Fertility", tick_space=0.05, figsize=(14,4))
barplot_sng_cat(all_fact_mean, cat="Field", tick_space=0.1, figsize=(10,4))
barplot_sng_cat(all_fact_mean, cat="Genetics", tick_space=0.01, figsize=(18,5))


# In[30]:


#####withOUT historical data#####

methods_list = ["reps_val_training_NO_HIST_13_Dec2019", 
                "reps_val_training_NO_HIST_Practical_GEM_26Feb2020",
                "reps_val_training_NO_HIST_E_dwnSample293_24Apr2020",
                "reps_val_training_NO_HIST_G_dwnSample12_23Apr2020"]
all_fact_mean = []
for method in methods_list:
    all_factors_mean = get_summarize_data_into_specific_cats(method)
    all_factors_mean["method"]=method
    all_fact_mean.append(all_factors_mean.copy())
all_fact_mean = pd.concat(all_fact_mean)

#add extra column with testing scenario
all_fact_mean["scenario"] = all_fact_mean["method"].str[-10:]
repl = {'_23Apr2020':"G holdout", '13_Dec2019':"GEM hard", '_26Feb2020':"GEM Practical", '_24Apr2020':"E holdout"}
for key in repl:
    #print(key)
    all_fact_mean["scenario"] = all_fact_mean["scenario"].str.replace(key, repl[key])

#without historical data
barplot_sng_cat(all_fact_mean, cat="Weather", tick_space=0.05, figsize=(10,4))
barplot_sng_cat(all_fact_mean, cat="Soil", tick_space=0.05, figsize=(12,4))
barplot_sng_cat(all_fact_mean, cat="Fertility", tick_space=0.05, figsize=(14,4))
barplot_sng_cat(all_fact_mean, cat="Field", tick_space=0.1, figsize=(10,4))
barplot_sng_cat(all_fact_mean, cat="Genetics", tick_space=0.01, figsize=(18,5))


# In[ ]:





# In[31]:


#summarize by Broad categories
#####with historical data#####

methods_list = ["GEM_reps_val_training_13_Dec2019", 
                "reps_val_training_Practical_GEM_26Feb2020",
                "reps_val_training_E_dwnSample293_24Apr2020",
                "reps_val_training_G_dwnSample12_23Apr2020"]
all_fact_mean = []
for method in methods_list:
    weath_train_splits, cons_train_splits, pcs_train_splits = import_sal_data_by_method(method)
    sal_weath, sal_soil, sal_field, sal_fert, sal_pcs = divide_and_label_data_keeping_splits(weath_train_splits, cons_train_splits, pcs_train_splits)
    #print(sal_weath.shape, sal_soil.shape, sal_field.shape, sal_fert.shape, sal_pcs.shape)
    main_cats_splts = sum_and_devide_for_for_perct(sal_weath, sal_soil, sal_field, sal_fert, sal_pcs)
    main_cats_splts = pd.DataFrame([main_cats_splts.mean(), main_cats_splts.std(), main_cats_splts.std()/np.sqrt(len(main_cats_splts))],
                                   index=["mean","std","stdErr"]).T
    main_cats_splts["method"]=method
    all_fact_mean.append(main_cats_splts.copy())
all_fact_mean = pd.concat(all_fact_mean)

#add extra column with testing scenario
all_fact_mean["scenario"] = all_fact_mean["method"].str[-10:]
repl = {'_23Apr2020':"G holdout", '13_Dec2019':"GEM hard", '_26Feb2020':"GEM Practical", '_24Apr2020':"E holdout"}
for key in repl:
    #print(key)
    all_fact_mean["scenario"] = all_fact_mean["scenario"].str.replace(key, repl[key])
all_fact_mean.index = pd.MultiIndex.from_tuples(list(zip(["all"]*len(all_fact_mean),all_fact_mean.index.tolist())), names=["Broad_cat","Specific_cat"])

barplot_sng_cat(all_fact_mean, cat="all", tick_space=0.05, figsize=(6,4))


# In[32]:


#####withOUT historical data#####

methods_list = ["reps_val_training_NO_HIST_13_Dec2019", 
                "reps_val_training_NO_HIST_Practical_GEM_26Feb2020",
                "reps_val_training_NO_HIST_E_dwnSample293_24Apr2020",
                "reps_val_training_NO_HIST_G_dwnSample12_23Apr2020"]
all_fact_mean = []
for method in methods_list:
    weath_train_splits, cons_train_splits, pcs_train_splits = import_sal_data_by_method(method)
    sal_weath, sal_soil, sal_field, sal_fert, sal_pcs = divide_and_label_data_keeping_splits(weath_train_splits, cons_train_splits, pcs_train_splits)
    #print(sal_weath.shape, sal_soil.shape, sal_field.shape, sal_fert.shape, sal_pcs.shape)
    main_cats_splts = sum_and_devide_for_for_perct(sal_weath, sal_soil, sal_field, sal_fert, sal_pcs)
    main_cats_splts = pd.DataFrame([main_cats_splts.mean(), main_cats_splts.std(), main_cats_splts.std()/np.sqrt(len(main_cats_splts))],
                                   index=["mean","std","stdErr"]).T
    main_cats_splts["method"]=method
    all_fact_mean.append(main_cats_splts.copy())
all_fact_mean = pd.concat(all_fact_mean)

#add extra column with testing scenario
all_fact_mean["scenario"] = all_fact_mean["method"].str[-10:]
repl = {'_23Apr2020':"G holdout", '13_Dec2019':"GEM hard", '_26Feb2020':"GEM Practical", '_24Apr2020':"E holdout"}
for key in repl:
    #print(key)
    all_fact_mean["scenario"] = all_fact_mean["scenario"].str.replace(key, repl[key])
all_fact_mean.index = pd.MultiIndex.from_tuples(list(zip(["all"]*len(all_fact_mean),all_fact_mean.index.tolist())), names=["Broad_cat","Specific_cat"])

barplot_sng_cat(all_fact_mean, cat="all", tick_space=0.05, figsize=(6,4))


# In[ ]:




