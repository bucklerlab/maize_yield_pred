#!/usr/bin/env python
# coding: utf-8

# In[1]:


import os, sys
import pandas as pd
import numpy as np


# In[2]:


def get_file_names(wrkdir, sufix, contains):
    #sufix="raw.csv"
    #contains="hybrid"
    in_files=[]
    for (dirpath, dirnames, filenames) in os.walk(wrkdir):
        file1 = [x for x in filenames if x[-len(sufix):]==sufix and contains in x]
        if len(file1)==1:
            in_files.append(dirpath+"/"+file1[0])
    return in_files


# In[3]:


wrkdir="../data/GenomesToFields_2014_2017_v1/"
in_files = get_file_names(wrkdir, sufix="clean.csv", contains="hybrid")
pheno_data=[]
for filename in in_files:
    pheno_data.append(pd.read_csv(filename, low_memory=False, parse_dates=['Date Planted','Date Harvested', 'Anthesis [date]', 'Silking [date]']))
pheno_data = pd.concat(pheno_data, sort=False)
#remove locations in Canada as we don't have data for them
pheno_data = pheno_data[pheno_data["Field-Location"].str[:2]!="ON"]
pheno_data = pheno_data[pheno_data["Grain Yield [bu/A]"].isna()==False].reset_index(drop=True)

#remove sufix on Field-Location as some years and locations have extra letters, etc.
pheno_data["Field-Location"] = pheno_data["Field-Location"].str[:4]
#create location_year
pheno_data["Location_Year"] = pheno_data["Field-Location"]+"@"+pheno_data["Year"].astype(str)
print(len(pheno_data["Field-Location"].unique()),len(pheno_data["Location_Year"].unique()))


# In[4]:


meta_data=[]
conv_dict={'corner1 lat': 'Latitude_of_Field_Corner_#1 (lower left)', 
           'corner1 lon': 'Longitude_of_Field_Corner_#1 (lower left)', 
           'corner 2lat': 'Latitude_of_Field_Corner_#2 (lower right)', 
           'corner2 lon': 'Longitude_of_Field_Corner_#2 (lower right)', 
           'corner3 lat': 'Latitude_of_Field_Corner_#3 (upper right)', 
           'corner3 lon': 'Longitude_of_Field_Corner_#3 (upper right)', 
           'corner4 lat': 'Latitude_of_Field_Corner_#4 (upper left)', 
           'corner4 lon': 'Longitude_of_Field_Corner_#4 (upper left)',
           'Experiment':'Experiment_Code'} 
in_files = get_file_names(wrkdir, sufix="metadata.csv", contains="metadata")
for filename in in_files:
    tmp = pd.read_csv(filename, low_memory=False, encoding = "ISO-8859-1")
    tmp.rename(columns=conv_dict, inplace=True)
    tmp["Year"] = filename.split("g2f_")[1][:4]
    meta_data.append(tmp)
meta_data = pd.concat(meta_data, sort=False).reset_index(drop=True)
meta_data["Location_Year"] = meta_data["Experiment_Code"]+"@"+meta_data["Year"]
meta_data = meta_data[meta_data["Location_Year"].str[2]=="H"].reset_index(drop=True)


# In[5]:


#create north, south, east, west
meta_data["North"]=meta_data[['Latitude_of_Field_Corner_#1 (lower left)',
                              'Latitude_of_Field_Corner_#2 (lower right)',
                              'Latitude_of_Field_Corner_#3 (upper right)',
                              'Latitude_of_Field_Corner_#4 (upper left)']].max(axis=1)
meta_data["South"]=meta_data[['Latitude_of_Field_Corner_#1 (lower left)',
                              'Latitude_of_Field_Corner_#2 (lower right)',
                              'Latitude_of_Field_Corner_#3 (upper right)',
                              'Latitude_of_Field_Corner_#4 (upper left)']].min(axis=1)
meta_data["East"]=meta_data[['Longitude_of_Field_Corner_#1 (lower left)',
                             'Longitude_of_Field_Corner_#2 (lower right)',
                             'Longitude_of_Field_Corner_#3 (upper right)',
                             'Longitude_of_Field_Corner_#4 (upper left)']].max(axis=1)
meta_data["West"]=meta_data[['Longitude_of_Field_Corner_#1 (lower left)',
                             'Longitude_of_Field_Corner_#2 (lower right)',
                             'Longitude_of_Field_Corner_#3 (upper right)',
                             'Longitude_of_Field_Corner_#4 (upper left)']].min(axis=1)
meta_data = meta_data[["Location_Year","North","South","East","West"]]


# In[6]:


#add location_Years that are not in metadata but are in phenotype data
tmp = pd.DataFrame([x for x in pheno_data["Location_Year"].unique().tolist() 
                    if x not in list(meta_data["Location_Year"].unique())], columns=["Location_Year"])
meta_data = pd.concat([meta_data,tmp], sort=False).reset_index(drop=True)
meta_data["Location"] = meta_data["Location_Year"].str.split("@", expand=True)[0]
meta_data["Year"] = meta_data["Location_Year"].str.split("@", expand=True)[1]
#fill in missing data using same location but different year
imputed=[]
for location_year in meta_data[meta_data["North"].isna()]["Location_Year"]:
    tmp = meta_data[meta_data["Location"]==location_year.split("@")[0]].dropna().copy()
    tmp["year_diff"] = abs(tmp["Year"].astype(int) - int(location_year.split("@")[1]))
    if len(tmp) > 0:
        imputed.append([location_year]+tmp.sort_values(["year_diff"]).iloc[0].tolist()[1:5])
    else:
        tmp = meta_data[meta_data["Location"].str[:2]==location_year.split("@")[0][:2]].dropna().copy()
        tmp["year_diff"] = abs(tmp["Year"].astype(int) - int(location_year.split("@")[1]))
        imputed.append([location_year]+tmp.sort_values(["year_diff"]).iloc[0].tolist()[1:5])
meta_data = pd.concat([meta_data, pd.DataFrame(imputed, columns=["Location_Year","North","South","East","West"])],
                      sort=False)
meta_data = meta_data[["Location_Year","North","South","East","West"]]
meta_data = meta_data.dropna().reset_index(drop=True)
#sanity check on gps coordinates
#meta_data["Location"] = meta_data["Location_Year"].str.split("@", expand=True)[0]
#meta_data["State"] = meta_data["Location"].str[:2]
#meta_data.pivot_table(index=["State"], aggfunc=np.std)


# In[7]:


#merge in meta data
pheno_data = pheno_data.merge(meta_data, on="Location_Year", how='left')


# In[8]:


#add columns for later use
pheno_data["Latitude"] = pheno_data["North"]
pheno_data["Longitude"] = pheno_data["East"]
pheno_data["State"] = pheno_data["Location_Year"].str[:2]
state_abrev = {"ALABAMA":"AL","ALASKA":"AK","ARIZONA":"AZ","ARKANSAS":"AR","CALIFORNIA":"CA","COLORADO":"CO",
               "CONNECTICUT":"CT","DELAWARE":"DE","FLORIDA":"FL","GEORGIA":"GA","HAWAII":"HI","IDAHO":"ID",
               "ILLINOIS":"IL","INDIANA":"IN","IOWA":"IA","KANSAS":"KS","KENTUCKY":"KY","LOUISIANA":"LA",
               "MAINE":"ME","MARYLAND":"MD","MASSACHUSETTS":"MA","MICHIGAN":"MI","MINNESOTA":"MN",
               "MISSISSIPPI":"MS","MISSOURI":"MO","MONTANA":"MT","NEBRASKA":"NE","NEVADA":"NV",
               "NEW HAMPSHIRE":"NH","NEW JERSEY":"NJ","NEW MEXICO":"NM","NEW YORK":"NY","NORTH CAROLINA":"NC",
               "NORTH DAKOTA":"ND","OHIO":"OH","OKLAHOMA":"OK","OREGON":"OR","PENNSYLVANIA":"PA",
               "RHODE ISLAND":"RI","SOUTH CAROLINA":"SC","SOUTH DAKOTA":"SD","TENNESSEE":"TN","TEXAS":"TX",
               "UTAH":"UT","VERMONT":"VT","VIRGINIA":"VA","WASHINGTON":"WA","WEST VIRGINIA":"WV",
               "WISCONSIN":"WI","WYOMING":"WY"}
state_abrev = pd.DataFrame(state_abrev, index=[0]).T.reset_index()
state_abrev.index = state_abrev[0]
state_abrev = state_abrev["index"].to_dict()
pheno_data.replace(state_abrev, inplace=True)


# In[9]:


#make rough determination of which county a G2F location is in.
#get lat lon data by county
county_lat_lon = pd.read_csv("../data/General/County_lat_long_mod.csv", skiprows=[0])
county_lat_lon.replace(state_abrev, inplace=True)
county_lat_lon["County"] = county_lat_lon["County"].str.upper()
county_lat_lon["Lat"] = county_lat_lon["Lat"].str[1:-2].astype('float')
county_lat_lon["Longitude"] = county_lat_lon["Longitude"].str.replace("–","-").str[:-2].astype("float")
tmp = pheno_data[["Location_Year","Latitude", "Longitude","State"]].drop_duplicates().copy()
loc_county=[]
for location_year in tmp["Location_Year"]:
    #print(location_year)
    state = tmp[tmp["Location_Year"]==location_year]["State"].iloc[0]
    lat = tmp[tmp["Location_Year"]==location_year]["Latitude"].iloc[0]
    lon = tmp[tmp["Location_Year"]==location_year]["Longitude"].iloc[0]
    tmp_state = county_lat_lon[county_lat_lon["State"]==state].copy()
    #calculate distance and determine nearest county seat to G2F location
    tmp_state["Lat_dist"] = abs(tmp_state["Lat"]-lat)
    tmp_state["Lon_dist"] = abs(tmp_state["Longitude"]-lon)
    tmp_state["sum_dist"] = tmp_state["Lat_dist"]+tmp_state["Lon_dist"]
    loc_county.append([location_year,tmp_state.sort_values("sum_dist").iloc[0]["County"]])
loc_county = pd.DataFrame(loc_county,columns=["Location_Year","County"])
pheno_data = pheno_data.merge(loc_county, on="Location_Year", how="left")
pheno_data["State"] = pheno_data["State"].str.replace(" ","_")
pheno_data["County"] = pheno_data["County"].str.replace(" ","_")


# In[10]:


first = ['Year', 'Field-Location', 'Location_Year', 'Pedigree', 'Grain Yield [bu/A]', 'Date Planted',
         'Date Harvested', 'Anthesis [date]', 'Silking [date]', 'North', 'South','East', 'West']
#calculate plant density
pheno_data["Plant Density"] = pheno_data["Stand Count [plants]"]/(pheno_data["Plot Area"]*0.092903)
pheno_data = pheno_data[first+[x for x in pheno_data.columns if x not in first]]
#impute missing plant density from others in year
mean_density_loc_year={}
for loc_year in pheno_data[(pheno_data["Plant Density"].isna()) | (pheno_data["Plant Density"] < 1)]["Location_Year"].unique():
    mean_density_loc_year[loc_year]=pheno_data[pheno_data["Location_Year"]==loc_year]["Plant Density"].mean()
    if (mean_density_loc_year[loc_year]>0) == False:
        mean_density_loc_year[loc_year]=pheno_data[pheno_data["Field-Location"]==loc_year.split("@")[0]]["Plant Density"].mean()
for row in pheno_data.index:
    if (pheno_data.loc[row, "Plant Density"]>0)==False:
        pheno_data.loc[row, "Plant Density"]=mean_density_loc_year[pheno_data.loc[row, "Location_Year"]]

pheno_data.to_csv("../data/Phenotype_data/G2F_work_set_2014_2017.csv")


# In[11]:


#add G2F soil health data
soil_health2014=pd.read_csv("../data/GenomesToFields_2014_2017_v1/G2F_Planting_Season_2014_v4/z._2014_supplemental_info/g2f_2014_field_characteristics.csv", encoding='iso-8859-1')
soil_health2014["Year"]=2014
soil_health2014 = soil_health2014[["Experiment","Soil pH","Year"]]
soil_health2014["Soil pH"] = soil_health2014["Soil pH"].str.replace("PH = ","")
soil_health2014.dropna(inplace=True)
soil_health2014 = soil_health2014[soil_health2014["Soil pH"].str.len()<6] #remove text writen in ph field
soil_health2014["Soil pH"] = soil_health2014["Soil pH"].astype(float)
soil_health2014.rename(columns={"Experiment":"Location", "Soil pH":"1:1 Soil pH"}, inplace=True)

soil_health2015=pd.read_csv("../data/GenomesToFields_2014_2017_v1/G2F_Planting_Season_2015_v2/d._2015_soil_data/g2f_2015_soil_data.csv")
soil_health2015["Year"]=2015
soil_health2015.rename(columns={'Experiment':'Location', 'PH':'1:1 Soil pH', 'OM':'Organic Matter LOI %', 
                                'P':'Mehlich P-III ppm P', 'K':'Potassium ppm K'}, inplace=True)
soil_health2015["Location"] = soil_health2015["Location"].str.replace("(","")
soil_health2015["Location"] = soil_health2015["Location"].str.replace(")","")
#note that the P methods betwen 2015 and 2016/2017 are slightly different, but expected to yield similar results, 
#at least for soils with Ph 7.3 or lower. http://www.agronext.iastate.edu/soilfertility/presentations/mbotest.pdf
soil_health2016=pd.read_csv("../data/GenomesToFields_2014_2017_v1/G2F_Planting_Season_2016_v2/c._2016_soil_data/g2f_2016_soil_data_clean.csv")
soil_health2016["Year"]=2016
soil_health2016["1:1 Soil pH"] = soil_health2016["1:1 Soil pH"].str.replace("1:2 soil:water ","").astype(float)
#pH 1:1 vs 1:2 seem to pretty comparible from what I can tell. https://www.nrcs.usda.gov/Internet/FSE_DOCUMENTS/nrcs142p2_050730.pdf
soil_health2017=pd.read_csv("../data/GenomesToFields_2014_2017_v1/G2F_Planting_Season_2017_v1/c._2017_soil_data/g2f_2017_soil_data_clean.csv")
soil_health2017["Year"]=2017
soil_health2017["Location"] = soil_health2017["Location"].str[:4]

soil_health=pd.concat([soil_health2017,soil_health2016, soil_health2015, soil_health2014], sort=False)[soil_health2017.columns]
soil_health["Location_Year"] = soil_health["Location"]+"@"+soil_health["Year"].astype(str)
soil_health = soil_health[soil_health["Location_Year"].isin(pheno_data["Location_Year"].unique().tolist())]
soil_health.reset_index(drop=True, inplace=True)
soil_health = soil_health[['Location_Year','Location', 'Year', '1:1 Soil pH','WDRF Buffer pH',
                           '1:1 S Salts mmho/cm','Organic Matter LOI %', 'Nitrate-N ppm N',
                           'lbs N/A', 'Potassium ppm K', 'Sulfate-S ppm S','Calcium ppm Ca', 'Magnesium ppm Mg',
                           'Sodium ppm Na', 'CEC/Sum of Cations me/100g', '%H Sat', '%K Sat','%Ca Sat', '%Mg Sat',
                           '%Na Sat', 'Mehlich P-III ppm P', '% Sand', '% Silt', '% Clay']]
#average multiple samplings within Location_Year
soil_health["Year"] = soil_health["Year"].astype(str)
soil_health = soil_health.pivot_table(index=["Location_Year"], aggfunc=np.mean)
soil_health = pd.concat([pd.DataFrame(soil_health.index.str.split("@").tolist(), columns=["Location","Year"], index=soil_health.index),
                           soil_health], axis=1)


# In[12]:


#add location year combos without data to prepare for imputation
tmp = pd.DataFrame(index=[x for x in pheno_data["Location_Year"].unique() if x not in soil_health.index],
                   columns=soil_health.columns)
tmp["Location"] = pd.Series(tmp.index).str.split("@",expand=True)[0].tolist()
tmp["Year"] = pd.Series(tmp.index).str.split("@",expand=True)[1].tolist()
soil_health = pd.concat([soil_health,tmp], axis=0, sort=False)
soil_health_imputed=[]
for loc_year in soil_health.index:
    #print(loc_year)
    if len(soil_health.loc[loc_year].dropna()) == soil_health.shape[1]:
        soil_health_imputed.append(soil_health.loc[loc_year])
    else:
        if len(soil_health[soil_health["Location"]==loc_year.split("@")[0]].mean().dropna())==len(soil_health.mean()):
            #fill NA's with average value for location across years
            soil_health_imputed.append(soil_health.loc[loc_year].fillna(soil_health[soil_health["Location"]==loc_year.split("@")[0]].mean()))
        else:
            #fill NA's with average value for state across years
            soil_health_imputed.append(soil_health.loc[loc_year].fillna(soil_health[soil_health["Location"].str[:2]==loc_year[:2]].mean()))
soil_health_imputed = pd.concat(soil_health_imputed, axis=1).T
print(soil_health_imputed.shape, soil_health_imputed.dropna().shape)
#fill remaining NA's with the average of all other values by column.
tmp = soil_health_imputed.loc[:,[x for x in soil_health_imputed.columns if x not in ["Location","Year"]]].mean()
soil_health_imputed.fillna(tmp, inplace=True)
soil_health_imputed.drop(columns=["Location","Year"], inplace=True)
pheno_data = pheno_data.merge(soil_health_imputed, left_on=["Location_Year"], right_index=True, how='left')
pheno_data.to_csv("../data/Phenotype_data/G2F_work_set_2014_2017.csv")


# In[13]:


#add fertilizer
fert=pd.read_csv("../data/GenomesToFields_2014_2017_v1/G2F_Converted_Fert&Irr/Combined_G2F_Fert.txt", sep="\t", na_values=-1)
fert["Location_Year"] = fert["Location"]+"@"+fert["Year"].astype(str)
fert = fert[["Location_Year", "Total N lbs/acre", "Total P lbs/acre", "Total K lbs/acre"]]
fert = fert.pivot_table(index=["Location_Year"], aggfunc=np.mean)

pheno_data = pheno_data.merge(fert, on=["Location_Year"], how='left')

#could impute here, but not doing that for now...
missing = pheno_data[(pheno_data["Total K lbs/acre"].isna()) &
                     (pheno_data["Total N lbs/acre"].isna()) &
                     (pheno_data["Total P lbs/acre"].isna())]["Location_Year"].unique().tolist()
#assume missing data means no fertelizer and fill nas with 0
pheno_data["Total K lbs/acre"] = pheno_data["Total K lbs/acre"].fillna(0)
pheno_data["Total N lbs/acre"] = pheno_data["Total N lbs/acre"].fillna(0)
pheno_data["Total P lbs/acre"] = pheno_data["Total P lbs/acre"].fillna(0)


# In[14]:


#add irrigation data
irrigation = pd.read_csv("../data/GenomesToFields_2014_2017_v1/G2F_Converted_Fert&Irr/Combined_G2F_irrigation.txt", sep="\t")
irrigation["Location_Year"] = irrigation["Experiment"]+"@"+irrigation["Year"].astype(str)
irrigation = irrigation.pivot_table(index=["Location_Year"], values="Irrigation amount (inches)", aggfunc=np.mean)
pheno_data = pheno_data.merge(irrigation, on=["Location_Year"], how='left')
pheno_data["Irrigation amount (inches)"] = pheno_data["Irrigation amount (inches)"].fillna(0)
pheno_data["Irrigated"] = pheno_data["Irrigation amount (inches)"]>0
pheno_data["Irrigated"]=pheno_data["Irrigated"].astype(int)


# In[15]:


pheno_data["Pedigree"] = pheno_data["Pedigree"].str.strip()
pheno_data["Pedigree"] = pheno_data["Pedigree"].str.replace("*","_")
pheno_data["Pedigree"] = pheno_data["Pedigree"].str.replace(" ","_")
pheno_data.to_csv("../data/Phenotype_data/G2F_work_set_2014_2017.csv")


# In[16]:


pheno_data.columns


# In[ ]:




