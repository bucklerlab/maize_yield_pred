#!/usr/bin/env python
# coding: utf-8

# In[7]:


import sys, os
import pandas as pd


# In[8]:


#import genotype codes
#geno_codes = pd.read_excel("../data/GenomesToFields_2014_2017_v1/G2F_Planting_Season_2017_v1/d._2017_genotypic_data/g2f_2017_gbs_hybrid_codes.xlsx")
#genotypes = geno_codes["Pedigree"].unique().tolist()


# In[10]:


#import phenotype data
phenotypes = pd.read_csv("../data/Phenotype_data/G2F_work_set_2014_2017.csv", low_memory=False, index_col=[0])
#print(len(phenotypes))
#phenotypes["Pedigree"] = phenotypes["Pedigree"].str.strip()
#phenotypes["Pedigree"] = phenotypes["Pedigree"].str.replace("*","_")
#phenotypes["Pedigree"] = phenotypes["Pedigree"].str.replace(" ","_")
print(len(phenotypes))
#phenotypes.to_csv("../data/Phenotype_data/G2F_work_set_2014_2017.csv")
pedigrees = phenotypes["Pedigree"].unique().tolist()


# In[4]:


#import genotype data filtered by Anna Rogers (NCSU)
hmp = pd.read_csv("../data/Genotype_data/G2FHybrids_RecipRem_2_22_2018_Anna_Rogers.hmp.txt", sep="\t")
replace_dict={"B47":"PHB47", "ICI":"ICI ", "MBNIL_":"MBNIL ", "TX130_RP":"TX130 RP","TX745RP":"TX745 RP","TR9":"TR 9",
              "REDEAR-B":"REDEAR2-2-2-1-1-2-B-B-1-B10",
              '\(LAMA2002-35-2-B-B-B-B/CG44\)-1-3-B-1-1-B24-B5-B16/LH195': '(LAMA2002-35-2-B-B-B-B_CG44)-1-3-B-1-1-B24-B5-B16/LH195',
              '\(LAMA2002-35-2-B-B-B-B/CG44\)-1-3-B-1-1-B24-B5-B16/TX777': '(LAMA2002-35-2-B-B-B-B_CG44)-1-3-B-1-1-B24-B5-B16/TX777',
              '\(TX739\)_LAMA2002-10-1/LH195': '(TX739);LAMA2002-10-1-B-B-B-B3-B7ORANGE-B6/LH195',
              '\(TX739\)_LAMA2002-10-1/PB80': '(TX739);LAMA2002-10-1-B-B-B-B3-B7ORANGE-B6/PB80',
              'ARGENTINE_FLINTY_COMPOSITE-C\(1\)-37-B-B-B2-1-B25/LH195': 'ARGNETINE FLINTY COMPOSITE-C(1)-37-B-B-B2-1-B25/LH195',
              '\(CML442-B/CML343-B-B-B-B-B-B\)-B-B-1-1-B-B-B-1-B12-1-B19/LH195': 'CML442_CML343/LH195',
              '\(CML450-B/TX110\)-B-3-B-1-B-B-1-1-B18-B21-B8/LH195': 'CML450_TX110/LH195'
             }
for key in replace_dict.keys():
    hmp.columns = hmp.columns.str.replace(key,replace_dict[key])
hmp.columns = hmp.columns.str.replace(" ","_")
genotypes = hmp.columns[11:].tolist()


# In[5]:


pheno_and_geno=[x for x in pedigrees if x in genotypes]
print("Pedigrees with both phenotypes and genetic data:" , len(pheno_and_geno))
print("Phenotype no genetic data:", len([x for x in pedigrees if x not in genotypes]))
print("Genetic data no phenotype:", len([x for x in genotypes if x not in pedigrees]))


# In[6]:


#geno_no_pheno = [x for x in genotypes if x not in pedigrees]
#print(len(geno_no_pheno))
#sorted(geno_no_pheno)


# In[7]:


#pheno_no_geno = [x for x in pedigrees if x not in genotypes and "/" in x]
#print(len(pheno_no_geno))
#sorted(pheno_no_geno)


# In[8]:


#filter genotype table to include only genotypes with phenotypes
hmp = hmp[hmp.columns[:11].tolist()+pheno_and_geno]
hmp.to_csv("../data/Genotype_data/G2FHybrids_RecipRem_2_22_2018_Anna_Rogers_genos_w_phenos.hmp.txt", sep="\t", index=False)

#Then import into tassel and create GRM and PCS
#go to analysis -> relatedness -> kinship.
#use defalts and Centered_IBS
#Save as G2FHybrids_RecipRem_2_22_2018_Anna_Rogers_genos_w_phenos_Cent_IBS.txt

#create PCs
#select the filtered hybrid genotype matrix (not GRM)
#go to analysis -> relatedness -> PCA
#type in 1000 PCs
#click Ok
#Save as G2FHybrids_RecipRem_2_22_2018_Anna_Rogers_genos_w_phenos_PCs.txt


# In[ ]:





# In[40]:


#create hybrid code file for Tassel
#geno_codes = geno_codes[geno_codes["Pedigree"].isin(pheno_and_geno)].dropna()
#geno_codes[["Female GBS","Male GBS"]].to_csv("../data/Genotype_data/codes_for_tassel.txt",
#                                                      sep="\t", header=False, index=False)


#load genotype data into tassel
#go to data -> homozygous genotypes -this drops heterozygous genotypes in the inbred lines (most of these are errors)
#go to Filter -> filter genotype table by sites
#put in 0.001 for site min allele freq. and hit ok.
#filter by taxa with min 0.001 #remove blanks
#filter again but put in 1213 for site min count (drop sites w/ 20% or more missing data)
#filter on taxa with 0.6
#hit ok
#go to data -> create hybrid genotypes
#select file just saved above
#select Ok

#Create Genomic relationship matrix
#select the filter hybrid genotype matrix.
#go to analysis -> relatedness -> kinship.
#use defalts and Centered_IBS
#Save as Centered_IBS.txt

#create PCs
#select the filtered hybrid genotype matrix (not GRM)
#go to analysis -> relatedness -> PCA
#type in 1000 PCs
#click Ok


# In[ ]:


'''
~/TASSEL5/run_pipeline.pl \
-Xmx50g \
-h5 ../GenomesToFields_2014_2017_v1/G2F_Planting_Season_2017_v1/d._2017_genotypic_data/g2f_2017_ZeaGBSv27_Imputed_AGPv4.h5 \
-homozygous \
-filterAlign \
-filterAlignMinFreq 0.0001 \


-export g2f_2017_ZeaGBSv27_Imputed_AGPv4_homozygous.h5 \
-exportType HDF5
'''


# In[ ]:




