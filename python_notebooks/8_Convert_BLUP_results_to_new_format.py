#!/usr/bin/env python
# coding: utf-8

# In[1]:


import os, sys
import pandas as pd
from datetime import datetime
import numpy as np
from scipy.stats import pearsonr
from scipy.stats import spearmanr
from scipy.stats import linregress
import matplotlib.pyplot as plt


# In[2]:


#Set results directory
results_dir="../data/Results/"

#read in GR files, convert to new format, write out in new format.
#Run statistics and write results to combined file with other approaches


# In[3]:


def get_performance_stats_folds(results):
    performance_stats=[]
    for split in results["split"].unique():
        tmp = results[results["split"]==split].copy()
        if len(tmp)<2:continue
        real = tmp["Observed"]
        PR = tmp["Predicted"]
        prsn = pearsonr(np.array(real), np.array(PR))
        sprmn = spearmanr(np.array(real), np.array(PR))
        prsn = prsn[0]
        sprmn = sprmn[0]
        slope = linregress(real,PR)[0]
        r2 = prsn**2
        mae = np.mean(np.abs(np.array(PR) - np.array(real)))
        rMAE = mae / real.mean()
        normMAE = mae / (real.max()-real.min())
        RMSE = np.sqrt(np.mean(np.square(np.array(PR) - np.array(real))))
        rRMSE = RMSE / np.mean(np.array(real))
        normRMSE = RMSE/(real.max()-real.min())
        #based on https://en.wikipedia.org/wiki/Root-mean-square_deviation
        performance_stats.append([split, prsn, sprmn, slope, r2, mae, rMAE, normMAE, RMSE, rRMSE, normRMSE, len(PR)])
    performance_stats = pd.DataFrame(performance_stats, columns=["split", "prsn", "sprmn", "slope", "r2", "mae", "rMAE",
                                                                 "normMAE", "RMSE", "rRMSE", "normRMSE","n"])
    return performance_stats


# In[4]:


def get_performance_stats_overall(results):
    performance_stats=[]
    tmp = results.copy()
    #if len(tmp)<2:continue
    real = tmp["Observed"]
    PR = tmp["Predicted"]
    prsn = pearsonr(np.array(real), np.array(PR))
    sprmn = spearmanr(np.array(real), np.array(PR))
    prsn = prsn[0]
    sprmn = sprmn[0]
    slope = linregress(real,PR)[0]
    r2 = prsn**2
    mae = np.mean(np.abs(np.array(PR) - np.array(real)))
    rMAE = mae / real.mean()
    normMAE = mae / (real.max()-real.min())
    RMSE = np.sqrt(np.mean(np.square(np.array(PR) - np.array(real))))
    rRMSE = RMSE / np.mean(np.array(real))
    normRMSE = RMSE/(real.max()-real.min())
    #based on https://en.wikipedia.org/wiki/Root-mean-square_deviation
    performance_stats.append([np.nan, prsn, sprmn, slope, r2, mae, rMAE, normMAE, RMSE, rRMSE, normRMSE, len(PR)])
    performance_stats = pd.DataFrame(performance_stats, columns=["split", "prsn", "sprmn", "slope", "r2", "mae", "rMAE",
                                                                 "normMAE", "RMSE", "rRMSE", "normRMSE","n"])
    return performance_stats


# In[28]:


res_list=[]
for file_name in [x for x in os.listdir(results_dir) if x[:3]=="GR_"]:
    print(file_name)
    #read in file
    GR_results = pd.read_csv(results_dir+file_name, sep=" ")
    GR_results["method"] = GR_results["method"].replace("AD","G")
    GR_results = GR_results.rename(columns={"split_name":"split","obs":"Observed","pred":"Predicted", "id":"index"})
    
    #add set, rep, and timestamp?
    GR_results["set"]="test"
    GR_results["rep"]=0
    GR_results["timestamp"] = datetime.timestamp(datetime. strptime('28/08/20','%d/%m/%y'))
    GR_results = GR_results[["index","Observed","Predicted","method","split","set","rep","timestamp"]]
    
    #modify method by adding file name details
    if file_name.split("_")[1][:7]=="ADEBLUP":
        GR_results["method"] = GR_results["method"]+"-AD"+"_"+"_".join(file_name.split("_ADEBLUP-Ypred-w=3-"))[:-4]
    else:
        GR_results["method"] = GR_results["method"]+"_"+"_".join(file_name.split("_28Aug2020_Ypred-w=3-"))[:-4]
    #add S_ to split names to make sure they remain strings
    GR_results["split"] = "S_"+ GR_results["split"].astype("str")
    
    res_list.append(GR_results)
GR_results = pd.concat(res_list)


# In[29]:


GR_results


# In[30]:


GR_results.to_csv(results_dir+"For_ensamble_GR_all_methods_14Jul2021.csv")


# In[31]:


new_res=[]
for method in GR_results["method"].unique():
    #print(method)
    stats_folds = get_performance_stats_folds(GR_results[GR_results["method"]==method])
    stats_overall = get_performance_stats_overall(GR_results[GR_results["method"]==method])
    #re-format
    tmp = pd.concat([stats_folds.mean(), stats_folds.std(), stats_overall.mean()], axis=1)
    tmp.columns = ["Mean","Std","Overall"]
    tmp = tmp.T
    tmp["n_folds"]=len(stats_folds)
    tmp = tmp.reset_index().dropna(axis=1)
    tmp.rename(columns ={"index":"Mean/std"}, inplace = True)
    tmp.loc[2,"n_folds"]=np.nan
    tmp["method"] = method
    new_res.append(tmp)
new_res = pd.concat(new_res)
new_res = new_res[["method"]+new_res.columns[:-1].tolist()]
new_res["last_update"]=datetime.now().strftime('%m%d%Y_%H%M%S')


# In[51]:


#pd.set_option('display.max_rows', 200)
#new_res[new_res["Mean/std"]=="Mean"].sort_values(["RMSE"])#[["method","prsn","RMSE","normRMSE"]]


# In[45]:


#read in previous results file
sum_res_file = "Summary_results_all_02Nov2020.csv"

#only use the first time you create the file then comment out
#new_res.to_csv(results_dir+sum_res_file, index=False)

#read in previously save summary results file
sum_res = pd.read_csv(results_dir+sum_res_file)


# In[46]:


sum_res


# In[47]:


#add new results to file while replace old ones from same method
sum_res = pd.concat([sum_res[sum_res["method"].isin(new_res["method"].unique().tolist())==False],new_res])


# In[48]:


sum_res


# In[50]:


#save to file
sum_res.to_csv(results_dir+sum_res_file, index=False)


# In[ ]:




