#!/usr/bin/env python
# coding: utf-8

# In[1]:


import os, sys
import pandas as pd
import json
import numpy as np
from scipy.stats import pearsonr
from scipy.stats import spearmanr
from scipy.stats import linregress
import matplotlib.pyplot as plt
from datetime import datetime
import itertools
from tqdm.notebook import tqdm
import warnings


# In[2]:


#surpress future warning that keeps comming from pearsonr
warnings.filterwarnings("ignore", category=FutureWarning)


# In[3]:


def get_performance_states(results):
    performance_stats=[]
    for split in results["split"].unique():
        tmp = results[results["split"]==split].copy()
        tmp = tmp.dropna()
        if len(tmp)<2:continue
        real = tmp["Observed"]
        PR = tmp["Predicted"]
        prsn = pearsonr(np.array(real), np.array(PR))
        sprmn = spearmanr(np.array(real), np.array(PR))
        prsn = prsn[0]
        sprmn = sprmn[0]
        slope = linregress(real,PR)[0]
        r2 = prsn**2
        mae = np.mean(np.abs(np.array(PR) - np.array(real)))
        rMAE = mae / real.mean()
        normMAE = mae / (real.max()-real.min())
        RMSE = np.sqrt(np.mean(np.square(np.array(PR) - np.array(real))))
        rRMSE = RMSE / np.mean(np.array(real))
        normRMSE = RMSE/(real.max()-real.min())
        #based on https://en.wikipedia.org/wiki/Root-mean-square_deviation
        performance_stats.append([split, prsn, sprmn, slope, r2, mae, rMAE, normMAE, RMSE, rRMSE, normRMSE, len(PR)])
    performance_stats = pd.DataFrame(performance_stats, columns=["split", "prsn", "sprmn", "slope", "r2", "mae", "rMAE",
                                                                 "normMAE", "RMSE", "rRMSE", "normRMSE","n"])
    return performance_stats

def get_performance_stats_overall(results):
    performance_stats=[]
    tmp = results.copy()
    tmp = tmp.dropna()
    #if len(tmp)<2:continue
    real = tmp["Observed"]
    PR = tmp["Predicted"]
    prsn = pearsonr(np.array(real), np.array(PR))
    sprmn = spearmanr(np.array(real), np.array(PR))
    prsn = prsn[0]
    sprmn = sprmn[0]
    slope = linregress(real,PR)[0]
    r2 = prsn**2
    mae = np.mean(np.abs(np.array(PR) - np.array(real)))
    rMAE = mae / real.mean()
    normMAE = mae / (real.max()-real.min())
    RMSE = np.sqrt(np.mean(np.square(np.array(PR) - np.array(real))))
    rRMSE = RMSE / np.mean(np.array(real))
    normRMSE = RMSE/(real.max()-real.min())
    #based on https://en.wikipedia.org/wiki/Root-mean-square_deviation
    performance_stats.append([np.nan, prsn, sprmn, slope, r2, mae, rMAE, normMAE, RMSE, rRMSE, normRMSE, len(PR)])
    performance_stats = pd.DataFrame(performance_stats, columns=["split", "prsn", "sprmn", "slope", "r2", "mae", "rMAE",
                                                                 "normMAE", "RMSE", "rRMSE", "normRMSE","n"])
    return performance_stats


# In[4]:


#calculate stats for all sets for a given method
def ensmble_and_rep_stats(results_CNN):
    #if len(results_CNN["method"].unique()) > 1:
    #    print("Warning: You have provided results from multiple methods.")
    by_split_rep=[]
    combined_stats=[]
    combined_stats_std=[]
    combined_stats_overall=[]
    combined_stats_sng_reps=[]
    for tset in results_CNN["set"].unique():
        #print(tset)
        sng_set = results_CNN[(results_CNN["set"]==tset)].copy()
        #average across reps
        results_sng_pvt = sng_set.pivot_table(index=["split","index"]).reset_index()
        #get performance data
        perf_stats_reps = get_performance_states(results_sng_pvt)
        perf_stats_reps_overall = get_performance_stats_overall(results_sng_pvt[results_sng_pvt["split"]!="S_Historical"])
        combined_stats.append(pd.DataFrame(perf_stats_reps[perf_stats_reps["split"]=="S_Historical"].mean(),
                                           columns=[tset+"_Hist"]))
        combined_stats.append(pd.DataFrame(perf_stats_reps[perf_stats_reps["split"]!="S_Historical"].mean(),
                                           columns=[tset+"_Final"]))
        combined_stats_std.append(pd.DataFrame(perf_stats_reps[perf_stats_reps["split"]=="S_Historical"].std(),
                                           columns=[tset+"_Hist"]))
        combined_stats_std.append(pd.DataFrame(perf_stats_reps[perf_stats_reps["split"]!="S_Historical"].std(),
                                           columns=[tset+"_Final"]))
        
        combined_stats_overall.append(pd.DataFrame(perf_stats_reps_overall[perf_stats_reps_overall["split"]!="S_Historical"].mean(),
                                           columns=[tset+"_Final"]))
        
        #get stats by rep
        for rep in sng_set["rep"].unique():
            #print(rep)
            sng_set_rep = sng_set[sng_set["rep"]==rep].copy()
            perf_stats_sng_rep = get_performance_states(sng_set_rep)
            combined_stats_sng_reps.append(pd.DataFrame(perf_stats_sng_rep[perf_stats_sng_rep["split"]=="S_Historical"].mean(),
                                                        columns=[tset+"_rep"+str(rep)+"_Hist"]))
            combined_stats_sng_reps.append(pd.DataFrame(perf_stats_sng_rep[perf_stats_sng_rep["split"]!="S_Historical"].mean(),
                                                        columns=[tset+"_rep"+str(rep)+"_Final"]))
            perf_stats_sng_rep["set"]=tset
            perf_stats_sng_rep["rep"]=rep
            by_split_rep.append(perf_stats_sng_rep)
    combined_stats = pd.concat(combined_stats, axis=1)
    combined_stats_std = pd.concat(combined_stats_std, axis=1)
    combined_stats_overall = pd.concat(combined_stats_overall, axis=1)
    combined_stats_sng_reps = pd.concat(combined_stats_sng_reps, axis=1)
    mult_ind = pd.DataFrame(combined_stats_sng_reps.columns)
    mult_ind = mult_ind[0].str.split("_", expand=True)
    mult_ind.columns = ["set","rep","model"]
    mult_ind["rep"] = mult_ind["rep"].str.replace("rep","").astype(int)
    combined_stats_sng_reps.columns = pd.MultiIndex.from_frame(mult_ind)
    combined_stats_sng_reps = combined_stats_sng_reps.T.reset_index()
    by_split_rep = pd.concat(by_split_rep)
    return combined_stats, combined_stats_std, combined_stats_sng_reps, by_split_rep, combined_stats_overall


# In[5]:


#find out what files and methods are available
results_dir="../data/Results/"
res_file_details = pd.DataFrame([x for x in os.listdir(results_dir) if x[:13]=="For_ensamble_"], columns=["File"])
res_file_details["file_short"] = res_file_details["File"].str[13:-4]
res_file_details["method"] = res_file_details["File"].str[13:-4]
res_file_details["File"] = results_dir+res_file_details["File"]

#include apsim results
apsim_res_dir="../../Washburn_repos/apsim_genotype_calibrator/data/Apsim_files/Classic/Calibrations/Standard_cultivars_pltArea_results/"
ap_res_file_details = pd.DataFrame([x for x in os.listdir(apsim_res_dir) if x[:13]=="For_ensamble_"], columns=["File"])
ap_res_file_details["file_short"] = ap_res_file_details["File"].str[13:-4]
ap_res_file_details["method"] = ap_res_file_details["File"].str[13:-4]
ap_res_file_details["File"] = apsim_res_dir+ap_res_file_details["File"]

res_file_details = pd.concat([res_file_details, ap_res_file_details])


# In[6]:


#display list of files/methods
res_file_details[["method","file_short","File"]].sort_values(["method","file_short"])


# In[7]:


#upload results from all methods
results_files=[]
for file in res_file_details["File"].tolist():
    tmp = pd.read_csv(file, index_col=[0])
    tmp = tmp[tmp["index"]!="index"]
    tmp["split"] = tmp["split"].astype("str")
    tmp["rep"] = tmp["rep"].astype("int")
    if "method" not in tmp.columns:
        tmp["method"]=res_file_details[res_file_details["File"]==file]["method"].iloc[0]
    print(file, len(tmp["split"].unique()), len(tmp["rep"].unique()))
    results_files.append(tmp.copy())
results_CNN = pd.concat(results_files)
results_CNN = results_CNN.apply(pd.to_numeric, errors='ignore')
#results_CNN["split"] = "S_"+ results_CNN["split"].astype("str")

#combine replicates from the same experiment
#results_CNN["method"] = results_CNN["method"].str.replace("\d+-\d+_","")
results_CNN["method"].unique()


# In[8]:


#add scenario labeling
results_CNN["scenario"] = results_CNN["method"].str[-10:]

repl = {'_23Apr2020':"G holdout", '13_Dec2019':"GEM hard", '_26Feb2020':"GEM Practical", '_24Apr2020':"E holdout"}
for key in repl:
    #print(key)
    results_CNN["scenario"] = results_CNN["scenario"].str.replace(key, repl[key])
results_CNN["scenario"].unique()


# In[9]:


results_CNN


# In[10]:


def powerset(iterable):
    "powerset([1,2,3]) --> () (1,) (2,) (3,) (1,2) (1,3) (2,3) (1,2,3)"
    s = list(iterable)
    return itertools.chain.from_iterable(itertools.combinations(s, r) for r in range(2, len(s)+1))

def combine_results(combined_stats_rep_ens, combined_stats_rep_ens_std, combined_stats_overall_ens, column):
    #combine mean and std results
    new_res = pd.concat([combined_stats_rep_ens[[column]],
                         combined_stats_rep_ens_std[[column]],
                         combined_stats_overall_ens[[column]]], axis=1)
    new_res.columns = ["Mean","Std","Overall"]
    new_res = new_res.dropna().T
    n_folds = len(tmp[tmp["split"]!="Historical"]["split"].unique())
    new_res["n_folds"]=n_folds
    new_res = new_res.reset_index()
    new_res.rename(columns ={"index":"Mean/std"}, inplace = True)
    new_res["scenario"]=scenario
    new_res["method"] = " & ".join(combo)
    new_res.loc[2,"n_folds"]=np.nan
    new_res["set"]=column
    new_res = new_res[["method","scenario","set"]+new_res.columns[:-3].tolist()]
    new_res["last_update"]=datetime.now().strftime('%m%d%Y_%H%M%S')
    return new_res

def run_ensmbl_stats(results_CNN, combo):
    #generate file with all desired data for ensmble
    tmp = results_CNN[results_CNN["method"].isin(combo)].copy()
    if set(tmp["method"].unique()) != set(combo):
        print("ERROR!!! combo methods do not match with data!")
    
    combined_stats_rep_ens, combined_stats_rep_ens_std, _, _, combined_stats_overall_ens = ensmble_and_rep_stats(tmp)
    
    #combine mean and std results
    new_res_test = combine_results(combined_stats_rep_ens, combined_stats_rep_ens_std, combined_stats_overall_ens,
                                   column="test_Final")
    if "validation" in tmp["set"].unique().tolist():
        new_res_val = combine_results(combined_stats_rep_ens, combined_stats_rep_ens_std, combined_stats_overall_ens,
                                  column="validation_Final")
    else:
        new_res_val = pd.DataFrame(columns=new_res_test.columns)
        
    new_res = pd.concat([new_res_test, new_res_val])
    return new_res


# In[ ]:


#for each scenario, try all possible ensmble combinations and record results for validation and testing set

sum_res_file = "Summary_results_multi_method_ensmbles_13Nov2020.csv"
all_res=[]
for scenario in tqdm(results_CNN["scenario"].unique().tolist()):
    #print(scenario)
    methds = results_CNN[results_CNN["scenario"]==scenario]["method"].unique().tolist()
    
    #run ensmble models with all possible variations on methods
    combos = list(powerset(methds))
    for combo in tqdm(combos):
        #print(scenario, combo)
        all_res.append(run_ensmbl_stats(results_CNN, combo))
    #save results after each scenario in case of falures
    out_res=pd.concat(all_res)
    #print(out_res)
    out_res.to_csv(results_dir+sum_res_file, index=False)


# In[ ]:


#save results to file
out_res = out_res.reset_index(drop=True)
out_res.to_csv(results_dir+sum_res_file, index=False)


# In[ ]:


out_res


# In[ ]:





# In[ ]:


#find the best ensmbles based on test set
tmp = out_res[out_res["set"]=="test_Final"].reset_index(drop=True).copy()
#do some orginizational magic
tmp=tmp.pivot(index="method",columns="Mean/std").sort_values([("scenario","Mean"),("RMSE","Mean")]).copy()
tmp = pd.DataFrame(tmp.stack()).reset_index()
tmp.drop_duplicates(["scenario","Mean/std"])


# In[ ]:


#find the best ensmbles based on validation set
tmp = out_res[out_res["set"]=="validation_Final"].reset_index(drop=True).copy()
#do some orginizational magic
tmp = tmp.pivot(index="method",columns="Mean/std").sort_values([("scenario","Mean"),("RMSE","Mean")]).copy()
tmp = pd.DataFrame(tmp.stack()).reset_index()
val_picks=tmp.drop_duplicates(["scenario","Mean/std"])["method"].unique().tolist()
#tmp.drop_duplicates(["scenario","Mean/std"])


# In[ ]:


out_res[(out_res["set"]=="test_Final") & (out_res["method"].isin(val_picks))]


# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:


'''
#try different combinations by hand
#tmp = results_CNN[results_CNN["method"].isin(["GEM_reps_val_training_13_Dec2019","GE_GR_13_Dec2019"])]
tmp = results_CNN[results_CNN["method"].isin(["reps_val_training_NO_HIST_13_Dec2019",
                                              #"G_GR_13_Dec2019",
                                              #"E_GR_13_Dec2019",
                                              #"GE_GR_13_Dec2019",
                                              #"GxE_GR_13_Dec2019",
                                              "GEM_reps_val_training_13_Dec2019"])]
#tmp = results_CNN[results_CNN["scenario"]=="GEM hard"]

for method1 in tmp["method"].unique():
    print(method1, "\t", len(tmp[(tmp["set"]=="test") & (tmp["method"]==method1) & (tmp["split"]!='S_Historical')]))

combined_stats_rep_excluded, combined_stats_rep_excluded_std, _, _, combined_stats_overall = ensmble_and_rep_stats(tmp)
'''


# In[ ]:


'''
#combine mean and std results
new_res = pd.concat([combined_stats_rep_excluded[["test_Final"]],
                     combined_stats_rep_excluded_std[["test_Final"]],
                     combined_stats_overall[["test_Final"]]], axis=1)
new_res.columns = ["Mean","Std","Overall"]
new_res = new_res.dropna().T
n_folds = len(tmp[tmp["split"]!="Historical"]["split"].unique())
new_res["n_folds"]=n_folds
new_res = new_res.reset_index()
new_res.rename(columns ={"index":"Mean/std"}, inplace = True)
new_res["method"] = method
new_res.loc[2,"n_folds"]=np.nan
new_res = new_res[["method"]+new_res.columns[:-1].tolist()]
new_res["last_update"]=datetime.now().strftime('%m%d%Y_%H%M%S')
'''


# In[ ]:


#new_res


# In[ ]:


'''
sum_res_file = "Summary_results_multi_method_ensmbles_13Nov2020.csv"

#only use the first time you create the file then comment out
#new_res.to_csv(results_dir+sum_res_file, index=False)

#read in previously save summary results file
sum_res = pd.read_csv(results_dir+sum_res_file)
'''


# In[ ]:


'''
#add new results to file while replace old ones from same method
sum_res = pd.concat([sum_res[sum_res["method"]!=method],new_res])
#save to file
sum_res.to_csv(results_dir+sum_res_file, index=False)
'''


# In[ ]:





# In[ ]:


#sum_res[sum_res["Mean/std"] == "Mean"][["method","prsn","slope","r2","RMSE","rRMSE","normRMSE","n_folds"]]


# In[ ]:


#sum_res[sum_res["Mean/std"] == "Std"][["method","prsn","slope","r2","RMSE","rRMSE","normRMSE","n_folds"]]


# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:




