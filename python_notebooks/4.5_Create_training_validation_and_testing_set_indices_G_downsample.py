#!/usr/bin/env python
# coding: utf-8

# In[1]:


import pandas as pd
import numpy as np
import os, sys
import random
from math import ceil
from matplotlib import pyplot as plt
import json
from sklearn.model_selection import KFold


# In[2]:


#read in index file (created in previous script)
indices = pd.read_csv("../data/index_file.csv", index_col=[0], parse_dates=["Date Planted"], low_memory=False)


# In[4]:


#create Historical data splits
hist_indices = indices[indices["G2F"]==False].copy()
G2F_indices = indices[indices["G2F"]==True].copy()

#test set - years 2014-present for all counties, Do not hold out G2F counties accross all years in practical case.
#idx1=hist_indices[hist_indices["Year"]>2013].copy().index.tolist()
#G2F_counties = (G2F_indices["State"]+G2F_indices["County"]).unique().tolist()
#idx2 = hist_indices[(hist_indices["State"]+hist_indices["County"]).isin(G2F_counties)].index.tolist()
idx1 = random.sample(hist_indices.index.tolist(), int(len(hist_indices)*0.1))
hist_test = indices.loc[idx1].copy()

#Val set - random 10% from samples not in test set
not_test = hist_indices[hist_indices.index.isin(hist_test.index.tolist())==False].index.tolist()
hist_val = random.sample(not_test,int(len(hist_indices)*0.1))
hist_val = indices.loc[sorted(hist_val)].copy()

#test set - everything not in test or validation set
hist_train = hist_indices[hist_indices.index.isin(hist_test.index.tolist()+hist_val.index.tolist())==False].copy()
print(len(hist_train), len(hist_val), len(hist_test))
print(str(round(100*len(hist_train)/len(hist_indices),1))+"%",
      str(round(100*len(hist_val)/len(hist_indices),1))+"%",
      str(round(100*len(hist_test)/len(hist_indices),1))+"%")
#sainity checks
#print([x for x in hist_test.index.tolist() if x in hist_train.index.tolist()])
#print([x for x in hist_test.index.tolist() if x in hist_val.index.tolist()])
hist_sets={"Historical":{"train": hist_train.index.tolist(),
                         "val": hist_val.index.tolist(),
                         "test": hist_test.index.tolist()}}


# In[6]:


#create G2F data splits Split genotypes 


# In[7]:


#find total number of replicates for each genotype accross all environments
geno_rep = G2F_indices.pivot_table(index=["Pedigree"], values="ID_Year", aggfunc=np.count_nonzero)
geno_rep.columns = ["reps"]
geno_rep = geno_rep.sort_values(["reps"])

#find total number of genotypes and samples that would be included for a given n genotype replication cutoff
n_inclusion_stats=[]
for n in range(1, geno_rep["reps"].max()):
    n_inclusion_stats.append([n, len(geno_rep[geno_rep["reps"]>=n]), len(geno_rep[geno_rep["reps"]>=n])*n])
n_inclusion_stats = pd.DataFrame(n_inclusion_stats, columns = ["n","#genotypes","#samples"])

#n_inclusion_stats.plot()
#n_inclusion_stats[n_inclusion_stats["#samples"] == n_inclusion_stats["#samples"].max()]
#pd.set_option('display.max_rows', 500)
#n_inclusion_stats
###decided to pick n=12 because it leaves us with a high number of samples and genotypes
n=12
print(n_inclusion_stats[n_inclusion_stats["n"]==n])
kept_genos = geno_rep[geno_rep["reps"]>=n].index.tolist() #genotypes to use in model
print(len(kept_genos))

#randomly downsample based on chosen n
#create training, testing, and validation sets
kept_ind=[]
for geno in kept_genos:
    #print(geno)
    kept_ind = kept_ind + random.sample(G2F_indices[G2F_indices["Pedigree"]==geno].index.tolist(),n)
kept_ind = G2F_indices.loc[kept_ind].copy()
print(len(kept_genos), len(kept_ind))


# In[8]:


#k-folds training and testing sets with random genotypes
n_splits=50
kf = KFold(n_splits=n_splits, shuffle=True)
sets={}
hld_year_geno=[]
fold=0
for train_index, test_index in kf.split(kept_genos):
    fold+=1
    test_genos = np.array(kept_genos)[test_index].tolist()
    test_set = kept_ind[kept_ind["Pedigree"].isin(test_genos)]
    train_set = kept_ind[kept_ind["Pedigree"].isin(test_genos)==False]
    #create validation set
    tmp_ids=sorted(random.sample(train_set.index.tolist(), int(0.05*len(train_set.index.tolist()))))
    val_set=train_set.loc[tmp_ids].copy()
    train_set=train_set[train_set.index.isin(tmp_ids)==False]
    #sanity checks
    if len(train_set[train_set["Pedigree"].isin(test_set["Pedigree"].unique().tolist())]) !=0:
        print("CONTAMINATED SETS: Genotype")
    sets[str(fold)]={"train":train_set.index.tolist(),
                         "val":val_set.index.tolist(),
                         "test":test_set.index.tolist()}
    hld_year_geno.append([fold,len(train_set),len(val_set),len(test_set)])


# In[10]:


#combine historical and G2F data sets into a single dictionary
sets.update(hist_sets)
len(sets)


# In[11]:


#pd.DataFrame(hld_year_geno, columns=["Fold","Train","Val","Test"])


# In[12]:


#save sets to json file
with open('../data/Train_val_test_sets_G_dwnSample'+str(n)+"_23Apr2020.json", 'w') as fp:
    json.dump(sets, fp)
    


# In[11]:


for iterset in sets:
    print(iterset)


# In[ ]:





# In[ ]:




