#!/usr/bin/env python
# coding: utf-8

# In[1]:


import pandas as pd
import numpy as np
import os, sys
import random
from math import ceil
from matplotlib import pyplot as plt
import json


# In[2]:


#read in index file (created in previous script)
indices = pd.read_csv("../data/index_file.csv", index_col=[0], parse_dates=["Date Planted"], low_memory=False)
#read in genotype clustering file
geno_clusters = pd.read_csv("../data/Genotype_data/genotype_clusters_Guillaume_27Sept2019.csv")
print(len(indices))


# In[6]:


#create Historical data splits
hist_indices = indices[indices["G2F"]==False].copy()
G2F_indices = indices[indices["G2F"]==True].copy()

#test set - years 2014-present for all counties, Do not hold out G2F counties accross all years in practical case.
idx1=hist_indices[hist_indices["Year"]>2013].copy().index.tolist()
#G2F_counties = (G2F_indices["State"]+G2F_indices["County"]).unique().tolist()
#idx2 = hist_indices[(hist_indices["State"]+hist_indices["County"]).isin(G2F_counties)].index.tolist()
hist_test = indices.loc[idx1].copy()

#Val set - random 10% from samples not in test set
not_test = hist_indices[hist_indices.index.isin(hist_test.index.tolist())==False].index.tolist()
hist_val = random.sample(not_test,int(len(hist_indices)*0.1))
hist_val = indices.loc[sorted(hist_val)].copy()

#test set - everything not in test or validation set
hist_train = hist_indices[hist_indices.index.isin(hist_test.index.tolist()+hist_val.index.tolist())==False].copy()
print(len(hist_train), len(hist_val), len(hist_test), len(hist_train)+len(hist_val)+len(hist_test))
print(str(round(100*len(hist_train)/len(hist_indices),1))+"%",
      str(round(100*len(hist_val)/len(hist_indices),1))+"%",
      str(round(100*len(hist_test)/len(hist_indices),1))+"%")
#sainity checks
#print([x for x in hist_test.index.tolist() if x in hist_train.index.tolist()])
#print([x for x in hist_test.index.tolist() if x in hist_val.index.tolist()])
hist_sets={"Historical":{"train": hist_train.index.tolist(),
                         "val": hist_val.index.tolist(),
                         "test": hist_test.index.tolist()}}


# In[8]:


#create G2F data splits


# In[9]:


sets={}
hld_year_geno=[]
for year in G2F_indices["Year"].unique():
    for rep in range(0,10):
        test_set = G2F_indices[G2F_indices["Year"]==year].copy()
        genos_in_year = test_set["Pedigree"].unique().tolist()
        geno_set = random.sample(genos_in_year, int(len(genos_in_year)*0.1))
        test_set = test_set[test_set["Pedigree"].isin(geno_set)]
        #hold out all genotypes sampled above as well as close relatives (those within the same cluster)
        hld_clusters = geno_clusters[geno_clusters["genotype"].isin(test_set["Pedigree"].unique())]["cluster"].unique().tolist()
        #genotypes to exclude from training set 
        non_train_genos = geno_clusters[geno_clusters["cluster"].isin(hld_clusters)]["genotype"].unique().tolist()
        train_set = G2F_indices[G2F_indices["Year"]!=year].copy()
        train_set = train_set[train_set["Pedigree"].isin(non_train_genos)==False]
        #create validation set
        tmp_ids=sorted(random.sample(train_set.index.tolist(), int(0.05*len(train_set.index.tolist()))))
        val_set=train_set.loc[tmp_ids].copy()
        train_set=train_set[train_set.index.isin(tmp_ids)==False]
        #sanity checks
        if len(train_set[train_set["Year"].isin(test_set["Year"].unique().tolist())]) !=0:
            print("CONTAMINATED SETS: Year")
        if len(train_set[train_set["Pedigree"].isin(test_set["Pedigree"].unique().tolist())]) !=0:
            print("CONTAMINATED SETS: Genotype")
        sets[str(year)+"@"+str(rep)]={"train":train_set.index.tolist(),
                     "val":val_set.index.tolist(),
                     "test":test_set.index.tolist()}
        #record data for diagnostic purposes
        hld_year_geno.append([year, rep, len(test_set["Pedigree"].unique())/len(genos_in_year),
                                    len(train_set["Pedigree"].unique())/len(G2F_indices["Pedigree"].unique()),
                                    len(train_set), len(val_set), len(test_set)])
    #print(year, len(test_set), len(test_set["Pedigree"].unique())/len(genos_in_year), len(hld_clusters))
    #print(year, len(val_set))
    #print(year, len(train_set), len(train_set["Pedigree"].unique())/len(G2F_indices["Pedigree"].unique()))


# In[10]:


#combine historical and G2F data sets into a single dictionary
sets.update(hist_sets)
len(sets)


# In[12]:


#save sets to json file
with open('../data/Train_val_test_sets_Practical_GEM_26Feb2020.json', 'w') as fp:
    json.dump(sets, fp)


# In[23]:


#example of how to open json file for further use
#with open('../data/Train_val_test_sets_1_Oct2019.json', 'r') as fp:
#    test = json.load(fp)


# In[ ]:




