#!/usr/bin/env python
# coding: utf-8

# In[1]:


import os, sys
import pandas as pd
import requests
import datetime
import numpy as np
import time
from concurrent.futures import ProcessPoolExecutor, ThreadPoolExecutor
import multiprocessing


# In[2]:


def fix_leap_years(daymet, STARTYEAR, ENDYEAR):
    #fix leap years. Daymet includes leapday, but deletes Dec 31 to make all years have 365 days.
    leap_years=[]
    for yr in range(STARTYEAR, ENDYEAR+1):
        if yr % 4 != 0: continue
        elif yr % 100 != 0: leap_years.append(yr)
        elif yr % 400 != 0: continue
        else: leap_years.append(yr)
    #add Dec 31 back into leap years as the average values for the next and previous 5 days 
    days_to_add=[]
    for yr in leap_years:
        dec31 = daymet[((daymet["year"]==yr) & (daymet["day"].isin(range(361,366)))) |
                       ((daymet["year"]==yr+1) & (daymet["day"].isin(range(1,6))))].mean().tolist()
        days_to_add.append([yr,366]+dec31[2:])
    days_to_add = pd.DataFrame(days_to_add, columns=["year", "day", "radn", "maxt", "mint", "rain", "snow", "vp", "dayL"])
    return days_to_add


# In[3]:


def multiprocess(func, args, workers):
    begin_time = time.time()
    with ProcessPoolExecutor(max_workers=workers) as executor:
        res = executor.map(func, args, [begin_time for i in range(len(args))])
    return list(res)
def download_daymets(i, base):
    start = time.time() - base
    curr_url = DAYMET_URL_STR.format(lats[i], lons[i]) + var_str + years_str
    #print("Processing:", curr_url)
    res = requests.get(curr_url)
    if not res.ok:
        print("Could not access the following URL:", curr_url)
        print(names[i])
        outFname = "None"
    else:
        if names[i] == "NULL":
            outFname = res.headers["Content-Disposition"].split("=")[-1]
        else:
            outFname = names[i]
        text_str = res.content
        outF = open(weath_dir+"tmp_daymet/"+outFname+".csv", 'wb')
        outF.write(text_str)
        outF.close()
        res.close()
        #num_downloaded += 1
    stop = time.time() - base
    print(outFname, stop-start, (stop-start)/60, (stop-start)/60/60)
    return start, stop
#print("Finished downloading", num_downloaded, "files.")


# In[4]:


def daymet_met(infile, base):
    start = time.time() - base
    Loc_Lat_Lon_Ele = [infile[:-4]]
    for line in open(weath_dir+"tmp_daymet/"+infile, "r"):
        if line[:8] == "Latitude":
            Loc_Lat_Lon_Ele.append(line.split(" ")[1])
            Loc_Lat_Lon_Ele.append(line.split(" ")[4].strip())
        if line[:9] == "Elevation":
            Loc_Lat_Lon_Ele.append(line.split(" ")[1])
    daymet = pd.read_csv(weath_dir+"tmp_daymet/"+infile, skiprows=7)
    daymet["year"] = daymet["year"].astype('int')
    daymet["yday"] = daymet["yday"].astype('int')
    daymet.rename(columns={"yday":"day", "tmax (deg c)":"maxt", "tmin (deg c)":"mint","prcp (mm/day)":"rain",
                           "swe (kg/m^2)":"snow"}, inplace=True)
    daymet["radn"]=(daymet["srad (W/m^2)"]*daymet["dayl (s)"])/1000000
    daymet["vp"]=daymet["vp (Pa)"]/1000
    daymet["dayL"] = daymet["dayl (s)"]/3600
    daymet = daymet[["year", "day", "radn", "maxt", "mint", "rain", "snow", "vp", "dayL"]]

    #calculate tav and amp
    daymet["avgt"]=(daymet["maxt"]+daymet["mint"])/2
    tav = daymet.groupby("year")["avgt"].mean().mean()
    date=[]
    for row in range(0, len(daymet)):
        date.append((datetime.datetime(daymet.loc[row]["year"].astype('int'), 1, 1) + datetime.timedelta(daymet.loc[row]["day"] - 1)).strftime("%m"))
    daymet["mnth"]=date
    daymet["date"]=daymet["mnth"].astype('str')+"_"+daymet["year"].astype('str')
    amp = daymet.groupby("mnth")["avgt"].mean().max() - daymet.groupby("mnth")["avgt"].mean().min()

    #write out met file
    handle = open(weath_dir+infile[:-4]+".met","w")
    handle.write("[weather.met.weather]\n")
    handle.write("Location = "+infile[:-4]+"\n")
    handle.write("latitude = "+Loc_Lat_Lon_Ele[1]+" (DECIMAL DEGREES)\n")
    handle.write("longitude = "+Loc_Lat_Lon_Ele[2]+" (DECIMAL DEGREES)\n")
    handle.write("Elevation = "+Loc_Lat_Lon_Ele[3]+" (meters above sea level)\n")
    handle.write("tav = "+str(tav)+" (oC) ! annual average ambient temperature\n")
    handle.write("amp = "+str(amp)+" (oC) ! annual amplitude in mean monthly temperature\n")
    handle.write("! source: http://daymet.ornl.gov/\n! Daymet Software Version 3.0; Daymet Data Version 3.0\n! 1979 is average of other years\n")
    daymet = daymet[["year", "day", "radn", "maxt", "mint", "rain", "snow", "vp", "dayL"]]
    handle.write(" ".join(daymet.columns.tolist())+"\n")
    handle.write(" ".join(["()", "()", "(MJ/m^2)", "(oC)", "(oC)", "(mm)", "(mm)", "(kPa)", "(hours)"])+"\n")
    handle.close()

    avg_yr = daymet.groupby("day")[["radn", "maxt", "mint", "rain", "snow", "vp", "dayL"]].mean()
    avg_yr["day"] = avg_yr.index.tolist()
    avg_yr["year"] = 1979
    avg_yr.reset_index(drop=True, inplace=True)
    daymet = pd.concat([avg_yr,daymet],
                       sort=True).reset_index(drop=True)[["year", "day", "radn", "maxt", "mint", "rain", "snow", "vp", "dayL"]]
    days_to_add = fix_leap_years(daymet, STARTYEAR, ENDYEAR)
    daymet = pd.concat([daymet, days_to_add], sort=True).sort_values(by=["year","day"])
    daymet.reset_index(drop=True, inplace=True)
    daymet[["year", "day", "radn", "maxt", "mint", "rain", "snow", "vp", "dayL"]].to_csv(weath_dir+infile[:-4]+".met", header=False, index=False, sep=" ",mode="a")
    stop = time.time() - base
    print(infile, stop-start, (stop-start)/60, (stop-start)/60/60)
    return start, stop


# In[5]:


#set working directory for weather data
weath_dir="../data/Weather_data/"

#set dataset variable (choose between Historical and G2F)
#dataset="Historical"
dataset="G2F"

if dataset=="Historical":
    ####import working data set from other script (Historical data)
    for_daymet = pd.read_csv("../data/Phenotype_data/work_set_yields_by_county.csv")
    for_daymet = for_daymet[["ID","Latitude","Longitude"]].drop_duplicates().reset_index(drop=True)
    for_daymet = for_daymet[for_daymet["Latitude"].isna()==False].reset_index(drop=True)
    #for_daymet = for_daymet.iloc[:4] #uncomment this to test on small number of counties

elif dataset=="G2F":
    ####import working data set from other script (G2F data)
    for_daymet = pd.read_csv("../data/Phenotype_data/G2F_work_set_2014_2017.csv")
    #drop unneeded data
    for_daymet = for_daymet[["Location_Year","North","East"]].drop_duplicates().reset_index(drop=True)
    for_daymet.rename(columns={"Location_Year": "ID", "North": "Latitude", "East": "Longitude"}, inplace=True)


# In[6]:


for_daymet


# In[7]:


#setup details for download
STARTYEAR = 1980
ENDYEAR   = 2018
NO_NAME = "NULL"
YEAR_LINE = "years:"
VAR_LINE  = "variables:"
DAYMET_VARIABLES = ['dayl', 'prcp', 'srad', 'swe', 'tmax', 'tmin', 'vp']
DAYMET_YEARS     = [str(year) for year in range(STARTYEAR, ENDYEAR + 1)]
DAYMET_URL_STR = r'https://daymet.ornl.gov/single-pixel/api/data?lat={}&lon={}'
lats  = for_daymet["Latitude"].tolist()
lons  = for_daymet["Longitude"].tolist()
names = for_daymet["ID"].tolist()
requested_vars = ",".join(DAYMET_VARIABLES)
requested_years = ",".join(DAYMET_YEARS)
os.system("mkdir "+weath_dir+"tmp_daymet/")


# In[8]:


#run acctual download
var_str = ''
if requested_vars:
    var_str = "&measuredParams=" + requested_vars

years_str = ''
if requested_years:
    years_str = "&year=" + requested_years

num_files_requested = len(lats)
num_downloaded = 0
'''
for i in range(num_files_requested):
    sys.stdout.write('\r')
    sys.stdout.write(str(i)+" / "+str(num_files_requested))
    sys.stdout.flush()
    results1 = download_daymets(i, time.time())
'''
results = multiprocess(download_daymets, range(num_files_requested), multiprocessing.cpu_count()-1)
#convert daymet files to apsim met file format
infiles = [x for x in os.listdir(weath_dir+"tmp_daymet/") if x[:-4] in for_daymet["ID"].tolist()]
print("processing weather files")
results = multiprocess(daymet_met, infiles, multiprocessing.cpu_count()-1)
#os.system("rm -r "+weath_dir+"tmp_daymet/")


# In[9]:


#Preprocess weather data and place in a single file
met_data=[]
for ID in [x[:-4] for x in os.listdir(weath_dir) if x[-4:]==".met" and x[:-4] in for_daymet["ID"].tolist()]:
    print(ID)
    site=pd.read_csv("../data/Weather_data/"+ID+".met", skiprows=list(range(0,10))+[11],header=0, delimiter="\s+")
    site["precip"]=site["rain"]+site["snow"]
    site = site[['year', 'day', 'radn', 'maxt', 'mint', 'precip', 'vp', 'dayL']]
    site = site.pivot(index="year", columns="day").stack(level=0).reset_index()
    site["ID"]=ID
    site = site[["ID","year","level_1"]+[x for x in site.columns if x not in ["ID","year","level_1"]]]
    del site.columns.name
    site = site.dropna(axis=1)
    met_data.append(site.copy())
met_data = pd.concat(met_data)


# In[10]:


#save final dataset to file
if dataset=="Historical":
    met_data.to_csv("../data/Weather_data/All_met_data_365.csv")

elif dataset=="G2F":
    met_data.to_csv("../data/Weather_data/G2F_all_met_data_365.csv")


# In[ ]:




