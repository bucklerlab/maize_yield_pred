#!/usr/bin/env python
# coding: utf-8

# In[1]:


import os, sys
import pandas as pd
import json
import numpy as np
from scipy.stats import pearsonr
from scipy.stats import spearmanr
from scipy.stats import linregress
import matplotlib.pyplot as plt
from datetime import datetime


# In[2]:


def get_performance_states(results):
    performance_stats=[]
    for split in results["split"].unique():
        tmp = results[results["split"]==split].copy()
        if len(tmp)<2:continue
        real = tmp["Observed"]
        PR = tmp["Predicted"]
        prsn = pearsonr(np.array(real), np.array(PR))
        sprmn = spearmanr(np.array(real), np.array(PR))
        prsn = prsn[0]
        sprmn = sprmn[0]
        slope = linregress(real,PR)[0]
        r2 = prsn**2
        mae = np.mean(np.abs(np.array(PR) - np.array(real)))
        rMAE = mae / real.mean()
        normMAE = mae / (real.max()-real.min())
        RMSE = np.sqrt(np.mean(np.square(np.array(PR) - np.array(real))))
        rRMSE = RMSE / np.mean(np.array(real))
        normRMSE = RMSE/(real.max()-real.min())
        #based on https://en.wikipedia.org/wiki/Root-mean-square_deviation
        performance_stats.append([split, prsn, sprmn, slope, r2, mae, rMAE, normMAE, RMSE, rRMSE, normRMSE, len(PR)])
    performance_stats = pd.DataFrame(performance_stats, columns=["split", "prsn", "sprmn", "slope", "r2", "mae", "rMAE",
                                                                 "normMAE", "RMSE", "rRMSE", "normRMSE","n"])
    return performance_stats

def get_performance_stats_overall(results):
    performance_stats=[]
    tmp = results.copy()
    #if len(tmp)<2:continue
    real = tmp["Observed"]
    PR = tmp["Predicted"]
    prsn = pearsonr(np.array(real), np.array(PR))
    sprmn = spearmanr(np.array(real), np.array(PR))
    prsn = prsn[0]
    sprmn = sprmn[0]
    slope = linregress(real,PR)[0]
    r2 = prsn**2
    mae = np.mean(np.abs(np.array(PR) - np.array(real)))
    rMAE = mae / real.mean()
    normMAE = mae / (real.max()-real.min())
    RMSE = np.sqrt(np.mean(np.square(np.array(PR) - np.array(real))))
    rRMSE = RMSE / np.mean(np.array(real))
    normRMSE = RMSE/(real.max()-real.min())
    #based on https://en.wikipedia.org/wiki/Root-mean-square_deviation
    performance_stats.append([np.nan, prsn, sprmn, slope, r2, mae, rMAE, normMAE, RMSE, rRMSE, normRMSE, len(PR)])
    performance_stats = pd.DataFrame(performance_stats, columns=["split", "prsn", "sprmn", "slope", "r2", "mae", "rMAE",
                                                                 "normMAE", "RMSE", "rRMSE", "normRMSE","n"])
    return performance_stats


# In[3]:


#calculate stats for all sets for a given method
def ensmble_and_rep_stats(results_CNN):
    if len(results_CNN["method"].unique()) > 1:
        print("Error: You have provided results from multiple methods.")
    by_split_rep=[]
    combined_stats=[]
    combined_stats_std=[]
    combined_stats_overall=[]
    combined_stats_sng_reps=[]
    for tset in results_CNN["set"].unique():
        print(tset)
        sng_set = results_CNN[(results_CNN["set"]==tset)].copy()
        #average across reps
        results_sng_pvt = sng_set.pivot_table(index=["split","index"]).reset_index()
        #get performance data
        perf_stats_reps = get_performance_states(results_sng_pvt)
        perf_stats_reps_overall = get_performance_stats_overall(results_sng_pvt[results_sng_pvt["split"]!="S_Historical"])
        combined_stats.append(pd.DataFrame(perf_stats_reps[perf_stats_reps["split"]=="S_Historical"].mean(),
                                           columns=[tset+"_Hist"]))
        combined_stats.append(pd.DataFrame(perf_stats_reps[perf_stats_reps["split"]!="S_Historical"].mean(),
                                           columns=[tset+"_Final"]))
        combined_stats_std.append(pd.DataFrame(perf_stats_reps[perf_stats_reps["split"]=="S_Historical"].std(),
                                           columns=[tset+"_Hist"]))
        combined_stats_std.append(pd.DataFrame(perf_stats_reps[perf_stats_reps["split"]!="S_Historical"].std(),
                                           columns=[tset+"_Final"]))
        
        combined_stats_overall.append(pd.DataFrame(perf_stats_reps_overall[perf_stats_reps_overall["split"]!="S_Historical"].mean(),
                                           columns=[tset+"_Final"]))
        
        #get stats by rep
        for rep in sng_set["rep"].unique():
            #print(rep)
            sng_set_rep = sng_set[sng_set["rep"]==rep].copy()
            perf_stats_sng_rep = get_performance_states(sng_set_rep)
            combined_stats_sng_reps.append(pd.DataFrame(perf_stats_sng_rep[perf_stats_sng_rep["split"]=="S_Historical"].mean(),
                                                        columns=[tset+"_rep"+str(rep)+"_Hist"]))
            combined_stats_sng_reps.append(pd.DataFrame(perf_stats_sng_rep[perf_stats_sng_rep["split"]!="S_Historical"].mean(),
                                                        columns=[tset+"_rep"+str(rep)+"_Final"]))
            perf_stats_sng_rep["set"]=tset
            perf_stats_sng_rep["rep"]=rep
            by_split_rep.append(perf_stats_sng_rep)
    combined_stats = pd.concat(combined_stats, axis=1)
    combined_stats_std = pd.concat(combined_stats_std, axis=1)
    combined_stats_overall = pd.concat(combined_stats_overall, axis=1)
    combined_stats_sng_reps = pd.concat(combined_stats_sng_reps, axis=1)
    mult_ind = pd.DataFrame(combined_stats_sng_reps.columns)
    mult_ind = mult_ind[0].str.split("_", expand=True)
    mult_ind.columns = ["set","rep","model"]
    mult_ind["rep"] = mult_ind["rep"].str.replace("rep","").astype(int)
    combined_stats_sng_reps.columns = pd.MultiIndex.from_frame(mult_ind)
    combined_stats_sng_reps = combined_stats_sng_reps.T.reset_index()
    by_split_rep = pd.concat(by_split_rep)
    return combined_stats, combined_stats_std, combined_stats_sng_reps, by_split_rep, combined_stats_overall


# In[4]:


#find out what files and methods are available
results_dir="../data/Results/"
res_file_details = pd.DataFrame([x for x in os.listdir(results_dir) if x[:19]=="Output_predictions_"], columns=["File"])
res_file_details["file_short"] = res_file_details["File"].str[19:-4]
res_file_details["method"] = res_file_details["File"].str[19:-4]
res_file_details["method"] = res_file_details["method"].str.replace("\d+-\d+_","")
res_file_details["method"] = res_file_details["method"].str.replace("\d+_\d+_","")
times = []
for file1 in res_file_details["File"]:
    times.append(datetime.fromtimestamp(os.path.getmtime(results_dir+"/"+file1)))
res_file_details["mtime"]=times
#res_file_details["mtime"] = [os.path.getmtime(results_dir+"/"+file1) for file1 in res_file_details["File"].tolist()]
#res_file_details["method"] = res_file_details["method"].str.replace("reps_val_training_","")

res_file_details["scenario"] = res_file_details["method"].str[-10:]
repl = {'_23Apr2020':"G holdout", '13_Dec2019':"GEM hard", '_26Feb2020':"GEM Practical", '_24Apr2020':"E holdout"}
for key in repl:
    #print(key)
    res_file_details["scenario"] = res_file_details["scenario"].str.replace(key, repl[key])

#display list of files/methods
pd.set_option('display.max_rows', 200)
res_file_details[["method","file_short","File","scenario", "mtime"]].sort_values(["mtime","method","file_short"])


# In[5]:


#choose method for further analysis


#method="reps_val_training_NO_SOIL_13_Dec2019"
#method="reps_val_training_NO_G_13_Dec2019"
#method="reps_val_training_NO_WEATH_13_Dec2019"
#method="reps_val_training_NO_G_HIST_13_Dec2019"
#method="reps_val_training_WEATH_only_NO_HIST_13_Dec2019"
#method="reps_val_training_SOIL_only_NO_HIST_13_Dec2019"
#method="reps_val_training_FERT_only_13_Dec2019"
#method="reps_val_training_NO_SOIL_NO_HIST_13_Dec2019"
#method="reps_val_training_FIELD_only_NO_HIST_13_Dec2019"
#method="reps_val_training_RM_LOWSOIL_NO_HIST_13_Dec2019"
#method="reps_val_training_NO_WEATH_NO_HIST_13_Dec2019"
#method="reps_val_training_NO_GYPS_NO_HIST_13_Dec2019"
#method="reps_val_training_NO_GYPS_13_Dec2019"
#method="reps_val_training_RM_LOWSOIL_13_Dec2019"
#method="reps_val_training_MLP_large13_Dec2019"
#method="reps_val_training_ReluFinalWeathCons_13_Dec2019"

method='reps_val_training_Practical_GEM_26Feb2020'
#method='reps_val_training_NO_HIST_Practical_GEM_26Feb2020'
#method='G2FonHist_reps_val_training_Practical_GEM_26Feb2020'
#method='reps_val_training_NO_SOIL_Practical_GEM_26Feb2020'
#method='reps_val_training_NO_G_Practical_GEM_26Feb2020'
#method='reps_val_training_NO_WEATH_Practical_GEM_26Feb2020'
#method='reps_val_training_NO_G_HIST_Practical_GEM_26Feb2020'
#method='reps_val_training_WEATH_only_NO_HIST_Practical_GEM_26Feb2020'
#method='reps_val_training_SOIL_only_NO_HIST_Practical_GEM_26Feb2020'
#method='reps_val_training_FERT_only_Practical_GEM_26Feb2020'
#method='reps_val_training_NO_SOIL_NO_HISTPractical_GEM_26Feb2020'
#method='reps_val_training_FIELD_only_NO_HIST_Practical_GEM_26Feb2020'
#method='reps_val_training_RM_LOWSOIL_NO_HIST_Practical_GEM_26Feb2020'
#method='reps_val_training_NO_WEATH_NO_HIST_Practical_GEM_26Feb2020'
#method='reps_val_training_NO_GYPS_NO_HIST_Practical_GEM_26Feb2020'
#method='reps_val_training_RM_LOWSOIL_Practical_GEM_26Feb2020'
#method='reps_val_training_NO_GYPS_Practical_GEM_26Feb2020'
#method='reps_val_training_MLP_largePractical_GEM_26Feb2020'
#method='reps_val_training_tanhPractical_GEM_26Feb2020'
#method='reps_val_training_tanh_final_Practical_GEM_26Feb2020'
#method='reps_val_training_ReluFinalWeathCons_Practical_GEM_26Feb2020'

#method="reps_val_training_E_dwnSample293_24Apr2020"
#method="reps_val_training_NO_HIST_E_dwnSample293_24Apr2020"
#method="G2FonHist_reps_val_training_E_dwnSample293_24Apr2020"
#method="reps_val_training_NO_SOIL_E_dwnSample293_24Apr2020"
#method="reps_val_training_NO_G_E_dwnSample293_24Apr2020"
#method="reps_val_training_NO_WEATH_E_dwnSample293_24Apr2020"
#method="reps_val_training_NO_G_HIST_E_dwnSample293_24Apr2020"
#method="reps_val_training_WEATH_only_NO_HIST_E_dwnSample293_24Apr2020"
#method="reps_val_training_SOIL_only_NO_HIST_E_dwnSample293_24Apr2020"
#method="reps_val_training_FERT_only_E_dwnSample293_24Apr2020"
#method="reps_val_training_FIELD_only_NO_HIST_E_dwnSample293_24Apr2020"
#method="reps_val_training_NO_SOIL_NO_HIST_E_dwnSample293_24Apr2020"
#method="reps_val_training_NO_WEATH_NO_HIST_E_dwnSample293_24Apr2020"
#method="reps_val_training_RM_LOWSOIL_NO_HIST_E_dwnSample293_24Apr2020"
#method="reps_val_training_NO_GYPS_NO_HIST_E_dwnSample293_24Apr2020"
#method="reps_val_training_RM_LOWSOIL_E_dwnSample293_24Apr2020"
#method="reps_val_training_NO_GYPS_E_dwnSample293_24Apr2020"
#method="reps_val_training_MLP_largeE_dwnSample293_24Apr2020"
#method="reps_val_training_ReluFinalWeathCons_E_dwnSample293_24Apr2020"

#method='reps_val_training_G_dwnSample12_23Apr2020'
#method='reps_val_training_NO_HIST_G_dwnSample12_23Apr2020'
#method="G2FonHist_reps_val_training_G_dwnSample12_23Apr2020"
#method='reps_val_training_NO_SOIL_G_dwnSample12_23Apr2020'
#method='reps_val_training_NO_G_G_dwnSample12_23Apr2020'
#method='reps_val_training_NO_WEATH_G_dwnSample12_23Apr2020'
#method='reps_val_training_NO_G_HIST_G_dwnSample12_23Apr2020'
#method='reps_val_training_WEATH_only_NO_HIST_G_dwnSample12_23Apr2020'
#method='reps_val_training_SOIL_only_NO_HIST_G_dwnSample12_23Apr2020'
#method='reps_val_training_FERT_only_G_dwnSample12_23Apr2020'
#method='reps_val_training_NO_SOIL_NO_HIST_G_dwnSample12_23Apr2020'
#method='reps_val_training_FIELD_only_NO_HIST_G_dwnSample12_23Apr2020'
#method='reps_val_training_RM_LOWSOIL_NO_HIST_G_dwnSample12_23Apr2020'
#method='reps_val_training_NO_WEATH_NO_HIST_G_dwnSample12_23Apr2020'
#method='reps_val_training_NO_GYPS_NO_HIST_G_dwnSample12_23Apr2020'
#method='reps_val_training_RM_LOWSOIL_G_dwnSample12_23Apr2020'
#method='reps_val_training_NO_GYPS_G_dwnSample12_23Apr2020'
#method='reps_val_training_MLP_largeG_dwnSample12_23Apr2020'
#method='reps_val_training_ReluFinalWeathCons_G_dwnSample12_23Apr2020'

#upload results from chosen method
results_files=[]
for file in res_file_details[res_file_details["method"]==method]["File"].tolist():
    tmp = pd.read_csv(results_dir+file)
    tmp = tmp[tmp["index"]!="index"]
    tmp["split"] = tmp["split"].astype("str")
    tmp["rep"] = tmp["rep"].astype("int")
    print(file, len(tmp["split"].unique()), len(tmp["rep"].unique()))
    results_files.append(tmp.copy())
results_CNN = pd.concat(results_files)
results_CNN = results_CNN.apply(pd.to_numeric, errors='ignore')
results_CNN["split"] = "S_"+ results_CNN["split"].astype("str")

#combine replicates from the same experiment
results_CNN["method"] = results_CNN["method"].str.replace("\d+-\d+_","")
results_CNN["method"].unique()


# In[6]:


#exculde the poorest performing x number of reps based on validation set
model="Final"
set1="validation"
#get stats based on validation set
tmp = results_CNN[(results_CNN["method"]==method) & (results_CNN["set"].isin(["validation"]))].copy()
combined_stats, combined_stats_std, combined_stats_sng_reps, by_split_rep, combined_stats_overall = ensmble_and_rep_stats(tmp)
#examine validation stats and determine what to drop
subset = combined_stats_sng_reps[(combined_stats_sng_reps["model"]==model) & (combined_stats_sng_reps["set"]==set1)]
ordered = subset.sort_values("RMSE").reset_index(drop=True)
#ordered = subset.sort_values("prsn", ascending=False)
#ordered


# In[7]:


#set number of worst performers to drop
num_reps_exclud = 6
keep_reps = ordered["rep"].tolist()[:-num_reps_exclud]
keep_reps


# In[8]:


#drop worst performers
tmp = results_CNN[(results_CNN["method"]==method) & (results_CNN["set"].isin(["validation","test"])) &
                                                     (results_CNN["rep"].isin(keep_reps))].copy()
combined_stats_rep_excluded, combined_stats_rep_excluded_std, _, _, combined_stats_overall = ensmble_and_rep_stats(tmp)
#combined_stats_rep_excluded


# In[9]:


#save validation and test obs and pred results for ensamble model
ensmble_out = "For_ensamble_"+method+".csv"
print(ensmble_out)
tmp.pivot_table(index=["set","split","index"]).reset_index().to_csv(results_dir+ensmble_out)


# In[10]:


#combine mean and std results
new_res = pd.concat([combined_stats_rep_excluded[["test_Final"]],
                     combined_stats_rep_excluded_std[["test_Final"]],
                     combined_stats_overall[["test_Final"]]], axis=1)
new_res.columns = ["Mean","Std","Overall"]
new_res = new_res.dropna().T
n_folds = len(tmp[tmp["split"]!="Historical"]["split"].unique())
new_res["n_folds"]=n_folds
new_res = new_res.reset_index()
new_res.rename(columns ={"index":"Mean/std"}, inplace = True)
new_res["method"] = method
new_res.loc[2,"n_folds"]=np.nan
new_res = new_res[["method"]+new_res.columns[:-1].tolist()]
new_res["last_update"]=datetime.now().strftime('%m%d%Y_%H%M%S')


# In[11]:


new_res


# In[12]:


sum_res_file = "Summary_results_all_02Nov2020.csv"

#only use the first time you create the file then comment out
#new_res.to_csv(results_dir+sum_res_file, index=False)

#read in previously save summary results file
sum_res = pd.read_csv(results_dir+sum_res_file)


# In[13]:


#add new results to file while replace old ones from same method
sum_res = pd.concat([sum_res[sum_res["method"]!=method],new_res])

#add extra column with testing scenario
sum_res["scenario"] = sum_res["method"].str[-10:]

repl = {'_23Apr2020':"G holdout", '13_Dec2019':"GEM hard", '_13Dec2019':"GEM hard", '_26Feb2020':"GEM Practical", '_24Apr2020':"E holdout"}
for key in repl:
    #print(key)
    sum_res["scenario"] = sum_res["scenario"].str.replace(key, repl[key])
#sum_res["scenario"].unique()

#do some orginizational magic
tmp=sum_res.pivot(index="method",columns="Mean/std").sort_values([("scenario","Mean"),("RMSE","Mean")]).copy()
sum_res = pd.DataFrame(tmp.stack()).reset_index()

#save to file
sum_res.to_csv(results_dir+sum_res_file, index=False)


# In[14]:


sum_res[sum_res["Mean/std"] == "Mean"][["method","prsn","slope","r2","RMSE","rRMSE","normRMSE","n_folds",
                                        "scenario"]]


# In[15]:


sum_res[sum_res["Mean/std"] == "Std"][["method","prsn","slope","r2","RMSE","rRMSE","normRMSE","scenario"]]


# In[ ]:





# In[ ]:





# In[ ]:




