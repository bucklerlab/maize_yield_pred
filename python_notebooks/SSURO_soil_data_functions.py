#!/usr/bin/env python
# coding: utf-8

# In[1]:


import os, sys
import pandas as pd
import geopandas
import numpy as np
import urllib.request
from dateutil.parser import parse
from fancyimpute import KNN
#much of this script is based on the design found within the FedData R package


# In[2]:


def get_ssurgo_inventory(soil_area, tmp_dir, verbose):
    os.system("mkdir "+tmp_dir)
    #download soil inventory to figure out which survey areas are within AOI
    #if area of interest becomes too large we may have to add a method for breaking it down into smaller areas
    base_url="https://sdmdataaccess.nrcs.usda.gov/Spatial/SDMNAD83Geographic.wfs?Service=WFS&Version=1.0.0&Request=GetFeature&Typename=SurveyAreaPoly&BBOX="
    coords = ",".join(soil_area[["West","South","East","North"]].astype('float').round(5).astype(str).tolist())
    if coords in os.listdir(tmp_dir):
        if verbose:
            print("Using previously downloaded inventory information")
        area_info=geopandas.read_file(tmp_dir+coords)
    else:
        if verbose:
            print("Querying SSURGO to determine which survey area(s) is/are needed.\n" + base_url + coords)
        try:
            area_info = geopandas.read_file(base_url+coords, layer = "surveyareapoly")
            area_info.to_file(tmp_dir+coords)
        #except ConnectionResetError:
        #    area_info = geopandas.read_file(base_url+coords, layer = "surveyareapoly")
        #    area_info.to_file(tmp_dir+coords)
        except:
            if verbose:
                print("Data could not be downloaded")
            #need to add url to a list that can be tried again later...
            area_info=pd.DataFrame()
    return area_info, coords
def download_ssurgo_study_area(area, date, tmp_dir, verbose):
    #download the actual data
    #http://websoilsurvey.sc.egov.usda.gov/DSD/Download/Cache/SSA/wss_SSA_SC001_[2018-09-15].zip
    base_url="http://websoilsurvey.sc.egov.usda.gov/DSD/Download/Cache/SSA/wss_SSA_"
    url=base_url+area+"_["+date+"].zip"
    out_file=area+"_["+date+"].zip"
    if out_file in os.listdir(tmp_dir):
        if verbose:
            print("Using Existing File.")
    else:
        if verbose:
            print("Downloading file: " + url + "\n to: "+tmp_dir+out_file)
        #download file(s)
        urllib.request.urlretrieve(url, tmp_dir+out_file)
    return tmp_dir+out_file


# In[3]:


def get_table_headers():
    ###May add support for this later, for now just using headers created by FedData
    #download dbtemplete
    #url = "https://websoilsurvey.sc.egov.usda.gov/DSD/Download/Cache/TemplateDB/wss_TemplateDB_soildb_US_2003_[2014-10-01].zip"
    #dbTemplete = urllib.request.urlretrieve(url, soil_dir+"tmp_soil/"+
    #                                        "wss_TemplateDB_soildb_US_2003_[2014-10-01].zip")[0]
    #unzip the file
    #os.system("unzip "+dbTemplete+ " -d "+soil_dir+"tmp_soil/")
    #extract table names and headers for templete file
    #import pypyodbc
    #####
    #tables=[]
    #for table in [x for x in os.listdir("../r_scripts/") if x[-9:]==".from_mdb"]:
    #    tables.append([table[:-10],"|".join(pd.read_csv("../r_scripts/"+table).columns.tolist()[1:])])
    #tables = pd.DataFrame(tables, columns=["TABLE_NAME","HEADERS"])
    #tables.to_csv("../data/General/SSURGO_tables_headers.csv")
    tablesHeaders = pd.read_csv("../data/General/SSURGO_tables_headers.csv")
    return tablesHeaders


# In[4]:


def get_ssurgo_study_area(area, date, tmp_dir, col_sufix, verbose):
    file = download_ssurgo_study_area(area, date, tmp_dir, verbose)
    #unzip the file(s)
    os.system("unzip "+file+ " -d "+tmp_dir)
    
    #get spatial data
    mapunits = geopandas.read_file(tmp_dir+area+"/spatial/", layer="soilmu_a_"+area)
    
    #get tabular data
    SSURGOTableMapping = pd.read_csv(tmp_dir+area+"/tabular/mstab.txt", header=None, sep="|")[[0,4]]
    SSURGOTableMapping.columns = ["TABLE","FILE"]
    SSURGOTableMapping["FILE"] = SSURGOTableMapping["FILE"]+".txt"
    tablesHeaders = get_table_headers()
    tableHeads = tablesHeaders.merge(SSURGOTableMapping, left_on="TABLE_NAME",
                                     right_on="TABLE")[["TABLE","FILE","HEADERS"]]
    tables_data={}
    for table in tableHeads["TABLE"]:
        try:
            tmp=pd.read_csv(tmp_dir+area+"/tabular/"+
                            tableHeads[tableHeads["TABLE"]==table]["FILE"].iloc[0],
                            names=tableHeads[tableHeads["TABLE"]==table]["HEADERS"].str.split("|").tolist()[0], sep="|")
            #change names of columns with added suffix to designate which of the three (.r, .l, .h) to use
            to_change = [x for x in tmp.columns.tolist() if col_sufix in x]
            to_change_dict={}
            for x in to_change:
                to_change_dict[x]=x.strip(col_sufix)
            tmp.rename(columns=to_change_dict, inplace=True)
            if len(tmp)>0:
                tables_data[table]=tmp.copy()
        except FileNotFoundError:
            continue
            #tables_data[table]=pd.DataFrame()
    #delete extracted files to conserve disk space
    os.system("rm -r "+tmp_dir+area)
    return mapunits, tables_data


# In[5]:


def get_soil_by_area(area_info, ID, tmp_dir, col_sufix, save, verbose):
    #get data for all SSURGO areas overlapping with area of interest
    SSURGOData=[]
    for area in area_info["areasymbol"]:
        date = parse(area_info[area_info["areasymbol"]==area]["saverest"].iloc[0]).strftime("%Y-%m-%d")
        SSURGOData.append(get_ssurgo_study_area(area, date, tmp_dir, col_sufix, verbose))

    #need to write code for combining multiple survey areas, for now just using first and printing message
    if len(SSURGOData) >1:
        if verbose:
            print("QUERY includes multiple SSURGO areas!  Current script only uses the first one.")
    spatial = SSURGOData[0][0]
    tabular = SSURGOData[0][1]

    if save:
        #save tables with headers (just incase we want them for something...)
        os.system("mkdir "+tmp_dir+ID)
        for key in tabular.keys():
            tabular[key].to_csv(tmp_dir+ID+"/"+key+".txt", sep="|", index=False)
        spatial.to_file(tmp_dir+ID+"/"+"SPATIAL.shp")
        spatial["MUKEY"] = spatial["MUKEY"].astype(int)
    return spatial, tabular


# In[6]:


def merge_tables(tabular, table_relationships, verbose):
    parent1=tabular[table_relationships["Parent Table"].iloc[0]].copy()
    for row in table_relationships.index:
        parent = table_relationships.loc[row]["Parent Table"]
        key = table_relationships.loc[row]["Key"]
        child = table_relationships.loc[row]["Child Table"]
        if child in tabular.keys():
            if verbose:
                print(row, parent, key, child, parent1.shape)
            parent1 = parent1.merge(tabular[child], on=key, how='left')
        else:
            if verbose:
                print("NOT AVAILABLE",row, parent, key, child)
    return parent1


# In[7]:


def pick_soils(spatial, tabular, num_keep):
    #determine which soils/mapunits to use. Keep those which are designated as farmland,
    #have yield estimates for corn (if possible), and cover the highest area.
    majcomp = tabular["component"][tabular["component"]["majcompflag"]=="Yes"][["compname","taxclname",
                                                                                "drainagecl","mukey",
                                                                                "cokey","slope",
                                                                                "hydgrp"]].copy()
    #bring in farmland suitability data
    majcomp = majcomp.merge(tabular["mapunit"][["muname", "muacres","farmlndcl","lkey","mukey"]], on="mukey", how='left')
    #determine which soils are most representative
    if len(majcomp[majcomp["farmlndcl"]!="Not prime farmland"]) >= num_keep:
        #remove non-farmland data
        majcomp = majcomp[majcomp["farmlndcl"]!="Not prime farmland"]
    #data to single column for irr and non irr
    if "cocropyld" in tabular.keys():
        cropyld = tabular["cocropyld"].copy()
        cropyld = cropyld[cropyld["cropname"]=="Corn"]
        cropyld["nonirryield"]=cropyld[[x for x in cropyld.columns if "nonirryield" in x]].mean(axis=1)
        cropyld["irryield"]=cropyld[[x for x in cropyld.columns if ("irryield" in x) and ("non" not in x)]].mean(axis=1)
        majcomp = majcomp.merge(cropyld[["cropname","cokey", "yldunits", "nonirryield", "irryield"]],
                                on="cokey", how='left')
        #remove data that does not include corn yield estimates (unless there are none for the whole area)
        if "Corn" in majcomp["cropname"].tolist():
            majcomp = majcomp[majcomp["cropname"]=="Corn"]
    #calculate relative area for each available soil and choose those with the highest
    spatial["MUKEY"] = spatial["MUKEY"].astype(int)
    #print(majcomp)
    geoarea = spatial[spatial["MUKEY"].isin(majcomp["mukey"])].copy()
    geoarea["area"] = geoarea.area
    #print(geoarea)
    geoarea = geoarea.pivot_table(index=["MUKEY"], values="area", aggfunc = np.sum).reset_index()
    #print(geoarea)
    geoarea["area"] = geoarea["area"]/geoarea["area"].sum()
    majcomp = majcomp.merge(geoarea, left_on="mukey", right_on="MUKEY")
    majcomp.drop_duplicates(subset="mukey", inplace=True)
    majcomp = majcomp.sort_values("area", ascending=False).reset_index(drop=True).iloc[:num_keep]
    return majcomp


# In[8]:


#Merging based on MuKey & averaging over Horizon Depth can allow us to weight each different
#depth evenly regardless of how many samples there were---need to still figure out non-numerical
#averaging
def Merge_Horizons(MergedCsv):
    #dropped keys post-merging
    MergedCsv = MergedCsv.drop(['chkey', 'hzthk', 'hzdepb',  'cokey'], axis=1)
    MergedCsv = MergedCsv[MergedCsv.hzdept <= 60]
    
    
    MeanGroup = MergedCsv.groupby(["mukey", "hzdept"]).mean()
    MergedCsv.merge(MeanGroup, on = ['mukey'], suffixes=('', '_mean'))
    #Bins to split data into ranges
    #MergedCsv['hzdept'].replace(MergedCsv['hzdepthrange'])
    bins = [-1, 16, 31, np.inf]
    names = ['0-15', '15-30', '30-60']
    MergedCsv['hzdepthrange'] = pd.cut(MergedCsv['hzdept'], bins, labels=names)
    
    MergedCsv = MergedCsv.groupby(['mukey','hzdepthrange']).mean().reset_index()
    #MergedCsv = MergedCsv.groupby(['hzdepthrange'])
    #MergedCsv = MergedCsv.drop_duplicates(['mukey','hzdept','gypsum'], keep='last')
    #MergedCsv = MergedCsv.groupby(['mukey', 'hzdepthrange'])
    ranger = []
    x = range(2, (len(MergedCsv)), 3)
    for n in x:
        ranger.append(n)
    for x in ranger:
        MergedCsv.iloc[x, 2:]  = MergedCsv.iloc[(x-1), 2:]
    MergedCsv = MergedCsv.drop(['hzdept'], axis=1)
    #add extra copies of first soil if less than 5 soils exist
    #while len(MergedCsv)<15:
    #    MergedCsv = pd.concat([MergedCsv,MergedCsv.iloc[:3]])
    
    
    #Mean is pulling out categorical data---can be fixed by taking the mean of a copy & remerging
    #return MergedCsv
    #MergedCsv.to_csv(soil_dir + "soil_soil/" + ID + ".csv")
    #Export_Data(MergedCsv)
    return MergedCsv


# In[9]:


def get_horizons(MergedCsv, spatial, horizons, num_keep):
    MergedCsv.dropna(axis=1, how='all', inplace=True)
    MergedCsv.dropna(subset=["hzdepb","hzdept"], inplace=True)
    to_drop=['compname','cokey','musym','hzname', 'chkey',"taxorder","taxclname","texture","texdesc","hzthk"]
    MergedCsv.drop(columns=[x for x in to_drop if x in MergedCsv.columns], inplace=True)
    first = ['mukey',"hzdept","hzdepb"]
    MergedCsv.drop_duplicates(inplace=True)
    MergedCsv = MergedCsv[first+[x for x in MergedCsv if x not in first]]
    #print(MergedCsv)
    soils=[]
    for mukey in MergedCsv["mukey"].unique():
        #print(mukey)
        tmp = MergedCsv[MergedCsv["mukey"]==mukey].copy()
        tmp = tmp[tmp['hzdept']<max(horizons)]
        for horiz in range(0, len(horizons)-1):
            top = horizons[horiz]
            bottom = horizons[horiz+1]
            tmp1 = tmp[(tmp["hzdepb"]>top) & (tmp["hzdept"]<bottom)].mean().copy()
            tmp1["horizons"]=top
            #print(tmp1)
            soils.append(tmp1)
            #print(top, bottom)
            #print(tmp[(tmp["hzdepb"]>=top) & (tmp["hzdept"]<=bottom)][["hzdept","hzdepb"]])
    soils = pd.concat(soils, axis=1, sort=True).T
    soils = soils.dropna(axis=0, how='all')
    #print(soils)
    soils["mukey"] = soils["mukey"].astype(int)
    #add horizon names
    #soils["horizons"] = horizons[:-1]*int(len(soils)/len(horizons[:-1]))
    #calculate areas
    geoarea = spatial[spatial["MUKEY"].isin(soils["mukey"])].copy()
    geoarea["area"] = geoarea.area
    geoarea = geoarea.pivot_table(index=["MUKEY"], values="area", aggfunc = np.sum).reset_index()
    geoarea["area"] = geoarea["area"]/geoarea["area"].sum()
    geoarea = geoarea.sort_values("area", ascending=False)
    if num_keep !=None:
        geoarea = geoarea.iloc[:num_keep,:]
    soils = geoarea.merge(soils, left_on="MUKEY", right_on="mukey", how='left')
    #drop no longer needed columns
    soils.drop(columns=["MUKEY","hzdept","hzdepb"], inplace=True)
    first=["mukey","horizons","area"]
    soils = soils[first+[x for x in soils.columns if x not in first]]
    if num_keep !=None:
        while len(soils)<(num_keep*(len(horizons)-1)):
            soils = pd.concat([soils,soils.iloc[:(len(horizons)-1)]])
    return soils


# In[10]:


def retrieve_soil_info_by_ID(ID, soil_dir, soil_areas, table_relationships, TabRead, desired_tables, verbose):
    soil_area = soil_areas[soil_areas["ID"]==ID].iloc[0]
    area_info, coords = get_ssurgo_inventory(soil_area, tmp_dir=soil_dir+"tmp_soil/SSURGO_inventory/", verbose=verbose)
    spatial, tabular = get_soil_by_area(area_info, ID, tmp_dir=soil_dir+"tmp_soil/", col_sufix=".r", save=False,
                                        verbose=verbose)
    if spatial["MUSYM"].all()=="NOTCOM": 
        if verbose:
            print("No usable soil data... Skipping")
        MergedCsv=pd.DataFrame()
    else:
        #merge together desired tables
        merged = merge_tables(tabular,
                              table_relationships[table_relationships["Child Table"].isin(desired_tables)],
                              verbose=verbose)
        #pick most representative soil components
        majcomp = pick_soils(spatial, tabular, num_keep=1000)
        #keep only desired columns
        desired_cols=["mukey","musym","cokey","compname","slope", "hydgrp","hzname", "hzdept","hzdepb","hzthk",
                      "sandtotal","claytotal", "dbthirdba","dbovendry","om","ksat", "wfifteenba",
                      "wthirdba","ph1to1h2o","wtdepaprjunmin"] #add extra columns potentially not designated in TabRead file
        merged = merged[[x for x in list(set(TabRead["Physical Name"].tolist() + desired_cols)) if x in merged.columns]]
        #keep only desired map units and components ("soils")
        MergedCsv = merged[(merged["mukey"].isin(majcomp["mukey"])) & (merged["cokey"].isin(majcomp["cokey"]))].copy()
        #MergedCsv = Merge_Horizons(MergedCsv)
        #put soil irrigation yield prediction data in
        #, "nonirryield", "irryield"
        #majcomp_cols = [x for x in ["mukey","cokey","area"] if x in majcomp.columns]
        #MergedCsv = MergedCsv.merge(majcomp[majcomp_cols], left_on="mukey", right_on="mukey", how="left")
        #print(MergedCsv)
        horizons = [0,15,30,60]
        #MergedCsv = get_horizons(MergedCsv, spatial, horizons, num_keep=None)
        #MergedCsv = weighted_average_horiz(MergedCsv)
        MergedCsv.to_csv(soil_dir + ID + ".csv")
    return MergedCsv, spatial, tabular, majcomp


# In[11]:


def weighted_average_horiz(soils):
    #get weighted average horizons
    soils = soils.drop(columns=["mukey"]).copy()
    soils_avg =[]
    for horiz in soils["horizons"].unique():
        tmp = soils[soils["horizons"]==horiz].copy()
        #fill missing values with mean of columns # NEED TO EXPLORE BETTER WAYS OF DOING THIS IN THE FUTURE
        tmp.dropna(axis=1, how='all', inplace=True)
        tmp = tmp.fillna(tmp.mean())
        #print(tmp.mean(axis=0))
        soils_avg.append(pd.DataFrame(np.average(tmp, axis=0, weights=tmp["area"]), index=tmp.columns).T)
    soils_avg = pd.concat(soils_avg, sort=True)
    soils_avg.drop(columns=["area"], inplace=True)
    soils_avg["horizons"] = soils_avg["horizons"].round(0).astype(int)
    soils_avg = soils_avg[["horizons"]+[x for x in soils_avg.columns if x not in ["horizons"]]]
    return soils_avg


# In[12]:


def impute_data(soildata):
    #removing ID column before imputing, giving a label to add later.
    all_processed_soils1a = soildata["ID"]
    soildata = soildata.drop("ID", axis = 1)
    
    #Imputing & relabeling---k=3 was chosen at random
    imputedtable = KNN(k=3).fit_transform(soildata)
    
    #KNN returns as an array, must be turned back into a pandas df.
    imputedtable = pd.DataFrame(imputedtable)
    
    #must convert df to list to return it as a column.
    imputedtable["ID"] = list(all_processed_soils1a)
     
    #Table headers removed with KNN, added back in. 
    imputedtable.columns = ["awc", "caco3", "cec7", "claytotal", "dbovendry", 
                     "ec", "freeiron?", "gypsum", "horizons", "ksat", "om", "partdensity", "ph1to1h2o", "sandtotal", "silttotal", "slope",
                      "ID"]
    return imputedtable


# In[ ]:


'''
###below code to remain commented out except when testing###
soil_dir="../data/Soil_data/"
os.system("mkdir "+soil_dir+"tmp_soil/")
#load county coordinate data from working phenotype table (produced in script 1)
soil_areas = pd.read_csv("../data/Phenotype_data/work_set_yields_by_county.csv", index_col=0)
soil_areas.drop_duplicates(subset="ID", inplace=True)
soil_areas = soil_areas[["ID","County","State","Latitude","Longitude"]]
soil_areas.dropna(inplace=True)
#place random county from each state at the top of the list so that one county per state is run first as a test
soil_areas = soil_areas.sample(frac=1, random_state=54321)
soil_areas["tmp"] = soil_areas.duplicated("State")
soil_areas = soil_areas.sort_values("tmp").reset_index(drop=True)
#for the moment we only care abotu downloading entire countys (in most cases the same thing as a single 
#SSURGO soil survey area). For that reason we will use an area of 0.001 degreas in all directions around
#the provided county center data. This is just an area which we are confident lies within the county and
#is used to pull down the data for the area of the entire county (assumed here to be the same as the
#soil survey area)
degs_to_add=0.001
soil_areas["North"] = soil_areas["Latitude"]+degs_to_add
soil_areas["South"] = soil_areas["Latitude"]-degs_to_add
soil_areas["East"] = soil_areas["Longitude"]+degs_to_add
soil_areas["West"] = soil_areas["Longitude"]-degs_to_add

#CSV Patrick made containing names of important soil categories
#TabRead = pd.read_csv("../data/General/TableColumnDescriptionsReport.csv", encoding="ISO-8859-1", skiprows=1)
TabRead = pd.read_csv("../data/General/Revised Table Selections Soil Table.csv", encoding="ISO-8859-1", skiprows=1)
#remove tables that we don't care about
TabRead = TabRead[TabRead["Usable"]=="yes"]
desired_tables = TabRead["Table Physical Name"].unique().tolist()
#import SSURGO table connections
table_relationships = pd.read_csv("../data/General/SSURGO_table_relationships.csv")
'''


# In[ ]:


'''
#get table headers
#failed = {}
all_processed_soils=[]
tablesHeaders = get_table_headers()
count=0
for ID in soil_areas["ID"].unique()[:41]:
    count+=1
    print(count,"/",len(soil_areas["ID"].unique()), ID)
    #Get SSURGO inventory data for desired area of interest
    #try:
    MergedCsv, spatial, tabular, majcomp = retrieve_soil_info_by_ID(ID, soil_dir, soil_areas, table_relationships,
                                                                    TabRead, desired_tables, verbose=True)
    if len(MergedCsv)!=0:
        MergedCsv["ID"]=ID
        all_processed_soils.append(MergedCsv)
    #except Exception as e:
    #    print(ID, "failed.  Added to list for future attempts.")
    #    failed[ID] = str(e)
all_processed_soils = pd.concat(all_processed_soils, sort=True)
'''


# In[ ]:




