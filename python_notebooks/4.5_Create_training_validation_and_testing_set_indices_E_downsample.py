#!/usr/bin/env python
# coding: utf-8

# In[1]:


import pandas as pd
import numpy as np
import os, sys
import random
from math import ceil
from matplotlib import pyplot as plt
import json
from sklearn.model_selection import KFold
pd.set_option('display.max_rows', 500)


# In[2]:


#read in index file (created in previous script)
indices = pd.read_csv("../data/index_file.csv", index_col=[0], parse_dates=["Date Planted"], low_memory=False)


# In[3]:


#![title](Hist_train_val_test_split.png)


# In[4]:


#create Historical data splits
hist_indices = indices[indices["G2F"]==False].copy()
G2F_indices = indices[indices["G2F"]==True].copy()

#test set - years 2014-present for all counties, as well as G2F counties across all years
idx1=hist_indices[hist_indices["Year"]>2013].copy().index.tolist()
G2F_counties = (G2F_indices["State"]+G2F_indices["County"]).unique().tolist()
idx2 = hist_indices[(hist_indices["State"]+hist_indices["County"]).isin(G2F_counties)].index.tolist()
hist_test = indices.loc[sorted(list(set(idx1+idx2)))].copy()

#Val set - random 10% from samples not in test set
not_test = hist_indices[hist_indices.index.isin(hist_test.index.tolist())==False].index.tolist()
hist_val = random.sample(not_test,int(len(hist_indices)*0.1))
hist_val = indices.loc[sorted(hist_val)].copy()

#test set - everything not in test or validation set
hist_train = hist_indices[hist_indices.index.isin(hist_test.index.tolist()+hist_val.index.tolist())==False].copy()
print(len(hist_train), len(hist_val), len(hist_test))
print(str(round(100*len(hist_train)/len(hist_indices),1))+"%",
      str(round(100*len(hist_val)/len(hist_indices),1))+"%",
      str(round(100*len(hist_test)/len(hist_indices),1))+"%")
#sainity checks
#print([x for x in hist_test.index.tolist() if x in hist_train.index.tolist()])
#print([x for x in hist_test.index.tolist() if x in hist_val.index.tolist()])
hist_sets={"Historical":{"train": hist_train.index.tolist(),
                         "val": hist_val.index.tolist(),
                         "test": hist_test.index.tolist()}}


# In[5]:


#![title](G2F_train_val_test_split.png)


# In[6]:


#create G2F data splits Split E 


# In[7]:


#create environments by county and year
G2F_indices["State@County@Year"] = G2F_indices["State"]+"@"+G2F_indices["County"]+"@"+G2F_indices["Year"].astype("str")

#pick environment to drop in order to balance downsampling of environments with downsampling of samples
smpls_per_env = G2F_indices.pivot_table(index=["State@County@Year"], values="State", aggfunc=np.count_nonzero).sort_values("State")
smpls_per_env["#Env"] = [len(smpls_per_env)-x for x in range(0, len(smpls_per_env))]
smpls_per_env["Total_Samples"] = smpls_per_env["State"]*smpls_per_env["#Env"]
#Used plot below to determine that the bigest jumps occur in the first few and a cutoff of >=293 is best
#smpls_per_env.plot.scatter(x="#Env",y="Total_Samples")
n=293
kept_envs = smpls_per_env[smpls_per_env["State"]>=n].index.tolist()
print(smpls_per_env[smpls_per_env["State"]==n])

#downsample so each env has same number of samples
kept_ind=[]
for env in kept_envs:
    #print(env)
    kept_ind = kept_ind + random.sample(G2F_indices[G2F_indices["State@County@Year"]==env].index.tolist(),n)
kept_ind = G2F_indices.loc[kept_ind].copy()
print(len(kept_envs), len(kept_ind))


# In[8]:


#create single env holdout
sets={}
hld_year_geno=[]
for env in kept_envs:
    #print(env)
    test_set = kept_ind[kept_ind["State@County@Year"]==env].copy()
    train_set = kept_ind[kept_ind["State@County@Year"]!=env].copy()
    #create validation set
    tmp_ids=sorted(random.sample(train_set.index.tolist(), int(0.05*len(train_set.index.tolist()))))
    val_set=train_set.loc[tmp_ids].copy()
    train_set=train_set[train_set.index.isin(tmp_ids)==False]
    #sanity checks
    if len(train_set[train_set["State@County@Year"].isin(test_set["State@County@Year"].unique().tolist())]) !=0:
        print("CONTAMINATED SETS: Genotype")
    sets[str(env)]={"train":train_set.index.tolist(),
                         "val":val_set.index.tolist(),
                         "test":test_set.index.tolist()}
    hld_year_geno.append([env,len(train_set),len(val_set),len(test_set)])


# In[9]:


#combine historical and G2F data sets into a single dictionary
sets.update(hist_sets)
len(sets)


# In[10]:


#pd.DataFrame(hld_year_geno, columns=["Env","Train","Val","Test"])


# In[11]:


#save sets to json file
with open('../data/Train_val_test_sets_E_dwnSample'+str(n)+"_24Apr2020.json", 'w') as fp:
    json.dump(sets, fp)


# In[12]:


for iterset in sets:
    print(iterset)


# In[ ]:





# In[23]:


#example of how to open json file for further use
#with open('../data/Train_val_test_sets_1_Oct2019.json', 'r') as fp:
#    test = json.load(fp)


# In[ ]:





# In[ ]:




