#!/usr/bin/env python
# coding: utf-8

# In[1]:


import os, sys
import pandas as pd
import geopandas
import numpy as np
import urllib.request
from dateutil.parser import parse
from SSURO_soil_data_functions import retrieve_soil_info_by_ID, get_table_headers, get_ssurgo_inventory
import datetime
import time
from concurrent.futures import ProcessPoolExecutor, ThreadPoolExecutor
import multiprocessing
#for imputation and testing
from sklearn.experimental import enable_iterative_imputer
from sklearn.impute import IterativeImputer
from sklearn.ensemble import RandomForestRegressor
import math
import random


# In[2]:


soil_dir="../data/Soil_data/"
os.system("mkdir "+soil_dir+"tmp_soil/")

#set dataset variable (choose between Historical and G2F)
#dataset="Historical"
dataset="G2F"

if dataset=="Historical":
    #load county coordinate data from working phenotype table (produced in script 1)
    soil_areas = pd.read_csv("../data/Phenotype_data/work_set_yields_by_county.csv", index_col=0)
    soil_areas.drop_duplicates(subset="ID", inplace=True)
    soil_areas = soil_areas[["ID","County","State","Latitude","Longitude"]]
    soil_areas.dropna(inplace=True)
    #place random county from each state at the top of the list so that one county per state is run first as a test
    soil_areas = soil_areas.sample(frac=1, random_state=54321)
    soil_areas["tmp"] = soil_areas.duplicated("State")
    soil_areas = soil_areas.sort_values("tmp").reset_index(drop=True)
    #for the moment we only care abotu downloading entire countys (in most cases the same thing as a single 
    #SSURGO soil survey area). For that reason we will use an area of 0.001 degreas in all directions around
    #the provided county center data. This is just an area which we are confident lies within the county and
    #is used to pull down the data for the area of the entire county (assumed here to be the same as the
    #soil survey area)
    degs_to_add=0.001
    soil_areas["North"] = soil_areas["Latitude"]+degs_to_add
    soil_areas["South"] = soil_areas["Latitude"]-degs_to_add
    soil_areas["East"] = soil_areas["Longitude"]+degs_to_add
    soil_areas["West"] = soil_areas["Longitude"]-degs_to_add

elif dataset=="G2F":
    #load coordinate data from working phenotype table (produced in script 1b)
    soil_areas = pd.read_csv("../data/Phenotype_data/G2F_work_set_2014_2017.csv", index_col=0)
    soil_areas = soil_areas[["Location_Year","North","South","East","West"]]
    soil_areas = soil_areas.drop_duplicates().reset_index(drop=True)
    soil_areas.rename(columns={"Location_Year": "ID"}, inplace=True)
    #remove Canada areas as no soil data exists for them in US database
    soil_areas = soil_areas[soil_areas["ID"].str[:2]!="ON"].reset_index(drop=True)
    #check that the area is not zero
    soil_areas["N-S"] = soil_areas["North"]-soil_areas["South"]
    soil_areas["E-W"] = soil_areas["East"]-soil_areas["West"]
    tmp = soil_areas[(soil_areas["N-S"]<=0) | (soil_areas["E-W"]<=0)]
    if len(tmp) > 0 :
        print(tmp)


# In[4]:


soil_areas


# In[5]:


#CSV Patrick made containing names of important soil categories
#TabRead = pd.read_csv("../data/General/TableColumnDescriptionsReport.csv", encoding="ISO-8859-1", skiprows=1)
TabRead = pd.read_csv("../data/General/Revised Table Selections Soil Table.csv", encoding="ISO-8859-1", skiprows=1)
#remove tables that we don't care about
TabRead = TabRead[TabRead["Usable"]=="yes"]
desired_tables = TabRead["Table Physical Name"].unique().tolist()
#import SSURGO table connections
table_relationships = pd.read_csv("../data/General/SSURGO_table_relationships.csv")


# In[6]:


#first get all inventories one at a time.  Seem to have problems getting them in parallel
for ID in soil_areas["ID"].unique():
    soil_area = soil_areas[soil_areas["ID"]==ID].iloc[0]
    area_info, coords = get_ssurgo_inventory(soil_area, tmp_dir=soil_dir+"tmp_soil/SSURGO_inventory/", verbose=True)


# In[7]:


def multiprocess(func, args, workers):
    begin_time = time.time()
    with ProcessPoolExecutor(max_workers=workers) as executor:
        res = executor.map(func, args, [begin_time for i in range(len(args))])
    return list(res)
def run_single_ID_w_try(ID, base):
    start = time.time() - base
    try:
        MergedCsv, spatial, tabular, majcomp = retrieve_soil_info_by_ID(ID, soil_dir, soil_areas, table_relationships,
                                                               TabRead, desired_tables, verbose=False)
        MergedCsv["ID"]=ID
        e="None"
        print(soil_areas[soil_areas["ID"]==ID].index[0],"/",len(soil_areas), ID)
    except: #Exception as ex:
        print(soil_areas[soil_areas["ID"]==ID].index[0],"/",len(soil_areas), ID,
              "failed.  Added to list for future attempts.")
        MergedCsv = pd.DataFrame()
        e="ERROR"
    stop = time.time() - base
    return start, stop, MergedCsv, ID, e


# In[8]:


#run all on as many cores as possible
args = soil_areas["ID"].unique().tolist()
if dataset=="Historical":
    results = multiprocess(run_single_ID_w_try, args, multiprocessing.cpu_count()-1)
elif dataset=="G2F":
    #Some G2F use the same downloaded files which causes conflicts in multiprocessing. Run with 1 CPU
    results = multiprocess(run_single_ID_w_try, args, 1)


# In[9]:


#combine results
all_processed_soils = pd.concat([x[2] for x in results], sort=True)
print(all_processed_soils.shape)

timings = []
for process in results:
    timings.append([process[0],process[1],process[3],process[4]])
timings = pd.DataFrame(timings, columns=["start","stop","ID","Errors"])
print (timings.shape)


# In[14]:


#read pre-processed files from directory
all_processed_soils=[]
for file in [x for x in os.listdir(soil_dir) if x[-4:]==".csv" and x[:-4] in args]:
    tmp=pd.read_csv(soil_dir+file, index_col=0)
    tmp["ID"]=file[:-4]
    all_processed_soils.append(tmp)
all_processed_soils = pd.concat(all_processed_soils, sort=True)
print(all_processed_soils.shape, len(all_processed_soils["ID"].unique()), len(all_processed_soils["ID"].unique())*3)
print(round((len(all_processed_soils["ID"].unique())/len(soil_areas["ID"].unique()))*100,2), "%  Success!")


# In[15]:


all_processed_soils.astype(str).pivot_table(index=["ID"], values="horizons", aggfunc=np.count_nonzero)["horizons"].unique()


# In[16]:


failed = soil_areas[soil_areas["ID"].isin(all_processed_soils["ID"].unique())==False].copy()
print(len(failed))
if dataset=="Historical":
    failed.to_csv(soil_dir+"Failed_counties.csv")
elif dataset=="G2F":
    failed.to_csv(soil_dir+"G2F_Failed_counties.csv")
print(round((len(failed["ID"].unique())/len(soil_areas["ID"].unique()))*100,2),"%  Failure")


# In[17]:


soil_areas["Failed"] = soil_areas["ID"].isin(failed["ID"])
soil_areas.sort_values(["ID"])


# In[18]:


failed.sort_values("ID")


# In[19]:


first=["ID","horizons"]
all_processed_soils = all_processed_soils[first + [x for x in all_processed_soils.columns if x not in first]]
#all_processed_soils.drop(["musym","mukey","cokey"], axis=1, inplace =True)
all_processed_soils.dropna(how='all', inplace=True)
print(all_processed_soils.shape)
if dataset=="Historical":
    all_processed_soils.to_csv(soil_dir+"all_processed_soils.csv")
elif dataset=="G2F":
    all_processed_soils.to_csv(soil_dir+"G2F_all_processed_soils.csv")


# In[20]:


#remove columns with high percentage NA
missingness = pd.DataFrame(all_processed_soils.isnull().mean(), columns=["% NA"])
#missingness["% NA"]=missingness["% NA"]*100
missingness.sort_values("% NA", ascending=False, inplace=True)
drop = missingness[missingness["% NA"]>=0.95].index.tolist()
all_processed_soils = all_processed_soils.drop(columns=drop).reset_index(drop=True)


# In[21]:


#save soils without imputation, simply droping rows and columns with lots of missing data
print(all_processed_soils.shape)
print(all_processed_soils.drop(columns=["partdensity"]).shape)
print(all_processed_soils.drop(columns=["partdensity"]).dropna().shape)
all_soils_no_impute = all_processed_soils.drop(columns=["partdensity"]).dropna().copy()
keep = all_soils_no_impute.astype(str).pivot_table(index=["ID"], values="horizons", aggfunc=np.count_nonzero)
keep = keep[keep["horizons"]==len(all_processed_soils["horizons"].unique())].index.tolist()
all_soils_no_impute = all_soils_no_impute[all_soils_no_impute["ID"].isin(keep)].reset_index(drop=True)
print(all_soils_no_impute.shape)
if dataset=="Historical":
    all_soils_no_impute.to_csv(soil_dir+"all_soils_no_impute.csv")
elif dataset=="G2F":
    all_soils_no_impute.to_csv(soil_dir+"G2F_all_soils_no_impute.csv")


# In[ ]:





# In[ ]:


#below is still experimental


# In[8]:


def create_imputation_test_set(all_processed_soils, missingness):
    for_testing = all_processed_soils.dropna().copy().reset_index(drop=True)
    #remove IDs without all horizons intact for simplicity (hareder problem than those with partial intact anyway)
    tmp=for_testing.astype(str).pivot_table(index=["ID"],values="horizons", aggfunc=np.count_nonzero)
    tmp = tmp[tmp["horizons"]==len(for_testing["horizons"].unique())].index.tolist()
    for_testing = for_testing[for_testing["ID"].isin(tmp)].reset_index(drop=True)
    mutated = for_testing.copy()
    #add missing data randomly but in the same percentage as exists in overall dataset.
    #Also make sure that missing data is grouped by ID. All or non should be missing within an ID.
    for feature in [x for x in missingness[missingness["% NA"]>0].index if x in mutated.columns]:
        #get number of distinct IDs to mutate. Round up if fraction.
        num_to_mutate=missingness.loc[feature]["% NA"]*len(mutated)
        numIDs_to_mutate = math.ceil(num_to_mutate/len(mutated["horizons"].unique()))
        #print(feature, num_to_mutate, num_to_mutate/len(for_testing["horizons"].unique()), numIDs_to_mutate)
        #pick random IDs
        IDs_to_mutate=random.sample(mutated["ID"].unique().tolist(), numIDs_to_mutate)
        idx_to_mutate = for_testing[mutated["ID"].isin(IDs_to_mutate)].index.tolist()
        #make mutations
        mutated.loc[idx_to_mutate,feature]=np.nan
    #sainity check
    missing_check = pd.concat([all_processed_soils.isnull().mean(),for_testing.isnull().mean(),mutated.isnull().mean()], axis=1)
    missing_check.columns=["Original","Reduced","Mutated"]
    #print(missing_check.sort_values(["Original"], ascending=False))
    return for_testing, mutated


# In[9]:


def perform_imputation(mutated, n_estimators, random_state):
    imputer = IterativeImputer(random_state=random_state,
                               estimator=RandomForestRegressor(random_state=random_state, n_estimators=n_estimators))
    mut_imputed = imputer.fit_transform(mutated.iloc[:,1:])
    mut_imputed=pd.concat([mutated.iloc[:,:1],pd.DataFrame(mut_imputed, columns=mutated.columns[1:])], sort=False, axis=1)
    mse_impute = truth.iloc[:,2:]-mut_imputed.iloc[:,2:]
    mse_impute = mse_impute.replace(0,np.nan)
    mse_impute = mse_impute**2
    rmse_impute = mse_impute.mean().dropna()**(1/2)
    rRMSE = (rmse_impute/truth.mean()).dropna()
    return rRMSE


# In[ ]:


random.seed(12345)


# In[ ]:


truth, mutated = create_imputation_test_set(all_processed_soils, missingness)
perform_imputation(mutated, n_estimators=100, random_state=0)


# In[10]:


from sklearn.preprocessing import MinMaxScaler
scaler=MinMaxScaler(feature_range=(0,1))
tmp=pd.DataFrame(scaler.fit_transform(all_processed_soils.iloc[:,2:]), columns=all_processed_soils.columns[2:])
all_proc_soils_scaled = pd.concat([all_processed_soils.iloc[:,:2],tmp], sort=False, axis=1)


# In[12]:


truth, mutated = create_imputation_test_set(all_proc_soils_scaled, missingness)
perform_imputation(mutated, n_estimators=100, random_state=0)


# In[14]:


from sklearn.preprocessing import PowerTransformer
scaler=PowerTransformer(method='yeo-johnson')
tmp=pd.DataFrame(scaler.fit_transform(all_processed_soils.iloc[:,2:]), columns=all_processed_soils.columns[2:])
all_proc_soils_scaled = pd.concat([all_processed_soils.iloc[:,:2],tmp], sort=False, axis=1)


# In[15]:


truth, mutated = create_imputation_test_set(all_proc_soils_scaled, missingness)
perform_imputation(mutated, n_estimators=100, random_state=0)


# In[17]:


truth


# In[ ]:




