#!/usr/bin/env python
# coding: utf-8

# In[1]:


import os, sys
import pandas as pd
import numpy as np
import time, datetime
from tqdm import tqdm_notebook as tqdm
from concurrent.futures import ProcessPoolExecutor, ThreadPoolExecutor
import multiprocessing
import pickle


# In[2]:


#load PCs
pcs = pd.read_csv("../data/Genotype_data/G2FHybrids_RecipRem_2_22_2018_Anna_Rogers_genos_w_phenos_PCs.txt",
                  sep="\t", low_memory=False, header=2)
#load relationship matrix
grm = pd.read_csv("../data/Genotype_data/G2FHybrids_RecipRem_2_22_2018_Anna_Rogers_genos_w_phenos_Cent_IBS.txt",
                  sep="\t", skiprows=3, header=None, index_col=0)
#load G2F data and filter out genotypes without PC data
G2F_phenos = pd.read_csv("../data/Phenotype_data/G2F_work_set_2014_2017.csv", low_memory=False,
                  parse_dates=['Date Planted','Date Harvested', 'Anthesis [date]', 'Silking [date]'], index_col=[0])
G2F_phenos.rename(columns={"Location_Year": "ID_Year", "Grain Yield [bu/A]": "BU / ACRE"}, inplace=True)
G2F_phenos["ID"]=G2F_phenos["ID_Year"]
G2F_phenos = G2F_phenos[G2F_phenos["Pedigree"].isin(pcs["Taxa"])].reset_index(drop=True)
print(len(G2F_phenos))


# In[3]:


#prep data for use without estimated planting data
#import and pre-process data
phenotypes = pd.read_csv("../data/Phenotype_data/work_set_yields_by_county.csv", index_col=0)
phenotypes.dropna(subset=["Latitude"], axis=0, inplace=True)
phenotypes = phenotypes[phenotypes["BU / ACRE"]>0]
phenotypes["ID_Year"] = phenotypes["ID"]+"@"+phenotypes["Year"].astype('str')
phenotypes.reset_index(drop=True, inplace=True)

#add historical planting density
plt_density=pd.read_csv("../data/General/planting_density_stateyear.csv")
plt_density.rename(columns={"Value":"Plant Density"}, inplace=True)
plt_density = plt_density[["Year", "State", "Plant Density"]]
#convert to plants/m2
plt_density["Plant Density"] = plt_density["Plant Density"].astype("int")/4046.86
#go through row by row and add plant density depending on availability
tmp=[]
for row in phenotypes.index:
    year = phenotypes.loc[row,"Year"]
    state = phenotypes.loc[row,"State"]
    density=plt_density[(plt_density["Year"]==year) & (plt_density["State"]==state)]["Plant Density"].mean()
    if (density>0)==False:
        density=plt_density[plt_density["Year"]==year]["Plant Density"].mean()
    tmp.append(density)
phenotypes["Plant Density"]=tmp


# In[4]:


###add in G2F data
DesCols = [x for x in phenotypes.columns if x in G2F_phenos.columns] + ['Date Planted',"Pedigree",'% Clay', '% Sand', '% Silt', '%Ca Sat',
       '%H Sat', '%K Sat', '%Mg Sat', '%Na Sat', '1:1 S Salts mmho/cm',
       '1:1 Soil pH', 'Calcium ppm Ca', 'Magnesium ppm Mg',
       'Mehlich P-III ppm P', 'Nitrate-N ppm N', 'Organic Matter LOI %',
       'Potassium ppm K', 'Sodium ppm Na', 'Sulfate-S ppm S', 'WDRF Buffer pH',
       'lbs N/A', 'Total K lbs/acre', 'Total N lbs/acre', 'Total P lbs/acre',
       'Irrigation amount (inches)']
phenotypes = pd.concat([phenotypes, G2F_phenos[DesCols]],
                       sort=False).reset_index(drop=True)
phenotypes["State"] = phenotypes["State"].str.replace(" ","_")
phenotypes["County"] = phenotypes["County"].str.replace(" ","_")
phenotypes["G2F"] = phenotypes["ID_Year"].str.split("@", expand=True)[2].isna()


# In[5]:


#add altitude data
#pd.read_csv("../data/Elevation_data/NationalFile_20190901.csv", sep="|")[["STATE_ALPHA", "COUNTY_NAME","ELEV_IN_FT"]]
altitude = pd.read_csv("../data/Elevation_data/combined_data/combined_data.csv", na_values="-")
altitude.rename(columns={"Ele(ft)":"Altitude"}, inplace=True)
altitude = altitude[["State","County","Altitude"]]
#fix abreviations
state_abrev = {"ALABAMA":"AL","ALASKA":"AK","ARIZONA":"AZ","ARKANSAS":"AR","CALIFORNIA":"CA","COLORADO":"CO",
               "CONNECTICUT":"CT","DELAWARE":"DE","FLORIDA":"FL","GEORGIA":"GA","HAWAII":"HI","IDAHO":"ID",
               "ILLINOIS":"IL","INDIANA":"IN","IOWA":"IA","KANSAS":"KS","KENTUCKY":"KY","LOUISIANA":"LA",
               "MAINE":"ME","MARYLAND":"MD","MASSACHUSETTS":"MA","MICHIGAN":"MI","MINNESOTA":"MN",
               "MISSISSIPPI":"MS","MISSOURI":"MO","MONTANA":"MT","NEBRASKA":"NE","NEVADA":"NV",
               "NEW HAMPSHIRE":"NH","NEW JERSEY":"NJ","NEW MEXICO":"NM","NEW YORK":"NY","NORTH CAROLINA":"NC",
               "NORTH DAKOTA":"ND","OHIO":"OH","OKLAHOMA":"OK","OREGON":"OR","PENNSYLVANIA":"PA",
               "RHODE ISLAND":"RI","SOUTH CAROLINA":"SC","SOUTH DAKOTA":"SD","TENNESSEE":"TN","TEXAS":"TX",
               "UTAH":"UT","VERMONT":"VT","VIRGINIA":"VA","WASHINGTON":"WA","WEST VIRGINIA":"WV",
               "WISCONSIN":"WI","WYOMING":"WY"}
state_abrev = pd.DataFrame(state_abrev, index=[0]).T.reset_index()
state_abrev.index = state_abrev[0]
state_abrev = state_abrev["index"].to_dict()
altitude.replace(state_abrev, inplace=True)
altitude["County"]=altitude["County"].str.upper()
altitude["State"]=altitude["State"].str.replace(" ","_")
altitude["County"]=altitude["County"].str.replace(" ","_")
altitude = altitude[altitude["Altitude"].isna()==False]
#ensure only one value per county
altitude = altitude.pivot_table(index=["State","County"]).reset_index()
#add county data that is missing - Louisiana seems to be missing for some reason but elevation there is -8 to 535 with 100 as average
to_add=[]
for state_county in (phenotypes["State"]+"@"+phenotypes["County"]).unique():
    if state_county not in (altitude["State"]+"@"+altitude["County"]).unique().tolist():
        if state_county.split("@")[0]=="LOUISIANA":
            to_add.append([state_county.split("@")[0], state_county.split("@")[1], 100])
        else:
            print(state_county)
to_add = pd.DataFrame(to_add, columns=["State","County","Altitude"])
altitude = pd.concat([altitude,to_add])

phenotypes = phenotypes.merge(altitude, how='left', on=["State","County"])


# In[6]:


len(phenotypes)


# In[7]:


DesCols=['ID_Year', 'State', 'County', 'Year', "Pedigree", 'Latitude', 'Longitude', "Altitude", 'Irrigated',
         "Plant Density", "G2F", 'Date Planted', '% Clay', '% Sand', '% Silt', '%Ca Sat','%H Sat',
         '%K Sat', '%Mg Sat', '%Na Sat', '1:1 S Salts mmho/cm','1:1 Soil pH', 'Calcium ppm Ca', 'Magnesium ppm Mg',
         'Mehlich P-III ppm P', 'Nitrate-N ppm N', 'Organic Matter LOI %','Potassium ppm K', 'Sodium ppm Na',
         'Sulfate-S ppm S', 'WDRF Buffer pH','lbs N/A', 'Total K lbs/acre', 'Total N lbs/acre', 'Total P lbs/acre',
         'Irrigation amount (inches)', 'BU / ACRE']


# In[8]:


#soils
#soils = pd.read_csv("../data/Soil_data/all_processed_soils.csv", index_col=0)
soils = pd.read_csv("../data/Soil_data/all_soils_no_impute.csv", index_col=0)
tmp = pd.read_csv("../data/Soil_data/G2F_all_soils_no_impute.csv", index_col=0)
soils = pd.concat([soils,tmp], sort=False).reset_index(drop=True).dropna()
#weather
weather = pd.read_csv("../data/Weather_data/All_met_data_365.csv", index_col=0)
weather["ID_Year"] = weather["ID"]+"@"+weather["year"].astype(str)
tmp = pd.read_csv("../data/Weather_data/G2F_all_met_data_365.csv", index_col=0)
tmp["ID_Year"]=tmp["ID"]
weather = pd.concat([weather,tmp], sort=False).reset_index(drop=True)

#reduce phenotypes table to only those with both weather and soil data
phenotypes = phenotypes[(phenotypes["ID"].isin(weather["ID"].unique())) & (phenotypes["ID"].isin(soils["ID"].unique()))]
phenotypes = phenotypes[DesCols].reset_index(drop=True)
phenotypes["State"] = phenotypes["State"].str.replace(" ","_")
phenotypes["County"] = phenotypes["County"].str.replace(" ","_")
print(len(phenotypes))


# In[9]:


#Import irrigation data so it can be added to weather
#import irrigation data
irrigation = pd.read_csv("../data/GenomesToFields_2014_2017_v1/G2F_Converted_Fert&Irr/Combined_G2F_irrigation.txt",
                         sep="\t", parse_dates=["Irrigation Date"])
irrigation["Location_Year"] = irrigation["Experiment"]+"@"+irrigation["Year"].astype(str)
#convert inches to mm
irrigation["Irrigation amount (mm)"] = irrigation["Irrigation amount (inches)"]*25.4
irrigation = irrigation[(irrigation["Irrigation amount (mm)"]>0) & (irrigation["Irrigation Date"].isna()==False)].reset_index(drop=True)
#add irrigation into weather data
irrigation_added=[]
for idx in irrigation.index:
    ID_Year = irrigation.loc[idx,"Location_Year"]
    Year = irrigation.loc[idx,"Year"]
    IrrDate = irrigation.loc[idx,"Irrigation Date"].timetuple().tm_yday
    IrrAmount = irrigation.loc[idx,"Irrigation amount (mm)"]
    weath_index = weather[(weather["ID_Year"]==ID_Year) & (weather["year"]==Year) & (weather["level_1"]=="precip")].index
    if len(weath_index)>1: print("You have found an error in the weather data pipeline. Please report.")
    weath_index = weath_index[0]
    previous=weather.loc[weath_index,str(IrrDate)]
    weather.loc[weath_index,str(IrrDate)]=weather.loc[weath_index,str(IrrDate)]+IrrAmount
    irrigation_added.append([ID_Year, Year, weath_index, IrrDate, IrrAmount, previous,
                             weather.loc[weath_index,str(IrrDate)]])
irrigation_added = pd.DataFrame(irrigation_added, columns=["ID_Year", "Year", "weath_index", "IrrDate", "IrrAmount", "previous","current"])


# In[10]:


with pd.option_context('display.max_rows', 200):  # more options can be specified also
    print(irrigation_added)


# In[11]:


#create numpy arrays with all data in form that is ready for easy test, val, train spliting (removing samples 
#in which data is not available for soil, weather, and phenotype. Also duplicating soil data so it has the 
#same index as weather and phenotype)
weather2 = weather[weather["ID_Year"].str[-4:]==weather["year"].astype(str)].copy() #this cuts the processing time in half
weather2.index=weather2["ID_Year"]
weather2 = weather2.iloc[:,3:-1]
np_yield=[]
np_general=[]
np_soils=[]
np_weather=[]
np_pcs=[]
np_grm=[]
#for ID_Year, index in zip(phenotypes["ID_Year"],phenotypes.index):
for index in tqdm(phenotypes.index):
    ID_Year=phenotypes.loc[index]["ID_Year"]
    #print(index, "/", len(phenotypes), ID_Year)
    np_yield.append(phenotypes.loc[index]["BU / ACRE"])
    np_general.append(phenotypes.loc[index][1:-1].values)
    if len(ID_Year.split("@"))>2:
        np_soils.append(soils[soils["ID"]==ID_Year[:-5]].iloc[:,2:].values)
    else:
        np_soils.append(soils[soils["ID"]==ID_Year].iloc[:,2:].values)
    #np_weather.append(weather[(weather["ID_Year"]==ID_Year) & (weather["year"]==int(ID_Year[-4:]))].iloc[:,3:-1].values)
    np_weather.append(weather2.loc[ID_Year].values)
    #PCs/Genomic Relationship Matrix
    if type(phenotypes.loc[index]["Pedigree"])==float:
        np_pcs.append(np.zeros(pcs.shape[1]-1))
        np_grm.append(np.zeros(grm.shape[1]))
    else:
        np_pcs.append(pcs[pcs["Taxa"]==phenotypes.loc[index]["Pedigree"]].iloc[0,1:].values)
        np_grm.append(grm.loc[phenotypes.loc[index]["Pedigree"]].values)
np_yield = np.array(np_yield)
np_general = np.stack(np_general, axis=0)
np_general = pd.DataFrame(np_general).fillna(-1).values
np_soils = np.stack(np_soils, axis=0)
np_weather = np.stack(np_weather, axis=0)
np_pcs = np.stack(np_pcs)
np_grm = np.stack(np_grm)
print(np_yield.shape, np_general.shape, np_soils.shape, np_weather.shape, np_pcs.shape, np_grm.shape)

#save data to a few files for simplicity.
np.save("../data/Yield.npy",np_yield)
np.save("../data/General.npy",np_general)
np.save("../data/Soils.npy",np_soils)
np.save("../data/Weather.npy",np_weather)
np.save("../data/PCs.npy",np_pcs)
np.save("../data/GRM.npy",np_grm)

#phenotypes = phenotypes.fillna(-1)
phenotypes = phenotypes[DesCols]
phenotypes.to_csv("../data/index_file.csv")


# In[12]:


def get_plnting_date(met_env_year, earliest, latest):
    #determine planting date
    mint=10 #minimum temperature cutoff in C #10C~50F
    tempwin=10
    #precip_win=4 #precipitation average windowsize
    precip_min=10 #precipitation in mm
    precp_avg=[0]*tempwin
    win_avg=[0]*tempwin
    for window in range(1,len(met_env_year)-(tempwin-1)):
        #print window, window+winsize
        win_avg.append(met_env_year[(met_env_year["day"]>=window) & (met_env_year["day"]<window+tempwin)]["mint"].mean())
        precp_avg.append(met_env_year[(met_env_year["day"]>=window) & (met_env_year["day"]<window+tempwin)]["precip"].mean())
    met_env_year["avg_win_mint"]=win_avg
    met_env_year["avg_win_precip"]=precp_avg
    
    plnt_date = met_env_year[(met_env_year["day"]>=earliest) &
                             (met_env_year["day"]<=latest) &
                             (met_env_year["avg_win_mint"]>=mint) &
                             (met_env_year["avg_win_precip"]<=precip_min)].copy()
    if len(plnt_date) == 0:
        plnt_date=latest
    else:
        plnt_date = plnt_date["day"].iloc[0]
    #print plnt_date
    #print met_env_year[met_env_year["day"]==plnt_date]
    return plnt_date


# In[13]:


######determine planting date.######
def get_planting_date_ranges(phenotypes):  #based on historical data and standard planting times at different latitudes.
    #import historical data, convert dates to day of year, remove rows where 0 percent have been planted
    hist_plnt_dates = pd.read_csv("../data/General/Planting_pct_by_state_by_week.csv", parse_dates=["Week Ending"])
    hist_plnt_dates = hist_plnt_dates[["Year","Week Ending","State","Value"]]
    hist_plnt_dates["State"] = hist_plnt_dates["State"].str.replace(" ","_")
    hist_plnt_dates["dayofyear"] = hist_plnt_dates["Week Ending"].dt.dayofyear
    hist_plnt_dates = hist_plnt_dates[hist_plnt_dates["Value"]>0]
    #find min, max, mean based on individual years (when available)
    plnt_day_state = pd.concat([hist_plnt_dates.pivot_table(index=["State","Year"], values="dayofyear", aggfunc=np.min),
                                hist_plnt_dates.pivot_table(index=["State","Year"], values="dayofyear", aggfunc=np.max),
                                hist_plnt_dates.pivot_table(index=["State","Year"], values="dayofyear", aggfunc=np.mean),
                                hist_plnt_dates.pivot_table(index=["State","Year"], values="dayofyear", aggfunc=np.count_nonzero)],
                               axis=1)
    plnt_day_state.columns = ["min","max","mean","n"]
    plnt_day_state = plnt_day_state.reset_index()
    #merge data in with phenotype data
    p1 = phenotypes.merge(plnt_day_state, on=["State","Year"], how="left")

    #find min, max, mean based on all years combined by state
    plnt_day_state = pd.concat([hist_plnt_dates.pivot_table(index=["State"], values="dayofyear", aggfunc=np.min),
                                hist_plnt_dates.pivot_table(index=["State"], values="dayofyear", aggfunc=np.max),
                                hist_plnt_dates.pivot_table(index=["State"], values="dayofyear", aggfunc=np.mean),
                                hist_plnt_dates.pivot_table(index=["State"], values="dayofyear", aggfunc=np.count_nonzero),
                                hist_plnt_dates.drop_duplicates(subset=["Year","State"]).pivot_table(index=["State"], values="Year", aggfunc=np.count_nonzero)],
                               axis=1)
    plnt_day_state.columns = ["min","max","mean","n","n_years"]
    plnt_day_state = plnt_day_state.reset_index()
    p2 = phenotypes.merge(plnt_day_state, on="State", how="left")

    #for states with no data fill in with estimated values by latitude
    zones = pd.DataFrame([[40,50,110,150],[35,39.999999,91,150],[25,34.999999,50,140]],
                         columns=["min_lat","max_lat","min","max"])
    zones["mean"] = zones[["min","max"]].mean(axis=1)
    by_zone=[]
    for index in phenotypes.index:
        by_zone.append(zones[(zones["min_lat"]<phenotypes.loc[index,"Latitude"]) &
                             (zones["max_lat"]>phenotypes.loc[index,"Latitude"])][["min","max","mean"]])
    by_zone = pd.concat(by_zone)
    by_zone = by_zone.reset_index(drop=True)
    p3 = pd.concat([phenotypes,by_zone], sort=False, axis=1)
    #do the actual merge
    p_final = p1.combine_first(p2)
    p_final = p_final.combine_first(p3)
    return p_final[phenotypes.columns.tolist()+[x for x in p_final.columns if x not in phenotypes.columns]]


# In[14]:


def multiprocess(func, args, workers):
    begin_time = time.time()
    with ProcessPoolExecutor(max_workers=workers) as executor:
        res = executor.map(func, args, [begin_time for i in range(len(args))])
    return list(res)
def get_plnt_date_by_index(index, base):
    start = time.time() - base
    print(index)
    days_to_harvest=125 # days between planting and harvesting (same for all in order to have same size tensors)
    padding=10 #days added both before planting and after harvesting
    ID_Year = phenotypes.loc[index,"ID_Year"]
    tmp = weather[(weather["ID_Year"]==ID_Year) & (weather["year"]==int(ID_Year[-4:]))].copy()
    tmp.index=tmp["level_1"]
    tmp = tmp[[str(x) for x in range(1,366)]].T.reset_index()
    tmp = tmp.rename(columns={"index":"day"})
    tmp["day"] = tmp["day"].astype("int")
    #determine planting date
    if phenotypes.loc[index,'Date Planted']==-1:
        plnt_date = get_plnting_date(tmp.copy(),phenotypes.loc[index,"min"], phenotypes.loc[index,"max"])
        phenotypes.loc[index,'Date Planted'] = pd.Timestamp(phenotypes.loc[index,'Year'],1,1) + datetime.timedelta(int(plnt_date) - 1)
    else:
        plnt_date = phenotypes.loc[index,'Date Planted'].timetuple().tm_yday
    ######calculate thermal time and create weather dataset by thermal time#####
    base_temp = 10 #(C) ~ 50F
    tmp = tmp.iloc[int(plnt_date-padding):int(plnt_date+days_to_harvest+padding)]
    tmp.index=tmp["day"]
    tt = ((tmp["maxt"]+tmp["mint"])/2)-base_temp
    tt[tt<0]=0
    #tmp.loc[:,"tt"]=tt.tolist() #uncomment to add thermal time per day
    #put in zeros up to planting date
    tt.loc[:plnt_date-1]=0
    cum_tt=[]
    cum=0
    for day in tt.index.tolist():
        cum= cum+tt.loc[day]
        cum_tt.append(cum)
        #print(day,cum)
    tmp["cum_tt"]=cum_tt
    weather_by_tt = tmp.reset_index(drop=True).T.copy()
    
    stop = time.time() - base
    return index, ID_Year, plnt_date, weather_by_tt, start, stop


# In[15]:


phenotypes["Date Planted"] = phenotypes["Date Planted"].fillna(-1)


# In[16]:


#impute/guess planting dates and create second option for weather data 
phenotypes = get_planting_date_ranges(phenotypes)
results = multiprocess(get_plnt_date_by_index, phenotypes.index.tolist(), 20)


# In[17]:


idx_Id_pltdt=[]
weather_season_only=[]
for x in range(0,len(results)):
    idx_Id_pltdt.append(results[x][:3])
    weather_season_only.append(results[x][3])
idx_Id_pltdt = pd.DataFrame(idx_Id_pltdt, columns=["index","ID_Year","Planting_date"])
idx_Id_pltdt["year"] = idx_Id_pltdt["ID_Year"].str[-4:].astype('int')
weather_season_only = pd.concat(weather_season_only)
np_weather_season_only= weather_season_only.values.reshape(len(idx_Id_pltdt),-1, weather_season_only.shape[1])
weather_season_only.to_csv("../data/Weather_data/Weather_season_only.csv")
np.save("../data/Weather_season_only.npy",np_weather_season_only)
print(np_weather_season_only.shape)


# In[18]:


#save new version of the index file with true and estimated planting dates included
idx_Id_pltdt["Date Planted"] = pd.to_datetime(pd.DataFrame([idx_Id_pltdt["ID_Year"].str[-4:].astype(int).tolist(),
                                                             [1]*len(idx_Id_pltdt), [1]*len(idx_Id_pltdt)],
                                                            index=["year","month","day"]).T) + pd.to_timedelta(idx_Id_pltdt["Planting_date"]-1, unit='Days')
phenotypes["Date Planted"]=idx_Id_pltdt["Date Planted"]
phenotypes = phenotypes[DesCols]
phenotypes.to_csv("../data/index_file.csv")


# In[19]:


phenotypes


# In[ ]:




