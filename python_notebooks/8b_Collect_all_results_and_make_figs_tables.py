#!/usr/bin/env python
# coding: utf-8

# In[1]:


import os, sys
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.ticker import FormatStrFormatter
import seaborn as sns


# In[2]:


def calc_pct_increase_decrease(val1, val2):
    #first value (val1) is assumed to be original or starting values and val2 is ending values
    #check if calculating increase or decreas
    if (val1 - val2) < 0:
        inc=True
    else:
        inc=False

    if inc==False: # decrease
        pct_inc_dec = (((val1 - val2)/val1)*100)

    if inc: #increase
        pct_inc_dec = (((val2 - val1)/val1)*100)
    return pct_inc_dec, inc

def pct_inc_dec_2methods(modified, method1, method2, scenario):
    meth1 = modified[(modified["method"]==method1) & (modified["Mean/std"]=="Mean") & (modified["scenario"]==scenario)].copy()
    meth2 = modified[(modified["method"]==method2) & (modified["Mean/std"]=="Mean") & (modified["scenario"]==scenario)].copy()
    inc_dec_res = []
    for col in meth1.columns:
        if pd.api.types.is_numeric_dtype(meth1[col])==False: continue #Skip columns that are not numeric
        if col == "n_folds":continue
        #print(col, pd.api.types.is_numeric_dtype(meth1[col]))

        #get values to use
        val1 = meth1[col].iloc[0]
        val2 = meth2[col].iloc[0]

        #calculate % increase or decrease
        pct_inc_dec, inc = calc_pct_increase_decrease(val1, val2)

        #place values in list
        inc_dec_res.append([scenario, method1, method2, col, val1, val2, pct_inc_dec, inc])
    inc_dec_res = pd.DataFrame(inc_dec_res, columns=["scenario", "method1", "method2", "column", "val1", "val2", "pct_inc_dec", "increase"])
    return inc_dec_res


# In[3]:


#import final summary results files from various methods


# In[4]:


#The ensamble one contains many results so just pick the best based on validation set
out_res = pd.read_csv("../data/Results/Summary_results_multi_method_ensmbles_13Nov2020.csv")
#find the best ensmbles based on validation set
tmp = out_res[out_res["set"]=="validation_Final"].reset_index(drop=True).copy()
#do some orginizational magic
tmp = tmp.pivot(index="method",columns="Mean/std").sort_values([("scenario","Mean"),("RMSE","Mean")]).copy()
tmp = pd.DataFrame(tmp.stack()).reset_index()
val_picks=tmp.drop_duplicates(["scenario","Mean/std"])["method"].unique().tolist()
#tmp.drop_duplicates(["scenario","Mean/std"])
out_res = out_res[(out_res["set"]=="test_Final") & (out_res["method"].isin(val_picks))]


# In[5]:


#import final summary results files from other methods
files = ["../data/Results/Summary_results_all_02Nov2020.csv",
         "../../Washburn_repos/apsim_genotype_calibrator/data/Apsim_files/Classic/Calibrations/Standard_cultivars_pltArea_results/Summary_results_apsim_18Nov2020.csv"]
dfs=[]
for fl in files:
    dfs.append(pd.read_csv(fl))

dfs.append(out_res)
sum_res = pd.concat(dfs)
sum_res = sum_res.drop(columns="set").reset_index(drop=True)


# In[6]:


#do some orginizational magic and sorting
tmp=sum_res.pivot(index="method",columns="Mean/std").sort_values([("scenario","Mean"),("RMSE","Mean")]).copy()
sum_res = pd.DataFrame(tmp.stack()).reset_index()
means = sum_res[sum_res["Mean/std"] == "Mean"][["method","Mean/std","prsn","slope","r2","RMSE","rRMSE","normRMSE","n_folds",
                                        "scenario"]].copy()
stds = sum_res[sum_res["Mean/std"] == "Std"][["method","Mean/std","prsn","slope","r2","RMSE","rRMSE","normRMSE","n_folds",
                                        "scenario"]].copy()
overalls = sum_res[sum_res["Mean/std"] == "Overall"][["method","Mean/std","prsn","slope","r2","RMSE","rRMSE","normRMSE","n_folds",
                                        "scenario"]].copy()
final_sums_out = pd.concat([means,stds,overalls])


# In[7]:


#create simplified method names for each
final_sums_out["Long_method"] = final_sums_out["method"]
method_names = {'APSIM_yield_FlwTm_repsTrain_val_test_sets_13_Dec2019':'CGM FTCal',
'APSIM_yield_FlwTm_repsTrain_val_test_sets_Practical_GEM_26Feb2020':'CGM FTCal',
'APSIM_yield_FlwrTm_repsTrain_val_test_sets_E_dwnSample293_24Apr2020':'CGM FTCal',
'APSIM_yield_FlwrTm_repsTrain_val_test_sets_G_dwnSample12_23Apr2020':'CGM FTCal',

'APSIM_yield_repsTrain_val_test_sets_13_Dec2019':'AUTO CGM',
'APSIM_yield_repsTrain_val_test_sets_E_dwnSample293_24Apr2020':'AUTO CGM',
'APSIM_yield_repsTrain_val_test_sets_G_dwnSample12_23Apr2020':'AUTO CGM',
'APSIM_yield_repsTrain_val_test_sets_Practical_GEM_26Feb2020':'AUTO CGM',

'E_GR_13_Dec2019':"E BLUP",
'E_GR_E_dwnSample293_24Apr2020':"E BLUP",
'E_GR_G_dwnSample12_23Apr2020':"E BLUP",
'E_GR_Practical_GEM_26Feb2020':"E BLUP",

'G2FonHist_GEM_reps_val_training_13_Dec2019':'CNN G2F on Hist',
'G2FonHist_reps_val_training_E_dwnSample293_24Apr2020':'CNN G2F on Hist',
'G2FonHist_reps_val_training_G_dwnSample12_23Apr2020':'CNN G2F on Hist',
'G2FonHist_reps_val_training_Practical_GEM_26Feb2020':'CNN G2F on Hist',

'GE_GR_13_Dec2019':"GE BLUP",
'GE_GR_E_dwnSample293_24Apr2020':"GE BLUP",
'GE_GR_G_dwnSample12_23Apr2020':"GE BLUP",
'GE_GR_Practical_GEM_26Feb2020':"GE BLUP",
    
'G_GR_13_Dec2019':"G BLUP",
'G_GR_E_dwnSample293_24Apr2020':"G BLUP",
'G_GR_G_dwnSample12_23Apr2020':"G BLUP",
'G_GR_Practical_GEM_26Feb2020':"G BLUP",

'GxE_GR_13_Dec2019':"GxE BLUP",
'GxE_GR_E_dwnSample293_24Apr2020':"GxE BLUP",
'GxE_GR_G_dwnSample12_23Apr2020':"GxE BLUP",
'GxE_GR_Practical_GEM_26Feb2020':"GxE BLUP",

#AD blup called D BLUP
'G-AD_GR_GEM_Hard_13Dec2019':"D BLUP",
'G-AD_GR_E_dwnSample293_24Apr2020':"D BLUP",
'G-AD_GR_G_dwnSample12_23Apr2020':"D BLUP",
'G-AD_GR_Practical_GEM_26Feb2020':"D BLUP",
                
'GE-AD_GR_GEM_Hard_13Dec2019':"DE BLUP",
'GE-AD_GR_E_dwnSample293_24Apr2020':"DE BLUP",
'GE-AD_GR_G_dwnSample12_23Apr2020':"DE BLUP",
'GE-AD_GR_Practical_GEM_26Feb2020':"DE BLUP",

'GxE-AD_GR_GEM_Hard_13Dec2019':"DxE BLUP",
'GxE-AD_GR_E_dwnSample293_24Apr2020':"DxE BLUP",
'GxE-AD_GR_G_dwnSample12_23Apr2020':"DxE BLUP",
'GxE-AD_GR_Practical_GEM_26Feb2020':"DxE BLUP",         

'reps_val_training_MLP_large13_Dec2019':"MLP",
'reps_val_training_MLP_largeE_dwnSample293_24Apr2020':"MLP",
'reps_val_training_MLP_largeG_dwnSample12_23Apr2020':"MLP",
'reps_val_training_MLP_largePractical_GEM_26Feb2020':"MLP",

'GEM_reps_val_training_13_Dec2019':"CNN",
'reps_val_training_E_dwnSample293_24Apr2020':"CNN",
'reps_val_training_G_dwnSample12_23Apr2020':"CNN",
'reps_val_training_Practical_GEM_26Feb2020':"CNN",
    
'reps_val_training_NO_HIST_13_Dec2019':"CNN NO HIST",
'reps_val_training_NO_HIST_E_dwnSample293_24Apr2020':"CNN NO HIST",
'reps_val_training_NO_HIST_G_dwnSample12_23Apr2020':"CNN NO HIST",
'reps_val_training_NO_HIST_Practical_GEM_26Feb2020':"CNN NO HIST",

'reps_val_training_NO_G_E_dwnSample293_24Apr2020':'CNN NO G',
'reps_val_training_NO_G_G_dwnSample12_23Apr2020':'CNN NO G',
'reps_val_training_NO_G_Practical_GEM_26Feb2020':'CNN NO G',
'reps_val_training_NO_G_13_Dec2019':'CNN NO G',

'reps_val_training_NO_SOIL_13_Dec2019':"CNN NO SOIL",
'reps_val_training_NO_SOIL_E_dwnSample293_24Apr2020':"CNN NO SOIL",
'reps_val_training_NO_SOIL_G_dwnSample12_23Apr2020':"CNN NO SOIL",
'reps_val_training_NO_SOIL_Practical_GEM_26Feb2020':"CNN NO SOIL",
                
'reps_val_training_NO_WEATH_13_Dec2019':"CNN NO WEATH",
'reps_val_training_NO_WEATH_E_dwnSample293_24Apr2020':"CNN NO WEATH",
'reps_val_training_NO_WEATH_G_dwnSample12_23Apr2020':"CNN NO WEATH",
'reps_val_training_NO_WEATH_Practical_GEM_26Feb2020':"CNN NO WEATH",
                
'reps_val_training_RM_LOWSOIL_13_Dec2019':'CNN RM MISS SOIL',
'reps_val_training_RM_LOWSOIL_E_dwnSample293_24Apr2020':'CNN RM MISS SOIL',
'reps_val_training_RM_LOWSOIL_G_dwnSample12_23Apr2020':'CNN RM MISS SOIL',
'reps_val_training_RM_LOWSOIL_Practical_GEM_26Feb2020':'CNN RM MISS SOIL',

'reps_val_training_NO_GYPS_13_Dec2019':'CNN NO GYPSUM',
'reps_val_training_NO_GYPS_E_dwnSample293_24Apr2020':'CNN NO GYPSUM',
'reps_val_training_NO_GYPS_G_dwnSample12_23Apr2020':'CNN NO GYPSUM',
'reps_val_training_NO_GYPS_Practical_GEM_26Feb2020':'CNN NO GYPSUM',

'reps_val_training_NO_G_HIST_E_dwnSample293_24Apr2020':'CNN NO G NO HIST',
'reps_val_training_NO_G_HIST_G_dwnSample12_23Apr2020':'CNN NO G NO HIST',
'reps_val_training_NO_G_HIST_Practical_GEM_26Feb2020':'CNN NO G NO HIST',
'reps_val_training_NO_G_HIST_13_Dec2019':'CNN NO G NO HIST',
                
'reps_val_training_NO_SOIL_NO_HIST_13_Dec2019':"CNN NO SOIL NO HIST",
'reps_val_training_NO_SOIL_NO_HIST_E_dwnSample293_24Apr2020':"CNN NO SOIL NO HIST",
'reps_val_training_NO_SOIL_NO_HIST_G_dwnSample12_23Apr2020':"CNN NO SOIL NO HIST",
'reps_val_training_NO_SOIL_NO_HIST_Practical_GEM_26Feb2020':"CNN NO SOIL NO HIST",
                
'reps_val_training_NO_WEATH_NO_HIST_13_Dec2019':"CNN NO WEATH NO HIST",
'reps_val_training_NO_WEATH_NO_HIST_E_dwnSample293_24Apr2020':"CNN NO WEATH NO HIST",
'reps_val_training_NO_WEATH_NO_HIST_G_dwnSample12_23Apr2020':"CNN NO WEATH NO HIST",
'reps_val_training_NO_WEATH_NO_HIST_Practical_GEM_26Feb2020':"CNN NO WEATH NO HIST",
                
'reps_val_training_RM_LOWSOIL_NO_HIST_13_Dec2019':'CNN RM MISS SOIL NO HIST',
'reps_val_training_RM_LOWSOIL_NO_HIST_E_dwnSample293_24Apr2020':'CNN RM MISS SOIL NO HIST',
'reps_val_training_RM_LOWSOIL_NO_HIST_G_dwnSample12_23Apr2020':'CNN RM MISS SOIL NO HIST',
'reps_val_training_RM_LOWSOIL_NO_HIST_Practical_GEM_26Feb2020':'CNN RM MISS SOIL NO HIST',

'reps_val_training_NO_GYPS_NO_HIST_13_Dec2019':'CNN NO GYPSUM NO HIST',
'reps_val_training_NO_GYPS_NO_HIST_E_dwnSample293_24Apr2020':'CNN NO GYPSUM NO HIST',
'reps_val_training_NO_GYPS_NO_HIST_G_dwnSample12_23Apr2020':'CNN NO GYPSUM NO HIST',
'reps_val_training_NO_GYPS_NO_HIST_Practical_GEM_26Feb2020':'CNN NO GYPSUM NO HIST',

'reps_val_training_G_only_13_Dec2019':"CNN G ONLY NO HIST",
'reps_val_training_G_only_E_dwnSample293_24Apr2020':"CNN G ONLY NO HIST",
'reps_val_training_G_only_G_dwnSample12_23Apr2020':"CNN G ONLY NO HIST",
'reps_val_training_G_only_Practical_GEM_26Feb2020':"CNN G ONLY NO HIST",
                
'reps_val_training_SOIL_only_NO_HIST_13_Dec2019':"CNN SOIL ONLY NO HIST",
'reps_val_training_SOIL_only_NO_HIST_E_dwnSample293_24Apr2020':"CNN SOIL ONLY NO HIST",
'reps_val_training_SOIL_only_NO_HIST_G_dwnSample12_23Apr2020':"CNN SOIL ONLY NO HIST",
'reps_val_training_SOIL_only_NO_HIST_Practical_GEM_26Feb2020':"CNN SOIL ONLY NO HIST",
                
'reps_val_training_FERT_only_13_Dec2019':"CNN FERT ONLY NO HIST",
'reps_val_training_FERT_only_E_dwnSample293_24Apr2020':"CNN FERT ONLY NO HIST",
'reps_val_training_FERT_only_G_dwnSample12_23Apr2020':"CNN FERT ONLY NO HIST",
'reps_val_training_FERT_only_Practical_GEM_26Feb2020':"CNN FERT ONLY NO HIST",
                
'reps_val_training_WEATH_only_NO_HIST_13_Dec2019':"CNN WEATH ONLY NO HIST",
'reps_val_training_WEATH_only_NO_HIST_E_dwnSample293_24Apr2020':"CNN WEATH ONLY NO HIST",
'reps_val_training_WEATH_only_NO_HIST_G_dwnSample12_23Apr2020':"CNN WEATH ONLY NO HIST",
'reps_val_training_WEATH_only_NO_HIST_Practical_GEM_26Feb2020':"CNN WEATH ONLY NO HIST",
                
'reps_val_training_NO_SOIL_NO_HIST_13_Dec2019':"CNN NO SOIL NO HIST",
'reps_val_training_NO_SOIL_NO_HIST_E_dwnSample293_24Apr2020':"CNN NO SOIL NO HIST",
'reps_val_training_NO_SOIL_NO_HIST_G_dwnSample12_23Apr2020':"CNN NO SOIL NO HIST",
'reps_val_training_NO_SOIL_NO_HIST_Practical_GEM_26Feb2020':"CNN NO SOIL NO HIST",
       
'reps_val_training_FIELD_only_NO_HIST_13_Dec2019':"CNN FIELD ONLY NO HIST",
'reps_val_training_FIELD_only_NO_HIST_E_dwnSample293_24Apr2020':"CNN FIELD ONLY NO HIST",
'reps_val_training_FIELD_only_NO_HIST_G_dwnSample12_23Apr2020':"CNN FIELD ONLY NO HIST",
'reps_val_training_FIELD_only_NO_HIST_Practical_GEM_26Feb2020':"CNN FIELD ONLY NO HIST",
}

for key in method_names.keys():
    #print(key, method_names[key])
    final_sums_out["method"] = final_sums_out["method"].str.replace(key, method_names[key])
final_sums_out = final_sums_out[["method","scenario","Mean/std","prsn","slope","r2","RMSE","rRMSE","normRMSE","n_folds","Long_method"]]
#change some names
final_sums_out = final_sums_out.rename(columns={"prsn":"Pearson r", "rRMSE":"Relative RMSE", "normRMSE":"Normalized RMSE"})


# In[8]:


final_sums_out


# In[9]:


#save final file to disk
final_sums_out.to_csv("../data/Results/Final_result_summarys_ordered_26July2021.csv")


# In[9]:


#pd.set_option('display.max_rows', 300)
#final_sums_out


# In[ ]:





# In[9]:


#remove some things/change labels for figures
#change GxE hard APSIM FTcal to CGM
modified = final_sums_out.copy()
#remove other GxE hard APSIM model
#change the correct APSIM FTCal to APSIM


modified = modified[modified["Long_method"]!="APSIM_yield_repsTrain_val_test_sets_13_Dec2019"]
idx = modified[(modified["method"]=="CGM FTCal") & (modified["scenario"]=="GEM hard")].index
modified.loc[idx,"method"]="AUTO CGM"

#rm APSIM FTCal
modified = modified[modified["method"]!="CGM FTCal"]
#rm G2F on HIST
modified = modified[modified["method"]!="CNN G2F on Hist"]
#rm E BLUp
modified = modified[modified["method"]!="E BLUP"]
#combine GE and GxE BLUP, Take the larger of the two and rename it to GE/GxE BLUP
#find Long_method names for the ones to be droped
tmp = modified[(modified["method"].isin(["GE BLUP","GxE BLUP"])) &
         (modified["Mean/std"]=="Mean")].sort_values(["scenario","RMSE"], ascending=False).drop_duplicates("scenario")["Long_method"].to_list()
modified = modified[modified["Long_method"].isin(tmp)==False]
modified["method"] = modified["method"].str.replace('GxE BLUP', "GE/GxE BLUP")
modified["method"] = modified["method"].str.replace('GE BLUP', "GE/GxE BLUP")

tmp = modified[(modified["method"].isin(["DE BLUP","DxE BLUP"])) &
         (modified["Mean/std"]=="Mean")].sort_values(["scenario","RMSE"], ascending=False).drop_duplicates("scenario")["Long_method"].to_list()
modified = modified[modified["Long_method"].isin(tmp)==False]
modified["method"] = modified["method"].str.replace('DxE BLUP', "DE/DxE BLUP")
modified["method"] = modified["method"].str.replace('DE BLUP', "DE/DxE BLUP")


# In[10]:


def create_scatter_plot(final_sums_out, scenario, stat):
    tmp = final_sums_out[(final_sums_out["scenario"]==scenario) & final_sums_out["Mean/std"].isin(["Mean","Std"])].copy()
    to_plot = tmp[tmp["Mean/std"]=="Mean"].copy()
    Std = tmp[tmp["Mean/std"]=="Std"].copy()
    StdErr=Std.copy()
    StdErr[stat] = 2*(Std[stat]/np.sqrt(Std["n_folds"]))
    to_plot = to_plot.merge(StdErr, on=["method"])
    to_plot["method"] = to_plot["method"].str.replace(".*&.+","Ensemble")
    #sort
    to_plot = to_plot.sort_values(stat+"_x").reset_index(drop=True)
    #plot
    ax = to_plot.plot.bar(x="method",y=stat+"_x", yerr=stat+"_y", capsize=5, legend=False, edgecolor='black',
                          color="gray") #, hatch="///")
    ax.set_xlabel("")
    ax.set_ylabel(stat)
    ax.set(facecolor = "white")
    ax.yaxis.set_major_formatter(FormatStrFormatter('%.1f'))
    #ax.grid(color="lightgray", axis='y')
    plt.show()


# In[11]:


#make some figures

methods_to_plot = ['G BLUP', 'CGM', 'CNN NO HIST', 'AUTO CGM', 'GE/GxE BLUP', "CNN", "MLP", "DE/DxE BLUP"] 
#modified["method"].unique()
for scenario in modified["scenario"].unique():
    print(scenario)
    create_scatter_plot(modified[modified["method"].isin(methods_to_plot)], scenario, stat="Pearson r")


# In[ ]:





# In[12]:


for scenario in modified["scenario"].unique():
    print(scenario)
    create_scatter_plot(modified[modified["method"].isin(methods_to_plot)], scenario, stat="Normalized RMSE")


# In[13]:


for scenario in modified["scenario"].unique():
    print(scenario)
    create_scatter_plot(modified[modified["method"].isin(methods_to_plot)], scenario, stat="RMSE")


# In[23]:


def compare_methods(modified):
    #calculate percentage increase/decrease
    #given two method names, calculate the increase or decrease between them for all variables

    pct_inc_dec_res=[]
    for scenario in modified["scenario"].unique():
        #print(scenario)
        method1="CNN NO HIST"
        method2="CNN"
        pct_inc_dec_res.append(pct_inc_dec_2methods(modified, method1, method2, scenario))

        #compare CNN and CNN NO G
        #method1="CNN NO G"
        #method2="CNN"
        #pct_inc_dec_res.append(pct_inc_dec_2methods(modified, method1, method2, scenario))

        #compare best vs worst.
        tmp = modified[(modified["scenario"]==scenario) & (modified["Mean/std"]=="Mean")].sort_values("RMSE").copy()
        method2 = tmp["method"].iloc[0] #best
        method1 = tmp["method"].iloc[-1] #worst
        pct_inc_dec_res.append(pct_inc_dec_2methods(modified, method1, method2, scenario))

        #compare best CNN vs. best BLUP
        tmp = modified[(modified["scenario"]==scenario) & (modified["Mean/std"]=="Mean")].sort_values("RMSE").copy()
        method2 = tmp[tmp["method"].isin(["CNN", "CNN NO HIST", "MLP"])]["method"].iloc[0] #best CNN
        method1 = tmp[tmp["method"].isin(['GE/GxE BLUP', "DE/DxE BLUP"])]["method"].iloc[0]
        pct_inc_dec_res.append(pct_inc_dec_2methods(modified, method1, method2, scenario))
    pct_inc_dec_res = pd.concat(pct_inc_dec_res)
    
    #create table with all values stacked
    pct_inc_dec_res["for_pivot"] = pct_inc_dec_res["scenario"]+"#"+pct_inc_dec_res["method1"]+"#"+pct_inc_dec_res["method2"]
    pct_inc_dec_pivot = pct_inc_dec_res.pivot(index="for_pivot", columns="column", values=["pct_inc_dec", "increase"])
    pct_inc_dec_pivot = pct_inc_dec_pivot.T.reset_index()
    pct_inc_dec_pivot["new_columns"] = pct_inc_dec_pivot["column"]+" "+pct_inc_dec_pivot["level_0"]
    pct_inc_dec_pivot.index = pct_inc_dec_pivot["new_columns"]
    pct_inc_dec_pivot = pct_inc_dec_pivot.drop(columns=["level_0","column","new_columns"]).sort_index(ascending=False).T
    pct_inc_dec_pivot.reset_index(inplace=True)
    pct_inc_dec_pivot = pd.concat([pct_inc_dec_pivot, pct_inc_dec_pivot["for_pivot"].str.split("#", expand=True)], axis=1)
    pct_inc_dec_pivot = pct_inc_dec_pivot.rename(columns={0:"scenario",1:"method1",2:"method2"}).drop(columns="for_pivot")
    front=["scenario","method1","method2"]
    pct_inc_dec_pivot = pct_inc_dec_pivot[front+[x for x in pct_inc_dec_pivot.columns if x not in front]]
    
    return pct_inc_dec_pivot, pct_inc_dec_res


# In[24]:


pct_inc_dec_pivot, pct_inc_dec_res = compare_methods(modified[modified["method"].isin(methods_to_plot)])


# In[25]:


pct_inc_dec_pivot[['scenario', 'method1', 'method2', 'Pearson r pct_inc_dec', 'Pearson r increase',
                   'RMSE pct_inc_dec', 'RMSE increase','Relative RMSE pct_inc_dec', 'Relative RMSE increase',
                   'Normalized RMSE pct_inc_dec', 'Normalized RMSE increase']]


# In[26]:


pct_inc_dec_res[pct_inc_dec_res["column"]=="RMSE"]


# In[27]:


pct_inc_dec_res[pct_inc_dec_res["column"]=="Normalized RMSE"]


# In[28]:


pct_inc_dec_res[pct_inc_dec_res["column"]=="Pearson r"]


# In[ ]:





# In[21]:


#### MAKE TABLE OF RESULTS from different CNN methods ####


# In[22]:


def make_table_scenarios_methods(modified, stat, mean_or_std="Mean"):
    tmp = modified[modified["method"].str.contains("BLUP")==False].copy() #remove blup methods
    tmp = tmp[tmp["method"].str.contains("&")==False] #remove esemble methods
    tmp = tmp[tmp["method"].str.contains("CGM")==False] #remove CGM methods
    tmp = tmp[tmp["Mean/std"]==mean_or_std] #jist use means
    tmp = tmp.pivot(index="method", columns="scenario", values=stat).reset_index()
    tmp["Historical"] = tmp["method"].str.contains("NO HIST")==False
    tmp["G2F_included"] = tmp["method"].str.split("CNN", expand=True)[1].str.split("NO HIST", expand=True)[0].str.strip()
    tmp = tmp.replace([" ",""],"ALL")
    tmp.columns.name = mean_or_std+"-"+stat
    tmp["NOvsONLY"] = tmp["G2F_included"].str.contains("ONLY")==False
    tmp["Historical"] = tmp["Historical"].replace({False:"NO_HIST", True:"HIST"})
    tmp = tmp.sort_values(["Historical","NOvsONLY","G2F_included"]).reset_index(drop=True)
    tmp = tmp[["Historical","G2F_included","E holdout", "G holdout", "GEM Practical", "GEM hard"]]
    tmp.index = tmp["G2F_included"]+" - "+tmp["Historical"]
    return tmp


# In[23]:


desired_methods=["CNN","CNN NO G", "CNN NO WEATH", 'CNN NO SOIL', 'CNN RM MISS SOIL', 'CNN NO GYPSUM',
                 'CNN NO HIST', "CNN NO G NO HIST", 'CNN NO WEATH NO HIST','CNN NO SOIL NO HIST',
                 'CNN RM MISS SOIL NO HIST', 'CNN NO GYPSUM NO HIST']


# In[24]:


tmp=modified[modified["method"].isin(desired_methods)]
table_mean_r = make_table_scenarios_methods(tmp, stat="Pearson r")
sns.set(font_scale=1)
fig, ax = plt.subplots(figsize=(4,6))
ax = sns.heatmap(table_mean_r[["E holdout", "G holdout", "GEM Practical", "GEM hard"]], annot=True, fmt=".2f", cmap='viridis')
plt.show()


# In[25]:


tmp=modified[modified["method"].isin(desired_methods)]
table_mean_nRMSE = make_table_scenarios_methods(tmp, stat="Normalized RMSE")
sns.set(font_scale=1)
fig, ax = plt.subplots(figsize=(4,6))
ax = sns.heatmap(table_mean_nRMSE[["E holdout", "G holdout", "GEM Practical", "GEM hard"]], annot=True, fmt=".2f", cmap='viridis')
plt.show()


# In[ ]:





# In[ ]:




