#!/usr/bin/env python
# coding: utf-8

# In[1]:


#This script is mostly weather data processing
import os, sys
import pandas as pd
import requests
import datetime
import numpy as np
import time
from concurrent.futures import ProcessPoolExecutor, ThreadPoolExecutor
import multiprocessing


# In[2]:


def load_NASS_data(file_name, oldest_year):
    data = pd.read_csv(file_name, thousands=",")
    data = data[["Year","State","Ag District", "County", "Data Item", "Value"]]
    data = data[data["County"]!="OTHER (COMBINED) COUNTIES"]
    data = data.pivot_table(index=["County","State","Year"], columns=["Data Item"], values="Value")
    data.columns = [x.split(" - ")[1] for x in data.columns.tolist()]
    data.rename(columns={"ACRES PLANTED, NET":"ACRES PLANTED",
                         "YIELD, MEASURED IN BU / ACRE":"BU / ACRE"}, inplace=True)
    data = data[["ACRES PLANTED", "ACRES HARVESTED", "BU / ACRE"]]
    data.reset_index(inplace=True)
    data["ID"] = data["State"]+"@"+data["County"]
    data["ID_Year"] = data["ID"]+"@"+data["Year"].astype(str)
    data = data[data["Year"]>=oldest_year]#.reset_index(drop=True)
    data = data[data["BU / ACRE"].isnull()==False]#.reset_index(drop=True)
    data = data[data["BU / ACRE"]!=0].reset_index(drop=True)
    return data


# In[3]:


#get full USDA data set
data = load_NASS_data("../data/Phenotype_data/All_corn_us_by_county.csv", 1980)
#get irrigated and non-irrigated data sets
irr_data = load_NASS_data("../data/Phenotype_data/IRRIGATED_corn_by_county.csv", 1980)
non_irr_data = load_NASS_data("../data/Phenotype_data/NON_IRRIGATED_corn_by_county.csv", 1980)
####add irrigation details and combine data sets
#1=irrigated, 0= not irrigated
data["Irrigated"]=np.nan
irr_data["Irrigated"]=1
non_irr_data["Irrigated"]=0
#combine all records into single data frame
data = pd.concat([data,irr_data,non_irr_data])


# In[4]:


#some counties now have three records per year (one irr, one non-irr, and one that is the sum of the two)
#need to remove the sum of the two (originally from data) so that the county is represented by irrigated and
#non-irrigated acres
to_remove=[]
for ID_Year in set(irr_data["ID_Year"].tolist()+non_irr_data["ID_Year"].tolist()):
    #print (ID_Year)
    length = len(data[data["ID_Year"]==ID_Year])
    if length == 3:
        #both irrigated and non exist, need to make sure the add up to normal record and delete normal record. 
        tmp = data[data["ID_Year"]==ID_Year]
        if tmp[tmp["Irrigated"].isnull()]["ACRES HARVESTED"].sum()!=tmp[tmp["Irrigated"].isnull()==False]["ACRES HARVESTED"].sum():
            print("WARNING: Irrigated and non-irrigated data do not add up to total data!!!"+" "+ID_Year)
        to_remove.append(tmp[tmp["Irrigated"].isnull()].index.values[0])
    if length == 2:
        #simply need to remove total and keep the irrigated or non-irrigated number
        tmp = data[data["ID_Year"]==ID_Year]
        #if tmp[tmp["Irrigated"].isnull()]["ACRES HARVESTED"].sum()!=tmp[tmp["Irrigated"].isnull()==False]["ACRES HARVESTED"].sum():
        #    print("WARNING: data do not add up!!!"+" "+ID_Year)
        to_remove.append(tmp[tmp["Irrigated"].isnull()].index.values[0])
data = data[data.index.isin(to_remove)==False].reset_index(drop=True)    


# In[5]:


#get lat lon data by county
county_lat_lon = pd.read_csv("../data/General/County_lat_long_mod.csv", skiprows=[0])
#fix abreviations
state_abrev = {"ALABAMA":"AL","ALASKA":"AK","ARIZONA":"AZ","ARKANSAS":"AR","CALIFORNIA":"CA","COLORADO":"CO",
               "CONNECTICUT":"CT","DELAWARE":"DE","FLORIDA":"FL","GEORGIA":"GA","HAWAII":"HI","IDAHO":"ID",
               "ILLINOIS":"IL","INDIANA":"IN","IOWA":"IA","KANSAS":"KS","KENTUCKY":"KY","LOUISIANA":"LA",
               "MAINE":"ME","MARYLAND":"MD","MASSACHUSETTS":"MA","MICHIGAN":"MI","MINNESOTA":"MN",
               "MISSISSIPPI":"MS","MISSOURI":"MO","MONTANA":"MT","NEBRASKA":"NE","NEVADA":"NV",
               "NEW HAMPSHIRE":"NH","NEW JERSEY":"NJ","NEW MEXICO":"NM","NEW YORK":"NY","NORTH CAROLINA":"NC",
               "NORTH DAKOTA":"ND","OHIO":"OH","OKLAHOMA":"OK","OREGON":"OR","PENNSYLVANIA":"PA",
               "RHODE ISLAND":"RI","SOUTH CAROLINA":"SC","SOUTH DAKOTA":"SD","TENNESSEE":"TN","TEXAS":"TX",
               "UTAH":"UT","VERMONT":"VT","VIRGINIA":"VA","WASHINGTON":"WA","WEST VIRGINIA":"WV",
               "WISCONSIN":"WI","WYOMING":"WY"}
state_abrev = pd.DataFrame(state_abrev, index=[0]).T.reset_index()
state_abrev.index = state_abrev[0]
state_abrev = state_abrev["index"].to_dict()
county_lat_lon.replace(state_abrev, inplace=True)
county_lat_lon["County"] = county_lat_lon["County"].str.upper()
county_lat_lon["ID"] = county_lat_lon["State"]+"@"+county_lat_lon["County"]
county_lat_lon["Lat"] = county_lat_lon["Lat"].str[1:-2].astype('float')
county_lat_lon["Longitude"] = county_lat_lon["Longitude"].str.replace("–","-").str[:-2].astype("float")
#merge state and county names
data = pd.merge(data,county_lat_lon, on="ID", how="left")[["ID","County_x","State_x","Year","ACRES PLANTED",
                                                           "ACRES HARVESTED","BU / ACRE","Irrigated","Lat","Longitude"]]
data.rename(columns={"County_x":"County", "State_x":"State", "Lat":"Latitude"}, inplace=True)
#fix id names so that there are no spaces
data["ID"] = data["ID"].str.replace(" ","_")


# In[6]:


#prepare to get soil data
#create square area of interest by adding 0.001 in each direction to the GPS coordenates
degs_to_add=0.001
data["North"] = data["Latitude"]+degs_to_add
data["South"] = data["Latitude"]-degs_to_add
data["East"] = data["Longitude"]+degs_to_add
data["West"] = data["Longitude"]-degs_to_add


# In[10]:


#write working dataset to file
data.to_csv("../data/Phenotype_data/work_set_yields_by_county.csv")


# In[ ]:


#just for fun count counties
county_counts = county_lat_lon["County"].value_counts().reset_index()
tmp=[]
for county in county_counts["index"]:
    tmp.append(county_lat_lon[county_lat_lon["County"]==county]["State"].tolist())
county_counts["States"]=tmp
county_counts.to_csv("../data/General/county_counts.csv")
tmp = county_lat_lon["State"].value_counts().reset_index()
tmp = pd.merge(county_counts[county_counts["County"]==1]["States"].str[0].value_counts().reset_index(), tmp, on='index', how='left')
tmp['proportion'] = tmp["States"]/tmp["State"]
tmp.columns = ["State","Unique_Counties","Total_Counties","Proportion_Unique"]
tmp.sort_values(["Proportion_Unique"], ascending=False).to_csv("../data/General/Unique_counties.csv")


# In[ ]:


print(data[data["Irrigated"]==1]["BU / ACRE"].mean(),
      data[data["Irrigated"]==0]["BU / ACRE"].mean(),
      data[data["Irrigated"].isnull()]["BU / ACRE"].mean(),
      data["BU / ACRE"].mean())


# In[ ]:




