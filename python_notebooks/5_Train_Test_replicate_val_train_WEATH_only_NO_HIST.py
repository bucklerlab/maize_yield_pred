#!/usr/bin/env python
# coding: utf-8

# In[1]:


#add in model saving of only the best performing model on the validation set.


# In[2]:


#from hyperopt import tpe, hp, fmin
import os,sys
os.environ["CUDA_DEVICE_ORDER"]="PCI_BUS_ID"
os.environ["CUDA_VISIBLE_DEVICES"]="1"
import pandas as pd
import numpy as np
from tensorflow.keras.models import Model
from tensorflow.keras.layers import Dense, Dropout, Flatten, Input, concatenate, BatchNormalization
from tensorflow.keras.layers import Conv1D, MaxPooling2D, Activation, AveragePooling1D
import tensorflow.keras
from tensorflow.keras.optimizers import SGD
from random import shuffle
from tensorflow.keras.models import model_from_json
from tensorflow.keras import backend as K
import gc
import json
from scipy.stats import pearsonr
from scipy.stats import spearmanr
import tensorflow as tf
#config = tf.ConfigProto(allow_soft_placement=True)
#config.gpu_options.per_process_gpu_memory_fraction = 0.2
#sess = tf.Session(config=config)
#K.set_session(sess)
np_load_old = np.load
from datetime import datetime


# In[3]:


gpus = tf.config.experimental.list_physical_devices('GPU')
if gpus:
  # Restrict TensorFlow to only allocate 1GB of memory on the first GPU
  try:
    tf.config.experimental.set_virtual_device_configuration(
        gpus[0],
        [tf.config.experimental.VirtualDeviceConfiguration(memory_limit=4096)])
    logical_gpus = tf.config.experimental.list_logical_devices('GPU')
    print(len(gpus), "Physical GPUs,", len(logical_gpus), "Logical GPUs")
  except RuntimeError as e:
    # Virtual devices must be set before GPUs have been initialized
    print(e)
#physical_devices = tf.config.list_physical_devices('GPU') 
#try: 
#  tf.config.experimental.set_memory_growth(physical_devices[0], True) 
#except: 
#  # Invalid device or cannot modify virtual devices once initialized. 
#  pass


# In[4]:


def readData(method):
    np.load = lambda *a, **k: np_load_old(*a, allow_pickle=True, **k)
    
    if "_G_only_" in method:
        include = ["G"]
    elif "_NO_SOIL_" in method:
        include = ["G", "general", "weather", "fertility"]
    elif "_NO_G_" in method:
        include = ["general", "soil", "weather", "fertility"]
    elif "_NO_WEATH_" in method:
        include = ["G", "general", "soil", "fertility"]
    elif "_WEATH_only_":
        include = ["weather"]
    else:
        include = ["G", "general", "soil", "weather", "fertility"]
    
    #always load phenotypes
    phenotypes=pd.read_csv(dataFolder+"index_file.csv", index_col=0, low_memory=False)
    rowIndexes=phenotypes.index.values
    myyield = phenotypes["BU / ACRE"].values
    
    general = phenotypes[['Year', 'Latitude', 'Longitude', 'Altitude', 'Plant Density']].fillna(-1).values
    
    if "soil" in include:
        soils = np.load(dataFolder+"Soils.npy")
        soils = soils.reshape(soils.shape[0], soils.shape[1] * soils.shape[2])
        if "general" in include:
            ConsData = np.concatenate([soils, general], axis=1)
        else:
            ConsData = soils
    elif "general" in include:
        ConsData = general
    else:
        ConsData = pd.DataFrame()
    
    if "weather" in include:
        weather = np.load(dataFolder+"Weather_season_only.npy")
        weather = np.transpose(weather, (0, 2, 1))
    else:
        weather = pd.DataFrame()
    
    fertility = phenotypes[['% Clay', '% Sand', '% Silt', '%Ca Sat', '%H Sat','%K Sat', '%Mg Sat', '%Na Sat',
                            '1:1 S Salts mmho/cm', '1:1 Soil pH','Calcium ppm Ca', 'Magnesium ppm Mg',
                            'Mehlich P-III ppm P','Nitrate-N ppm N', 'Organic Matter LOI %', 'Potassium ppm K',
                            'Sodium ppm Na', 'Sulfate-S ppm S', 'WDRF Buffer pH', 'lbs N/A', 'Total K lbs/acre',
                            'Total N lbs/acre', 'Total P lbs/acre', 'Irrigation amount (inches)', 'Irrigated']].fillna(-1).values
    if "G" in include:
        pcs = np.load(dataFolder+"PCs.npy")[:,0:30]
        if "fertility" in include:
            pcs = np.concatenate([pcs, fertility], axis=1)
    elif "fertility" in include:
        pcs = fertility
    else:
        pcs = pd.DataFrame()
    
    np.load = np_load_old
    return weather, ConsData, pcs, rowIndexes, myyield, phenotypes


# In[5]:


def buildCons(mysize,hidden_layer2,drop_out2):
    
    mInput = Input(shape=(mysize,),name="conIn")
    model = Dense(hidden_layer2, activation='relu',name="consDense1")(mInput)
    # model = Dropout(drop_out2)(model)
    mOutput = Dense(hidden_layer2, activation='linear',name="consDense2")(model)
    # mOutput = Dropout(drop_out2)(model)

    return mInput, mOutput

def buildWeather(window_size, width,hidden_layer1,drop_out1,filter_size,kernel_size,pool_size,strides):
    
    mInput = Input(shape=(window_size,width ),name="seqIn")
    model = Conv1D(filter_size, kernel_size= kernel_size, padding='valid', activation='relu',name="conv1")(mInput)
    model = AveragePooling1D(pool_size= pool_size, strides= strides, padding='same',name="pool1")(model)
    model = Conv1D(filter_size, kernel_size=kernel_size, padding='valid', activation='relu',name="conv2")(model)
    model = AveragePooling1D(pool_size= pool_size, strides= strides, padding='same',name="pool2")(model)
    model = Flatten(name="flatten")(model)
    model = Dense(hidden_layer1, activation='relu',name="denseSeq1")(model)
    model = Dropout(drop_out1,name="drop1")(model)
    model = Dense(hidden_layer1, activation='relu',name="denseSeq2")(model)
    model = Dropout(drop_out1,name="drop2")(model)
    mOutput =Dense(hidden_layer1,  activation='linear',name="seqOut")(model)

    return mInput, mOutput

def buildCombine(out1, out2,hidden_layer3,drop_out3):
    
    #mymodel = concatenate([out1, out2],name="concat")
    mymodel = Dense(hidden_layer3, activation='relu',name="combDense1")(out1)
    # mymodel = Dropout(drop_out3,name="combDrop1")(mymodel)
    mymodel = Dense(hidden_layer3, activation='relu',name="combDense2")(mymodel)
    # mymodel = Dropout(drop_out3,name="combDrop2")(mymodel)
    mymodel = Dense(hidden_layer3, activation='relu',name="combDense3")(mymodel)
    # mymodel = Dropout(drop_out3,name="combDrop3")(mymodel)
    mymodel = Dense(hidden_layer3, activation='relu',name="combDense4")(mymodel)
    # mymodel = Dropout(drop_out3,name="combDrop4")(mymodel)
    mymodel = Dense(hidden_layer3, activation='relu',name="combDense5")(mymodel)
    # mymodel = Dropout(drop_out3,name="combDrop5")(mymodel)
    mymodel = Dense(hidden_layer3, activation='relu',name="combDense6")(mymodel)
    # mymodel = Dropout(drop_out3,name="combDrop6")(mymodel)
    mymodel = Dense(hidden_layer3, activation='relu',name="combDense7")(mymodel)
    # mymodel = Dropout(drop_out3,name="combDrop7")(mymodel)
    mymodelOut = Dense(1, activation='linear',name="combout")(mymodel)
    
    return mymodelOut


# In[6]:


def setup_hist_model(weather, ConsData, args):
    myModelinWeather, myModelOutWeather = buildWeather(weather.shape[1], 8,
                                                       hidden_layer1=args['hidden_layer1'],
                                                       drop_out1=args['drop_out1'],
                                                       filter_size=args['filter_size'],
                                                       kernel_size=args['kernel_size'],
                                                       pool_size=args['pool_size'],
                                                       strides=args['strides'])

    
    #Consin, ConsOut = buildCons(ConsData.shape[1],
    #                                    hidden_layer2=args['hidden_layer2'],
    #                                    drop_out2=args['drop_out2'])
    ConsOut=[]
    GeneralOut = buildCombine(myModelOutWeather, ConsOut,
                                  hidden_layer3=args['hidden_layer3'],
                                  drop_out3=args['drop_out3'])

    myModel = Model(inputs=myModelinWeather, outputs=GeneralOut)    
        
    myModel.compile(loss="mse",
                    optimizer=tensorflow.keras.optimizers.Adam(lr=args['learning_rate'], beta_1=args["beta1"],
                                                               beta_2=args['beta2']),
                    metrics=['mean_absolute_error'])
    return myModel


# In[7]:


def fit_compiled_model(myModel, train_x_list, trainYield, val_x_list, valYield, batch_size, method):
    tmp_dir="../models/tmp_chk_dir_"+method+"/"
    os.system("mkdir "+tmp_dir)
    checkpoint_filepath = tmp_dir+"checkpoint_"+method+".h5"
    model_checkpoint_callback = tf.keras.callbacks.ModelCheckpoint(filepath=checkpoint_filepath,
                                                                   save_weights_only=True,
                                                                   monitor='val_loss',
                                                                   mode='min',
                                                                   save_best_only=True)
    
    print(len(train_x_list), len(val_x_list))
    myModel.fit(train_x_list, trainYield, validation_data=(val_x_list, valYield),
                batch_size=batch_size,
                epochs=50,
                verbose=1,
                shuffle=True,
                callbacks=[model_checkpoint_callback])
    
    # The model weights (that are considered the best) are loaded into the model.
    myModel.load_weights(checkpoint_filepath)
    return myModel


# In[8]:


def pred_write_to_file(myModel, tensors, index, method, split, tensor_set, rep, timestamp, results_file):
    #predict and write data to file
    prediction = predict(myModel, tensors[0])
    out_data = pd.DataFrame([index,tensors[-1],prediction.flatten()], index=["index","Observed","Predicted"]).T
    out_data["method"]=method
    out_data["split"]=split
    out_data["set"]=tensor_set
    out_data["rep"]=rep
    out_data["timestamp"]=timestamp
    out_data["index"] = out_data["index"].astype('int')
    out_data.to_csv(results_file, mode='a', index=False)
    return out_data

def predict(myModel, test_x_list):
    print(len(test_x_list))
    prediction = myModel.predict(test_x_list)
    return prediction

def predict_and_save_results(myModel, method, split, rep, results_file, indices, train_tensors, val_tensors, test_tensors):
    timestamp = datetime.timestamp(datetime.now())
    for tensor_set in ["train", "validation", "test"]:
        print(method, split, tensor_set, rep, timestamp)
        if tensor_set == "train":
            out_data = pred_write_to_file(myModel, train_tensors, indices[0], method, split, tensor_set, rep, timestamp, results_file)
        elif tensor_set == "validation":
            out_data = pred_write_to_file(myModel, val_tensors, indices[1], method, split, tensor_set, rep, timestamp, results_file)
        elif tensor_set == "test":
            out_data = pred_write_to_file(myModel, test_tensors, indices[2], method, split, tensor_set, rep, timestamp, results_file)
        print(len(out_data))
    return out_data


# In[9]:


#set input and output details

dataFolder="../data/"

#train_test_sets="Train_val_test_sets_13_Dec2019.json"
#train_test_sets="Train_val_test_sets_Practical_GEM_26Feb2020.json"
train_test_sets="Train_val_test_sets_E_dwnSample293_24Apr2020.json"
#train_test_sets="Train_val_test_sets_G_dwnSample12_23Apr2020.json"


reps = (0,16)
method = str(reps[0])+"-"+str(reps[1])+"_reps_val_training_WEATH_only_NO_HIST_"+train_test_sets[20:-5]
results_file = dataFolder+"Results/Output_predictions_"+method+".csv"


hist_split = "G_only"
print(results_file)


# In[10]:


#load in data
weather, ConsData, pcs, rowIndexes, myyield, phenotypes = readData(method)
#get training set indicies
with open(dataFolder+ train_test_sets, "r") as fp:
    sets = json.load(fp)
    print(train_test_sets)


# In[11]:


print(weather.shape, ConsData.shape, pcs.shape, myyield.shape)


# In[12]:


#setup historical model

args={
 'batch_size': 16,
 'beta1': 0.9892954491063367,
 'beta2': 0.9800504584722821,
 'drop_out1': 0.2,
 'drop_out2': 0,
 'drop_out3': 0,

 'filter_size': 128,
 'hidden_layer1': 32,
 'hidden_layer2': 128,
 'hidden_layer3': 16,
 'histLayerNum': 7,

 'kernel_size': 3,
 'learning_rate': 0.0001,
 'pool_size': 16,
 'strides': 3}

#histModel = setup_hist_model(weather, ConsData, args)


# In[13]:


'''
#RUN HISTORICAL MODEL

#create train, validation, and testing set
train_tensors, val_tensors, test_tensors, indices = get_train_val_test_sets(sets[hist_split], weather, ConsData, pcs, myyield)
#remove pcs from historical tensors
train_tensors = rmv_pcs(train_tensors)
val_tensors = rmv_pcs(val_tensors)
test_tensors = rmv_pcs(test_tensors)
print(len(train_tensors[0]), len(val_tensors[0]), len(test_tensors[0]))

#train and write results of historical model
for rep in range(reps[0], reps[1]):
    hist_mod_name= hist_split+"_"+method+"_"+str(rep) #name to save model files to
    print(hist_mod_name)
    #setup model
    histModel = setup_hist_model(weather, ConsData, args)
    #run model
    histModel = fit_compiled_model(histModel, train_tensors[:-1], train_tensors[-1], val_tensors[:-1], 
                                 val_tensors[-1], args['batch_size'], method)
    #save model for future use, run predictions, and clear gpu memory.
    saveModel(histModel, hist_mod_name)
    predict_and_save_results(histModel,method, hist_split, rep, results_file, indices, train_tensors, val_tensors, test_tensors)
    K.clear_session()
    gc.collect()
'''


# In[14]:


def get_tensors_sng_set(weather, ConsData, pcs, myyield, train_index):
    if len(weather)==0:
        trainSetweather = weather
    else:
        trainSetweather = weather[train_index, :, :].astype('float32')
    
    if len(ConsData)==0:
        trainSetCons = ConsData
    else:
        trainSetCons = ConsData[train_index, :].astype('float32')
        
    if len(pcs)==0:
        trainsetPCs = pcs
    else:
        trainsetPCs = pcs[train_index,:].astype('float32')
    
    trainYield = myyield[train_index].astype('float32')
    return trainSetweather, trainSetCons, trainsetPCs, trainYield

def get_train_val_test_sets(set_inds, weather, ConsData, pcs, myyield):
    train_index = set_inds["train"]
    val_index = set_inds["val"]
    test_index = set_inds["test"]

    train_tensors = get_tensors_sng_set(weather, ConsData, pcs, myyield, train_index)
    val_tensors = get_tensors_sng_set(weather, ConsData, pcs, myyield, val_index)
    test_tensors = get_tensors_sng_set(weather, ConsData, pcs, myyield, test_index)
    return train_tensors, val_tensors, test_tensors, [train_index, val_index, test_index]


# In[15]:


def getRetrainableModel(historicalModel, pcSize, hidden_layer4):
    x = historicalModel.layers[-2].output

    #pcInput = Input(shape=(pcSize,),name="pcIn")
    concatenatedmodel = x #concatenate([pcInput, x],name="pcConcat")


    concatenatedmodel = Dense(hidden_layer4, activation='relu',name="pcdense1")(concatenatedmodel)
    concatenatedmodel = Dense(hidden_layer4, activation='relu',name="pcdense2")(concatenatedmodel)
    concatenatedmodel = Dense(hidden_layer4, activation='relu',name="pcdense3")(concatenatedmodel)
    concatenatedmodel = Dense(hidden_layer4, activation='relu',name="pcdense4")(concatenatedmodel)

    concatenatedmodelOut = Dense(1, activation='linear',name="pcdenseout")(concatenatedmodel)

    trainableModel = Model(inputs=historicalModel.input,outputs=concatenatedmodelOut)
    return trainableModel
def saveModel(model, name):
    model_json = model.to_json()
    with open("../models/"+name+".json", "w") as json_file:
        json_file.write(model_json)
    # serialize weights to HDF5
    model.save_weights("../models/"+name+".h5")
    print("Saved model to disk")
    return
def loadModel(name):
    json_file = open("../models/"+name+".json", 'r')
    loaded_model_json = json_file.read()
    json_file.close()
    loaded_model = model_from_json(loaded_model_json)
    # load weights into new model
    loaded_model.load_weights("../models/"+name+".h5")
    print("Loaded model from disk")
    return loaded_model

def rmv_pcs(train_tensors):
    train_tensors = [train_tensors[0], train_tensors[1], train_tensors[3]]
    return train_tensors


# In[ ]:


hidden_layer4=16
for split in [x for x in sets.keys() if x != hist_split]:
    #run replicates
    for rep in range(reps[0], reps[1]):
        #create train, validation, and testing set
        train_tensors, val_tensors, test_tensors, indices = get_train_val_test_sets(sets[split], weather, ConsData,
                                                                                    pcs, myyield)
        print(split, len(train_tensors[0]), len(val_tensors[0]), len(test_tensors[0]))

        hist_mod_name= hist_split+"_"+method+"_"+str(rep)
        historicalModel=setup_hist_model(weather, ConsData, args)
        trainableModel=getRetrainableModel(historicalModel, pcs.shape[1], hidden_layer4)
        
        
        trainableModel.compile(loss="mse", optimizer=tensorflow.keras.optimizers.Adam(lr=args["learning_rate"], 
                                                                                      beta_1=args["beta1"],
                                                                                      beta_2=args["beta2"]),
                               metrics=['mean_absolute_error'])

        #run model
        trainableModel = fit_compiled_model(trainableModel, train_tensors[0], train_tensors[-1], val_tensors[0],
                                            val_tensors[-1], args['batch_size'], method)

        #save model for future use, run predictions, and clear gpu memory.
        mod_name = hist_mod_name+"_"+split
        saveModel(trainableModel, mod_name)
        predict_and_save_results(trainableModel, method, split, rep, results_file, indices, train_tensors, val_tensors, test_tensors)
        K.clear_session()
        gc.collect()


# In[ ]:




