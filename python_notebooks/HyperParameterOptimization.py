from hyperopt import tpe, hp, fmin
import pandas as pd
import numpy as np
from keras.models import Model
from keras.layers import Dense, Dropout, Flatten, Input, concatenate,BatchNormalization, Activation
from keras.layers import Conv1D,AveragePooling1D
import keras
from keras import regularizers
from keras import backend as K

import json

import random
random.seed(45)
from scipy.stats import pearsonr
from scipy.stats import spearmanr
import tensorflow as tf
from numpy.random import seed
seed(1)
from tensorflow import set_random_seed
set_random_seed(2)
config = tf.ConfigProto(allow_soft_placement=True)
config.gpu_options.per_process_gpu_memory_fraction = 0.2
sess = tf.Session(config=config)
K.set_session(sess)
np_load_old = np.load

def transformWeather(weather):
    transW=np.zeros((weather.shape[0], weather.shape[1], weather.shape[2]))
    MaxMap=np.zeros(( weather.shape[1], weather.shape[2]))
    minMap = np.zeros((weather.shape[1], weather.shape[2]))
    for i in range(weather.shape[1]):
        for j in range(weather.shape[2]):
            mn = np.min(weather[:,i,j])
            mx= np.max(weather[:,i,j])
            transW[:,i,j]=(weather[:,i,j]-mn)/(mx-mn+1)
            minMap[i,j]=mn
            MaxMap[i,j]=mx

    return minMap, MaxMap, transW


def transformAccordingtoMap(weather, minMap, MaxMap):
    transW = np.zeros((weather.shape[0], weather.shape[1], weather.shape[2]))
    for i in range(weather.shape[1]):
        for j in range(weather.shape[2]):
            mn = minMap[i,j]
            mx = MaxMap[i,j]
            transW[:, i, j] = (weather[:, i, j] - mn) / (mx - mn + 1)
    return transW

def transformMatrix(M):
    trM = np.zeros((M.shape[0],M.shape[1]))
    MaxMap = np.zeros(M.shape[1])
    minMap =np.zeros(M.shape[1])
    for i in range(M.shape[1]):
        mn = np.min(M[:, i])
        mx = np.max(M[:, i])
        trM[:, i] = (M[:, i] - mn) / (mx - mn + 1)
        minMap[i] = mn
        MaxMap[i] = mx


    return minMap, MaxMap, trM

def transformMatrixAccordingtoMap(M, minMap, MaxMap):
    trM = np.zeros((M.shape[0], M.shape[1]))

    for i in range(M.shape[1]):
        mn = minMap[i]
        mx = MaxMap[i]
        trM[:, i] = (M[:, i] - mn) / (mx - mn + 1)

    return trM

def objective_func(args):

    with open("/home/ec796/workSpace/GxE/Datasets/december-data/Train_val_test_sets_25_Nov2019.json", "r") as fp:
        sets = json.load(fp)
    fertility, soils, general, weather, myyield, Pcs = readData()

    hidden_layer=args['hidden_layer']
    hidden_layerNumber= args['hidden_layerNumber']

    hidden_layerNumberg2f = args['hidden_layerNumberg2f']
    hidden_layerSizeg2f = args['hidden_layerSizeg2f']
    tail_layerNumber = args['tail_layerNumber']
    tail_layerSize = args['tail_layerSize']


    conv_Number = args['conv_Number']
    drop_out=args['drop_out']
    batch_size=args['batch_size']
    beta1=args['beta1']
    beta2=args['beta2']
    learning_rate=args['learning_rate']
    filter_size=args['filter_size']
    kernel_size=args['kernel_size']
    pool_size=args['pool_size']
    strides=args['strides']
    try:
        hModel ,WminMap, WmaxMap, CminMap, CmaxMap = trainHistory(sets, soils, general, weather, myyield,
                                          hidden_layer=hidden_layer,
                                          hidden_layerNumber=hidden_layerNumber,
                                          conv_Number=conv_Number,
                                          drop_out=drop_out,
                                            batch_size = batch_size,
                                            beta1 = beta1,
                                            beta2 =beta2,
                                            learning_rate =learning_rate,
                                            filter_size =filter_size,
                                            kernel_size = kernel_size,
                                            pool_size = pool_size,
                                            strides = strides
        )
        perf=trainG2F(Pcs, sets, hModel,fertility, soils, general, weather, myyield,hidden_layerNumberg2f,hidden_layerSizeg2f,tail_layerNumber,tail_layerSize ,WminMap, WmaxMap, CminMap, CmaxMap,beta1,beta2,learning_rate,batch_size)
    except:
        perf=-1 # equals r=0





    return 1-perf

def buildCombine(out1, out2,
                 hidden_layer,
                 drop_out,
                 hidden_layerNumber
                 ):
    mymodel = concatenate([out1, out2])

    for i in range(hidden_layerNumber):
        mymodel = Dense(hidden_layer)(mymodel)
        mymodel=Activation('relu')(mymodel)
        mymodel = BatchNormalization()(mymodel)
        mymodel = Dropout(drop_out)(mymodel)
    mymodelOut = Dense(1, activation='linear')(mymodel)

    return mymodelOut

def buildSeq(window_size, width,
            conv_Number,
             filter_size,
             kernel_size,
            pool_size,
           strides
             ):
    mInput = Input(shape=(window_size, width ))
    if conv_Number ==1:
        model = Conv1D(filter_size, kernel_size=kernel_size, padding='valid', activation='relu', kernel_initializer='random_uniform', bias_initializer='zeros', kernel_regularizer=regularizers.l2())(mInput)
        model = Conv1D(filter_size, kernel_size=kernel_size, padding='valid', activation='relu',kernel_initializer='random_uniform', bias_initializer='zeros', kernel_regularizer=regularizers.l2())(model)
        model = BatchNormalization()(model)
        model = AveragePooling1D(pool_size= pool_size, strides=strides, padding='same')(model)
    elif conv_Number == 2:
        model = Conv1D(filter_size, kernel_size=kernel_size, padding='valid', activation='relu', kernel_initializer='random_uniform', bias_initializer='zeros', kernel_regularizer=regularizers.l2())(mInput)
        model = Conv1D(filter_size, kernel_size=kernel_size, padding='valid', activation='relu',kernel_initializer='random_uniform', bias_initializer='zeros', kernel_regularizer=regularizers.l2())(model)
        model = BatchNormalization()(model)
        model = AveragePooling1D(pool_size= pool_size, strides=strides, padding='same')(model)

        model = Conv1D(filter_size, kernel_size=kernel_size, padding='valid', activation='relu', kernel_initializer='random_uniform', bias_initializer='zeros', kernel_regularizer=regularizers.l2())(model)
        model = Conv1D(filter_size, kernel_size=kernel_size, padding='valid', activation='relu',kernel_initializer='random_uniform', bias_initializer='zeros', kernel_regularizer=regularizers.l2())(model)
        model = BatchNormalization()(model)
        model = AveragePooling1D(pool_size= pool_size, strides=strides, padding='same')(model)
    elif conv_Number == 3:
        model = Conv1D(filter_size, kernel_size=kernel_size, padding='valid', activation='relu', kernel_initializer='random_uniform', bias_initializer='zeros', kernel_regularizer=regularizers.l2())(mInput)
        model = Conv1D(filter_size, kernel_size=kernel_size, padding='valid', activation='relu',kernel_initializer='random_uniform', bias_initializer='zeros', kernel_regularizer=regularizers.l2())(model)
        model = BatchNormalization()(model)
        model = AveragePooling1D(pool_size= pool_size, strides=strides, padding='same')(model)

        model = Conv1D(filter_size, kernel_size=kernel_size, padding='valid', activation='relu', kernel_initializer='random_uniform', bias_initializer='zeros', kernel_regularizer=regularizers.l2())(model)
        model = Conv1D(filter_size, kernel_size=kernel_size, padding='valid', activation='relu',kernel_initializer='random_uniform', bias_initializer='zeros', kernel_regularizer=regularizers.l2())(model)
        model = BatchNormalization()(model)
        model = AveragePooling1D(pool_size= pool_size, strides=strides, padding='same')(model)

        model = Conv1D(filter_size, kernel_size=kernel_size, padding='valid', activation='relu', kernel_initializer='random_uniform', bias_initializer='zeros', kernel_regularizer=regularizers.l2())(model)
        model = Conv1D(filter_size, kernel_size=kernel_size, padding='valid', activation='relu',kernel_initializer='random_uniform', bias_initializer='zeros', kernel_regularizer=regularizers.l2())(model)
        model = BatchNormalization()(model)
        model = AveragePooling1D(pool_size= pool_size, strides=strides, padding='same')(model)



    mOutput = Flatten()(model)
    return mInput, mOutput


def buildFeatures(mysize):
    mInput = Input(shape=(mysize,))
    return mInput,mInput
def readData():
    np.load = lambda *a, **k: np_load_old(*a, allow_pickle=True, **k)
    soils2 = np.load("/home/ec796/workSpace/GxE/Datasets/december-data/Soils.npy")

    weather = np.load("/home/ec796/workSpace/GxE/Datasets/december-data/Weather_season_only.npy")
    myyield = np.load("/home/ec796/workSpace/GxE/Datasets/december-data/Yield.npy")

    phenotypes=pd.read_csv("/home/ec796/workSpace/GxE/Datasets/december-data/index_file.csv", index_col=0, low_memory=False)
    #for historical data we want to include 'Year', 'Latitude', 'Longitude', 'Altitude', 'Irrigated', 'Plant Density'
    general = phenotypes[['Year', 'Latitude', 'Longitude', 'Altitude', 'Plant Density']].fillna(-1).values
    #extra fertility data available only for the G2F
    fertility = phenotypes[['% Clay', '% Sand', '% Silt', '%Ca Sat', '%H Sat','%K Sat', '%Mg Sat', '%Na Sat',
                            '1:1 S Salts mmho/cm', '1:1 Soil pH','Calcium ppm Ca', 'Magnesium ppm Mg',
                            'Mehlich P-III ppm P','Nitrate-N ppm N', 'Organic Matter LOI %', 'Potassium ppm K',
                            'Sodium ppm Na', 'Sulfate-S ppm S', 'WDRF Buffer pH', 'lbs N/A', 'Total K lbs/acre',
                            'Total N lbs/acre', 'Total P lbs/acre', 'Irrigation amount (inches)', 'Irrigated']].fillna(-1).values
    pcs = np.load("/home/ec796/workSpace/GxE/Datasets/december-data/PCs.npy")
    np.load = np_load_old
    return fertility, soils2, general, weather, myyield,pcs

def trainHistory(sets, soilsOld, generalOld, weatherOld, myyieldOld,  hidden_layer,
                                    hidden_layerNumber ,
                                    conv_Number,
                                    drop_out ,
                                    batch_size ,
                                    beta1,
                                    beta2,
                                    learning_rate ,
                                    filter_size ,
                                    kernel_size ,
                                    pool_size ,
                                    strides ):
    soils, general, weather, myyield =  soilsOld, generalOld, weatherOld, myyieldOld
    value = sets["Historical"]
    train_index1 = value["train"]
    train_index2 = value["val"]
    train_index = list(np.concatenate([np.array(train_index1),np.array(train_index2)]))


    weather = np.transpose(weather,(0,2,1))
    # SOIL
    soils = soils.reshape(soils.shape[0], soils.shape[1] * soils.shape[2])



    # SOIL + GENERAL
    ConsData = np.concatenate([soils, general], axis=1)
    # ConsData = soils

    trainSetweather = weather[train_index, :, :]
    trainSetCons = ConsData[train_index, :]
    trainYield = myyield[train_index]


    myModelinWeather, myModelOutWeather = buildSeq(trainSetweather.shape[1], trainSetweather.shape[2],
                                                   conv_Number=conv_Number,
                                                   filter_size=filter_size,
                                                   kernel_size=kernel_size,
                                                   pool_size=pool_size,
                                                   strides=strides
                                                   )
    Consin, ConsOut = buildFeatures(trainSetCons.shape[1] )

    GeneralOut = buildCombine(myModelOutWeather, ConsOut,
                              hidden_layer=hidden_layer,
                              drop_out=drop_out,
                              hidden_layerNumber=hidden_layerNumber
                              )




    myModel = Model(inputs=[myModelinWeather, Consin], outputs=GeneralOut)

    myModel.compile(loss="mse",
                    optimizer=keras.optimizers.Adam(lr=learning_rate, beta_1=beta1,beta_2=beta2),
                    metrics=['mean_absolute_error'])
####################################################################################################################
    WminMap, WmaxMap,trainSetweather=transformWeather(trainSetweather)
    CminMap, CmaxMap, trainSetCons= transformMatrix(trainSetCons)


######################################################################################################################
    myModel.fit([trainSetweather, trainSetCons], trainYield,
                batch_size=batch_size,
                epochs=30,
                verbose=1)


    return myModel ,WminMap, WmaxMap, CminMap, CmaxMap

def trainG2F(PcsOld, sets, hModel,fertilityOld, soilsOld, generalOld, weatherOld, myyieldOld,hidden_layerNumberg2f,hidden_layerSizeg2f,tail_layerNumber,tail_layerSize ,WminMap, WmaxMap, CminMap, CmaxMap,beta1,beta2,learning_rate,batch_size):
    counter=0
    lim=5
    PRSN=[]
    for iterSet in sets:
        fertility, soils, general, weather, myyield, Pcs = fertilityOld, soilsOld, generalOld, weatherOld, myyieldOld, PcsOld[:,0:30]
        counter=counter+1
        historicalModel = hModel


        if counter>=lim:
            break

        value = sets[iterSet]
        train_index1 = value["train"]
        train_index2 = value["val"]
        train_index = list(np.concatenate([np.array(train_index1), np.array(train_index2)]))
        test_index = value["test"]

        weather = np.transpose(weather, (0, 2, 1))
        # SOIL
        soils = soils.reshape(soils.shape[0], soils.shape[1] * soils.shape[2])

        # general = getGeneral(general)

        # SOIL + GENERAL
        ConsData = np.concatenate([soils, general], axis=1)
        # ConsData = soils

        Pcs = np.concatenate([Pcs, fertility], axis=1)

        trainSetweather = weather[train_index, :, :]
        trainSetCons = ConsData[train_index, :]
        trainsetPCs = Pcs[train_index, :]
        trainYield = myyield[train_index]
        testSetweather = weather[test_index, :, :]
        testSetCons = ConsData[test_index, :]
        testsetPCs = Pcs[test_index, :]
        testYield = myyield[test_index]

        PCminMap, PCmaxMap, trainsetPCs = transformMatrix(trainsetPCs)
        testsetPCs = transformMatrixAccordingtoMap(testsetPCs, PCminMap, PCmaxMap)

        trainSetweather = transformAccordingtoMap(trainSetweather, WminMap, WmaxMap)
        testSetweather = transformAccordingtoMap(testSetweather, WminMap, WmaxMap)
        trainSetCons = transformMatrixAccordingtoMap(trainSetCons, CminMap, CmaxMap)
        testSetCons = transformMatrixAccordingtoMap(testSetCons, CminMap, CmaxMap)

        trainableModel = getRetrainableModel(historicalModel, Pcs.shape[1], hidden_layerNumberg2f,hidden_layerSizeg2f,tail_layerNumber,tail_layerSize)

        trainableModel.compile(loss="mse",
                               optimizer=keras.optimizers.Adam(lr=learning_rate,
                                                               beta_1=beta1,
                                                               beta_2=beta2),
                               metrics=['mean_absolute_error'])

        trainableModel.fit([trainSetweather, trainSetCons, trainsetPCs], trainYield,
                           batch_size=batch_size,
                           epochs=30,
                           verbose=1)

        real = []
        PR = []
        prediction = trainableModel.predict([testSetweather, testSetCons, testsetPCs])

        for i in range(len(prediction)):
            PR.append(prediction[i][0])
            real.append(testYield[i])

        prsn = pearsonr(np.array(real), np.array(PR))

        prsn = prsn[0]
        if np.isnan(prsn):
            prsn=-1
        PRSN.append(prsn)

    return np.mean(np.array(PRSN))

def getRetrainableModel(historicalModel,pcSize, hidden_layerNumberg2f,hidden_layerSizeg2f,tail_layerNumber,tail_layerSize):


    x = historicalModel.layers[-2].output

    pcInput = Input(shape=(pcSize,))

    PCmodel = Dense(hidden_layerSizeg2f)(pcInput)
    PCmodel = Activation('relu')(PCmodel)
    PCmodel = BatchNormalization()(PCmodel)

    if hidden_layerNumberg2f >2:
        for iii in range(2,hidden_layerNumberg2f):
            PCmodel = Dense(hidden_layerSizeg2f)(PCmodel)
            PCmodel = Activation('relu')(PCmodel)
            PCmodel = BatchNormalization()(PCmodel)


    PCmodel = Dense(hidden_layerSizeg2f)(PCmodel)
    PCmodel = Activation('linear')(PCmodel)
    PCmodelOut = BatchNormalization()(PCmodel)


    concatenatedmodel = concatenate([PCmodelOut, x])

    for iii in range(tail_layerNumber):
        concatenatedmodel = Dense(tail_layerSize)(concatenatedmodel)
        concatenatedmodel = Activation('relu')(concatenatedmodel)
        concatenatedmodel = BatchNormalization()(concatenatedmodel)


    concatenatedmodelOut = Dense(1, activation='linear')(concatenatedmodel)

    trainableModel = Model(inputs=[historicalModel.input[0],historicalModel.input[1],pcInput],outputs=concatenatedmodelOut)

    return trainableModel


space = {'hidden_layer': hp.choice('hidden_layer',[4,8,16,32,64,128]),
        'hidden_layerNumber': hp.choice('hidden_layerNumber',[2,3,4,5,6]),
        'conv_Number': hp.choice('conv_Number',[1,2,3]),
        'drop_out': hp.choice('drop_out',[0,0.05, 0.1,0.2,0.3]),
        'batch_size': hp.choice('batch_size',[16, 32, 64, 128]),
        'beta1': hp.uniform('beta1',0.9, 0.9999),
        'beta2': hp.uniform('beta2',0.9, 0.9999),
        'learning_rate': hp.choice('learning_rate',[0.1, 0.01, 0.001, 0.0001]),
        'filter_size': hp.choice('filter_size', [4, 8, 16, 32, 64, 128]),
        'kernel_size': hp.choice('kernel_size', [1, 3, 5, 8, 16, 32]),
        'pool_size': hp.choice('pool_size', [3, 5, 8, 16, 32]),
        'strides': hp.choice('strides', [1, 3, 5, 8]),
         'hidden_layerNumberg2f' : hp.choice('hidden_layerNumberg2f',[2,3,4,5]),
        'hidden_layerSizeg2f': hp.choice('hidden_layerSizeg2f',[4,8,16,32,64,128]),
        'tail_layerNumber' :  hp.choice('tail_layerNumber',[1,2,3,4,5]),
        'tail_layerSize':hp.choice('tail_layerSize',[4,8,16,32,64,128])
        }

best_classifier = fmin(objective_func,space, algo=tpe.suggest,max_evals=30)
print(best_classifier)
