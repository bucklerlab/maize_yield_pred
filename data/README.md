This directory is for data. Since git repositories are not designed for large amounts of data the data must be downloaded onto the machine on which you wish to run the scripts.

pre-compiled data can be found at: https://data.cyverse.org/dav-anon/iplant/home/washjake/CNN_data_for_ms11May2021.tar.gz