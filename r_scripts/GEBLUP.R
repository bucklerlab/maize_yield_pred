# Title     : Clustering genotypes for cross-validation
# Created by: Guillaume Ramstein (gr226@cornell.edu)
# Created on: 5/30/19

#--------------------------------------------------------
# Script parameters
#--------------------------------------------------------
overwrite <- FALSE

# Working directory
wd <- ifelse(grepl("gr226", tolower(Sys.info()["nodename"])), "~/Documents", "/workdir/gr226")
dir.create(wd, showWarnings = FALSE)
setwd(wd)

# Input
input_file <- "gxe_prediction/data/G2F_input.csv"

split_file <- "gxe_prediction/data/Train_val_test_sets_13_Dec2019.json"

soil_ECs <- c(
  "latitude", "longitude", "altitude", "irrigated", "density",
  "clay", "sand", "silt",
  "Ca_sat", "H_sat", "K_sat", "Mg_sat", "Na_sat",
  "salts", "pH",
  "Ca", "Mg", "P", "N", "LOI", "K", "Na", "S",
  "pH_buffer",
  "N_A",
  "K_acre", "N_acre", "P_acre",
  "irrigation",
  "awc", "caco3", "cec7", "claytotal",
  "dbovendry", "ec", "gypsum", "ksat",
  "om", "ph1to1h2o", "sandtotal", "silttotal",
  "slope")

cnn_ECs <- NULL

scale_ECs <- TRUE

weather_ECs <- c("day_of_year",
                 "day_length",
                 "max_temp", "min_temp",
                 "precip",
                 "radn",
                 "vapor_pressure",
                 "CTT")

w <- 3

# Output
store_ERM <- FALSE
ERM_files <- paste0("gxe_prediction/data/ERM_weather-w=", w, ".rds")

CV_file <- paste0("gxe_prediction/data/CV_results-w=", w, ".txt")
Ypred_file <- paste0("gxe_prediction/data/Ypred-w=", w, ".txt")

#--------------------------------------------------------
# Functions
#--------------------------------------------------------
# Functions
library(data.table)
library(foreach)
library(dplyr)
library(ggplot2)
library(rjson)

# Environmental relationship matrices
make_ERM <- function(E_list, w, summary_functions=NULL) {
  
  foreach(E=E_list, .combine="+") %do% {
    
    # Time bins
    windows <- cut_interval(1:nrow(E), length=w)
    
    if (length(unique(windows)) == 1) {
      Z <- matrix(1, nrow=nrow(E), ncol=1)
    } else {
      Z <- model.matrix(~ windows)
    }
    
    # Average by time bin
    EC <- crossprod(Z, E) %>% t %>% scale %>% t %>% na.omit
    
    # Summary by time bin
    if (! is.null(summary_functions) ) {
      
      EC_summary <- foreach(summary_function=summary_functions) %do% {
        
        summary_by_window <- by(E, windows, function(x) apply(x, 2, summary_function))
        
        do.call(rbind, summary_by_window) %>% t %>% scale %>% t %>% na.omit
        
      } %>% as.list
      
      EC <- do.call(rbind, append(list(EC), EC_summary))
      
    }
    
    # Environmental relationship matrix
    return(crossprod(EC))
    
  }
  
}

# Regress function
regress <- function (formula, Vformula, identity = TRUE, kernel = NULL, 
          start = NULL, taper = NULL, pos, verbose = 0, gamVals = NULL, 
          maxcyc = 50, tol = 1e-04, data) 
{
  
  require(MASS)
  
  if (verbose > 9) 
    cat("Extracting objects from call\n")
  if (missing(data)) 
    data <- environment(formula)
  mf <- model.frame(formula, data = data, na.action = na.pass)
  mf <- eval(mf, parent.frame())
  y <- model.response(mf)
  model <- list()
  model <- c(model, mf)
  if (missing(Vformula)) 
    Vformula <- NULL
  isNA <- apply(is.na(mf), 1, any)
  if (!is.null(Vformula)) {
    V <- model.frame(Vformula, data = data, na.action = na.pass)
    V <- eval(V, parent.frame())
    mfr <- is.na(V)
    if (ncol(mfr) == 1) {
      isNA <- isNA | mfr
    }
    else {
      isNA <- isNA | apply(mfr[, !apply(mfr, 2, all)], 
                           1, any)
    }
    rm(mfr)
    Vcoef.names <- names(V)
    V <- as.list(V)
    k <- length(V)
  }
  else {
    V <- NULL
    k <- 0
    Vcoef.names = NULL
  }
  if (ncol(mf) == 1) 
    mf <- cbind(mf, 1)
  X <- model.matrix(formula, mf[!isNA, ])
  y <- y[!isNA]
  n <- length(y)
  Xcolnames <- dimnames(X)[[2]]
  if (is.null(Xcolnames)) {
    Xcolnames <- paste("X.column", c(1:dim(as.matrix(X))[2]), 
                       sep = "")
  }
  X <- matrix(X, n, length(X)/n)
  qr <- qr(X)
  rankQ <- n - qr$rank
  if (qr$rank) {
    X <- matrix(X[, qr$pivot[1:qr$rank]], n, qr$rank)
    Xcolnames <- Xcolnames[qr$pivot[1:qr$rank]]
  }
  else {
    cat("\nERROR: X has rank 0\n\n")
  }
  if (verbose > 9) 
    cat("Setting up kernel\n")
  if (missing(kernel)) {
    K <- X
    colnames(K) <- Xcolnames
    reml <- TRUE
    kernel <- NULL
  }
  else {
    if (length(kernel) == 1 && kernel > 0) {
      K <- matrix(rep(1, n), n, 1)
      colnames(K) <- c("1")
    }
    if (length(kernel) == 1 && kernel <= 0) {
      K <- Kcolnames <- NULL
      KX <- X
      rankQK <- n
    }
    if (length(kernel) > 1) {
      if (is.matrix(kernel)) {
        K <- kernel[!isNA, ]
      }
      else {
        K <- model.frame(kernel, data = data, na.action = na.pass)
        K <- eval(K, parent.frame())
        if (ncol(K) == 1) {
          dimNamesK <- dimnames(K)
          K <- K[!isNA, ]
          dimNamesK[[1]] <- dimNamesK[[1]][!isNA]
          K <- data.frame(V1 = K)
          dimnames(K) <- dimNamesK
        }
        else {
          K <- K[!isNA, ]
        }
        K <- model.matrix(kernel, K)
      }
    }
    reml <- FALSE
  }
  if (!is.null(K)) {
    Kcolnames <- colnames(K)
    qr <- qr(K)
    rankQK <- n - qr$rank
    if (qr$rank == 0) 
      K <- NULL
    else {
      K <- matrix(K[, qr$pivot[1:qr$rank]], n, qr$rank)
      Kcolnames <- Kcolnames[qr$pivot[1:qr$rank]]
      KX <- cbind(K, X)
      qr <- qr(KX)
      KX <- matrix(KX[, qr$pivot[1:qr$rank]], n, qr$rank)
    }
  }
  if (missing(maxcyc)) 
    maxcyc <- 50
  if (missing(tol)) 
    tol <- 1e-04
  delta <- 1
  if (verbose > 9) 
    cat("Removing parts of random effects corresponding to missing values\n")
  for (i in 1:k) {
    if (is.matrix(V[[i]])) {
      V[[i]] <- V[[i]][!isNA, !isNA]
    }
    if (is.factor(V[[i]])) {
      V[[i]] <- V[[i]][!isNA]
    }
  }
  In <- diag(rep(1, n), n, n)
  if (identity) {
    V[[k + 1]] <- as.factor(1:n)
    names(V)[k + 1] <- "In"
    k <- k + 1
    Vcoef.names <- c(Vcoef.names, "In")
    Vformula <- as.character(Vformula)
    Vformula[1] <- "~"
    Vformula[2] <- paste(Vformula[2], "+In")
    Vformula <- as.formula(Vformula)
  }
  model <- c(model, V)
  model$formula <- formula
  model$Vformula <- Vformula
  if (!missing(pos)) 
    pos <- as.logical(pos)
  if (missing(pos)) 
    pos <- rep(FALSE, k)
  if (length(pos) < k) 
    cat("Warning: argument pos is only partially specified; additional terms (n=", 
        k - length(pos), ") set to FALSE internally.\n", 
        sep = "")
  pos <- c(pos, rep(FALSE, k))
  pos <- pos[1:k]
  if (verbose > 9) 
    cat("Checking if we can apply the Sherman Morrison Woodbury identites for matrix inversion\n")
  if (all(sapply(V, is.factor)) & k > 2) {
    SWsolveINDICATOR <- TRUE
  }
  else SWsolveINDICATOR <- FALSE
  Z <- list()
  for (i in 1:length(V)) {
    if (is.factor(V[[i]])) {
      Vi <- model.matrix(~V[[i]] - 1)
      colnames(Vi) <- levels(V[[i]])
      Z[[i]] <- Vi
      V[[i]] <- tcrossprod(Vi)
    }
    else {
      Z[[i]] <- V[[i]]
    }
  }
  names(Z) <- names(V)
  A <- matrix(rep(0, k^2), k, k)
  entries <- expand.grid(1:k, 1:k)
  x <- rep(0, k)
  sigma <- c(1, rep(0, k - 1))
  stats <- rep(0, 0)
  if (missing(taper)) {
    taper <- rep(0.9, maxcyc)
    if (missing(start) && k > 1) 
      taper[1:2] <- c(0.5, 0.7)
  }
  else {
    taper <- pmin(abs(taper), 1)
    if ((l <- length(taper)) < maxcyc) 
      taper <- c(taper, rep(taper[l], maxcyc - l))
  }
  if (!is.null(start)) {
    start <- c(start, rep(1, k))
    start <- start[1:k]
  }
  if (k > 2 && is.null(start)) 
    start <- rep(var(y, na.rm = TRUE), k)
  if (k == 1 && is.null(start)) 
    start <- var(y, na.rm = TRUE)
  if (is.null(start) && k == 2) {
    if (missing(gamVals)) {
      gamVals <- seq(0.01, 0.02, length = 3)^2
      gamVals <- sort(c(gamVals, seq(0.1, 0.9, length = 3), 
                        1 - gamVals))
      gamVals <- 0.5
    }
    if (length(gamVals) > 1) {
      if (verbose >= 1) 
        cat("Evaluating the llik at gamma = \n")
      if (verbose >= 1) 
        cat(gamVals)
      if (verbose >= 1) 
        cat("\n")
      reg.obj <- reml(gamVals, y, X, V[[1]], V[[2]], verbose = verbose)
      llik <- reg.obj$llik
      llik <- as.double(llik)
      if (verbose >= 2) 
        cat(llik, "\n")
      gam <- gamVals[llik == max(llik)]
      gam <- gam[1]
      if (verbose >= 2) 
        cat("MLE is near", gam, "and llik =", max(llik), 
            "there\n")
    }
    if (length(gamVals) == 1) {
      gam <- gamVals[1]
      reg.obj <- list(rms = var(y))
    }
    start <- c(1 - gam, gam) * reg.obj$rms
    if (gam == 0.9999) {
      taper[1] <- taper[1]/100
      maxcyc <- maxcyc * 10
    }
    if (verbose >= 1) 
      cat(c("start algorithm at", round(start, 4), "\n"))
  }
  if (is.null(start) & k > 2) {
    LLvals <- NULL
    V2 <- V[[2]]
    for (ii in 3:k) V2 <- V2 + V[[ii]]
    LLvals <- c(LLvals, reml(0.5, y, X, V[[1]], V2)$llik)
    V2 <- V[[1]] + V2
    for (ii in 1:k) {
      V2 <- V2 - V[[ii]]
      LLvals <- c(LLvals, reml(0.75, y, X, V2, V[[ii]])$llik)
    }
    best <- which.max(LLvals)
    if (verbose) {
      cat("Checking starting points\n")
      cat("llik values of", LLvals, "\n")
    }
    if (best == 1) {
      start <- rep(var(y, na.rm = TRUE), k)
    }
    else {
      start <- rep(0.25, k)
      start[best] <- 0.75
    }
  }
  sigma <- coef <- start
  coef[pos] <- log(sigma[pos])
  coef[!pos] <- sigma[!pos]
  T <- vector("list", length = k)
  for (ii in 1:k) T[[ii]] <- matrix(NA, n, n)
  for (cycle in 1:maxcyc) {
    ind <- which(pos)
    if (length(ind)) {
      coef[ind] <- pmin(coef[ind], 20)
      coef[ind] <- pmax(coef[ind], -20)
      sigma[ind] <- exp(coef[ind])
    }
    if (verbose) {
      cat(cycle, "sigma =", sigma)
    }
    if (!SWsolveINDICATOR) {
      Sigma <- 0
      for (i in 1:k) Sigma <- Sigma + V[[i]] * sigma[i]
      W <- solve(Sigma, In)
    }
    else {
      W <- SWsolve2(Z[1:(k - 1)], sigma)
    }
    if (is.null(K)) 
      WQK <- W
    else {
      WK <- W %*% K
      WQK <- W - WK %*% solve(t(K) %*% WK, t(WK))
    }
    if (reml) 
      WQX <- WQK
    else {
      WX <- W %*% KX
      WQX <- W - WX %*% solve(t(KX) %*% WX, t(WX))
    }
    rss <- as.numeric(t(y) %*% WQX %*% y)
    sigma <- sigma * rss/rankQK
    coef[!pos] <- sigma[!pos]
    coef[pos] <- log(sigma[pos])
    WQK <- WQK * rankQK/rss
    WQX <- WQX * rankQK/rss
    rss <- rankQK
    eig <- sort(eigen(WQK, symmetric = TRUE, only.values = TRUE)$values, 
                decreasing = TRUE)[1:rankQK]
    if (any(eig < 0)) {
      cat("error: Sigma is not positive definite on contrasts: range(eig)=", 
          range(eig), "\n")
      WQK <- WQK + (tol - min(eig)) * diag(nobs)
      eig <- eig + tol - min(eig)
    }
    ldet <- sum(log(eig))
    llik <- ldet/2 - rss/2
    if (cycle == 1) 
      llik0 <- llik
    delta.llik <- llik - llik0
    llik0 <- llik
    if (verbose) {
      if (reml) 
        cat(" resid llik =", llik, "\n")
      else cat(" llik =", llik, "\n")
      cat(cycle, "adjusted sigma =", sigma)
      if (cycle > 1) {
        if (reml) 
          cat(" delta.llik =", delta.llik, "\n")
        else cat(" delta.llik =", delta.llik, "\n")
      }
      else cat("\n")
    }
    x <- NULL
    var.components <- rep(1, k)
    ind <- which(pos)
    if (length(ind)) 
      var.components[ind] <- sigma[ind]
    if (!SWsolveINDICATOR) {
      if (identity) {
        T[[k]] <- WQK
        if (k > 1) {
          for (ii in (k - 1):1) T[[ii]] <- WQK %*% V[[ii]]
        }
      }
      else {
        for (ii in 1:k) T[[ii]] <- WQK %*% V[[ii]]
      }
    }
    else {
      if (identity) {
        T[[k]] <- WQK
        if (k > 1) {
          for (ii in (k - 1):1) T[[ii]] <- tcrossprod(WQK %*% 
                                                        Z[[ii]], Z[[ii]])
        }
      }
      else {
        for (ii in 1:k) T[[ii]] <- tcrossprod(WQK %*% 
                                                Z[[ii]], Z[[ii]])
      }
    }
    x <- sapply(T, function(x) as.numeric(t(y) %*% x %*% 
                                            WQX %*% y - sum(diag(x))))
    x <- x * var.components
    ff <- function(x) sum(T[[x[1]]] * t(T[[x[2]]])) * var.components[x[1]] * 
      var.components[x[2]]
    aa <- apply(entries, 1, ff)
    A[as.matrix(entries)] <- aa
    stats <- c(stats, llik, sigma[1:k], x[1:k])
    if (verbose >= 9) {
    }
    A.svd <- ginv(A)
    x <- A.svd %*% x
    if (qr(A)$rank < k) {
      if (cycle == 1) {
        if (verbose) {
          cat("Warning: Non identifiable dispersion model\n")
          cat(sigma)
          cat("\n")
        }
      }
    }
    coef <- coef + taper[cycle] * x
    sigma[!pos] <- coef[!pos]
    sigma[pos] <- exp(coef[pos])
    if (cycle > 1 & abs(delta.llik) < tol * 10) 
      break
    if (max(abs(x)) < tol) 
      break
  }
  if (!SWsolveINDICATOR) {
    Sigma <- 0
    for (i in 1:k) Sigma <- Sigma + V[[i]] * sigma[i]
    W <- solve(Sigma, In)
  }
  else {
    W <- SWsolve2(Z[1:(k - 1)], sigma)
  }
  if (cycle == maxcyc) {
    if (verbose) 
      cat("WARNING:  maximum number of cycles reached before convergence\n")
  }
  stats <- as.numeric(stats)
  stats <- matrix(stats, cycle, 2 * k + 1, byrow = TRUE)
  colnames(stats) <- c("llik", paste("s^2_", Vcoef.names, sep = ""), 
                       paste("der_", Vcoef.names, sep = ""))
  WX <- W %*% X
  XtWX <- crossprod(X, WX)
  cov <- XtWX
  cov <- solve(cov, cbind(t(WX), diag(1, dim(XtWX)[1])))
  beta.cov <- matrix(cov[, (dim(t(WX))[2] + 1):dim(cov)[2]], 
                     dim(X)[2], dim(X)[2])
  cov <- matrix(cov[, 1:dim(t(WX))[2]], dim(X)[2], dim(X)[1])
  beta <- cov %*% y
  beta <- matrix(beta, length(beta), 1)
  row.names(beta) <- Xcolnames
  beta.se <- sqrt(abs(diag(beta.cov)))
  pos.cov <- (diag(beta.cov) < 0)
  beta.se[pos.cov] <- NA
  beta.se <- matrix(beta.se, length(beta.se), 1)
  row.names(beta.se) <- Xcolnames
  rms <- rss/rankQ
  Q <- In - X %*% cov
  
  if (identity) {
    gam <- sigma[k]
    if (SWsolveINDICATOR) {
      Sigma <- 0
      for (i in 1:k) {
        Sigma <- Sigma + V[[i]] * sigma[i]
      }
    }
  }
  
  result <- list(trace = stats,
                 llik = llik,
                 cycle = cycle, 
                 rdf = rankQ,
                 beta = beta,
                 beta.cov = beta.cov,
                 beta.se = beta.se, 
                 sigma = sigma[1:k],
                 W = W, 
                 Q = Q,
                 model = model,
                 Sigma = Sigma)
  
  class(result) <- "regress"
  return(result)
  
}

# Multi-kernel model
VC_fit <- function(fixed, random) {
  
  # Initialization
  fit <- try(regress(formula=fixed, Vformula=random))
  
  if (class(fit) == "try-error") {
    
    out <- NA
    
  } else {
    
    # Constrained fit
    fit <- try(regress(formula=fixed,
                       Vformula=random,
                       pos=rep(TRUE, length(fit$sigma)),
                       start=pmax(0, fit$sigma)))
    
    if (class(fit) == "try-error") {
      
      out <- NA
      
    } else {
      
      out <- fit
      
    }
    
  }
  
  return(out)
  
}

# Predict from VC fit
VC_pred <- function(fit, X, V_list) {
  
  require(foreach)
  
  # Prediction
  RHS <- fit$W %*% fit$Q %*% fit$model[[1]]
  
  out <- X %*% fit$beta
  
  for (k in 1:length(V_list)) {
    
    out <- out + fit$sigma[k] * V_list[[k]] %*% RHS
    
  }
  
  return(setNames(c(out), rownames(out)))
  
} 

# Generalized cross-validation, given design matrices, relationship matrices and residuals
GCV <- function(fit) {
  
  require(foreach)
  
  # Smoothing matrix
  Q <- fit$Q
  n <- nrow(Q)
  RHS <- fit$W %*% Q
  
  H <- diag(n) - Q +
    foreach(Vname=setdiff(fit$Vnames, "In"), .combine="+") %do% {
      fit$sigma[Vname] * fit$model[[Vname]] %*% RHS
    }
  
  # Mean of squared residuals
  MSE <- mean((fit$model[[1]] - fit$predicted) ^ 2)
  
  # GCV
  out <- MSE / (1 - mean(diag(H)))
  
  return(out)
  
}

# Multiplot
multiplot <- function(plots=NULL, cols=1, layout=NULL, byrow=TRUE) {
  require(grid)
  
  numPlots = length(plots)
  
  # If layout is NULL, then use 'cols' to determine layout
  if (is.null(layout)) {
    # Make the panel
    # ncol: Number of columns of plots
    # nrow: Number of rows needed, calculated from # of cols
    layout <- matrix(seq(1, cols * ceiling(numPlots/cols)),
                     ncol = cols, nrow = ceiling(numPlots/cols),
                     byrow=byrow)
  }
  
  if (numPlots==1) {
    print(plots[[1]])
    
  } else {
    # Set up the page
    grid.newpage()
    pushViewport(viewport(layout = grid.layout(nrow(layout), ncol(layout))))
    
    # Make each plot, in the correct location
    for (i in 1:numPlots) {
      # Get the i,j matrix positions of the regions that contain this subplot
      matchidx <- as.data.frame(which(layout == i, arr.ind = TRUE))
      
      print(plots[[i]], vp = viewport(layout.pos.row = matchidx$row,
                                      layout.pos.col = matchidx$col))
    }
  }
}

#########################################################
# Input
#########################################################
# Input data
input_DF <- read.csv(input_file, row.names=1, header=TRUE, stringsAsFactors=FALSE)

# Training/Testing splits
split_list <- fromJSON(file=split_file)

# Genotypic data
geno <- as.matrix(input_DF[, grep("^PC", colnames(input_DF))])
rownames(geno) <- rownames(input_DF)

# Genomic relationship matrix
G <- tcrossprod(geno)
G <- G / mean(diag(G))

gc()

# Environmental covariates
env.soil <- scale(as.matrix(input_DF[, soil_ECs]), center=TRUE, scale=scale_ECs)

env.weather <- setNames(
  foreach(weather_EC=weather_ECs) %do% {
    as.matrix(input_DF[, grep(paste0("^", weather_EC), colnames(input_DF))]) %>% t
  }, weather_ECs)

# Environmental relationship matrices
K.soil <- tcrossprod(env.soil)
K.soil <- K.soil/mean(diag(K.soil))

K.weather <- make_ERM(env.weather, w)
K.weather <- K.weather/mean(diag(K.weather))

gc()

# CNN features
if (length(cnn_ECs) > 0) {
  
  env.cnn <- scale(as.matrix(input_DF[, cnn_ECs]), center=TRUE, scale=scale_ECs)
  
  K.cnn <- tcrossprod(env.cnn)
  K.cnn <- K.cnn/mean(diag(K.cnn))
  
}

# Phenotype
y <- setNames(input_DF$yield, rownames(input_DF))

#########################################################
# GEBLUP
#########################################################
if (file.exists(CV_file) & file.exists(Ypred_file) & (! overwrite)) {
  
  CV <- read.table(CV_file, header=TRUE, stringsAsFactors=FALSE)
  Ypred <- read.table(Ypred_file, header=TRUE, stringsAsFactors=FALSE)
  
  split_names <- setdiff(names(split_list), CV$split_name)
  
} else {
  
  CV <- data.frame()
  Ypred <- data.frame()
  
  split_names <- names(split_list)
  
}

# Cross-validation by genotype and environment clusters
for (split_name in split_names) {
  
  cat("Cluster:", split_name, "\n")
  
  # Calibration set
  CS <- format(do.call(c, split_list[[split_name]][c('train', 'val')]), scientific=FALSE, trim=TRUE)
  y_CS <- y[CS]
  
  # Testing set
  TS <- format(split_list[[split_name]][["test"]], scientific=FALSE, trim=TRUE)
  y_TS <- y[TS]
  
  # GBLUP
  cat("\t", paste("GBLUP, on", Sys.time()), "\n")
  
  G_CS <- G[CS, CS]
  
  fit_G <- try(VC_fit(y_CS ~ 1, ~ G_CS))
  V.GBLUP <- fit_G$sigma
  names(V.GBLUP) <- paste0(c("V_G", "V_e"), ".GBLUP")
  
  ypred_G <- VC_pred(fit=fit_G,
                     X=matrix(1, nrow=length(TS), ncol=1),
                     V_list=list(G[TS, CS]))
  
  Ypred <- rbind(Ypred,
                 data.frame(
                   split_name=split_name,
                   method="G",
                   id=TS,
                   obs=y[TS],
                   pred=ypred_G
                 ))
  
  r_G <- ifelse(class(fit_G) == "regress",
                cor(ypred_G, y_TS, use="complete"),
                NA)
  
  rm(fit_G)
  gc()
  
  # EBLUP
  cat("\t", paste("E-BLUP, on", Sys.time()), "\n")
  
  Ks_CS <- K.soil[CS, CS]
  Kw_CS <- K.weather[CS, CS]
  
  fit_E <- try(VC_fit(y_CS ~ 1, ~ Ks_CS + Kw_CS))
  V.EBLUP <- fit_E$sigma
  names(V.EBLUP) <- paste0(c("V_S", "V_W", "V_e"), ".EBLUP")
  
  ypred_E <- VC_pred(fit=fit_E,
                     X=matrix(1, nrow=length(TS), ncol=1),
                     V_list=list(K.soil[TS, CS],
                                 K.weather[TS, CS]))
  
  Ypred <- rbind(Ypred,
                 data.frame(
                   split_name=split_name,
                   method="E",
                   id=TS,
                   obs=y[TS],
                   pred=ypred_E
                 ))
  
  r_E <- ifelse(class(fit_E) == "regress",
                cor(ypred_E, y_TS, use="complete"),
                NA)
  
  rm(fit_E)
  gc()
  
  # GEBLUP
  cat("\t", paste("GE-BLUP, on", Sys.time()), "\n")
  
  fit_GE <- try(VC_fit(y_CS ~ 1, ~ G_CS + Ks_CS + Kw_CS))
  V.GEBLUP <- fit_GE$sigma
  names(V.GEBLUP) <- paste0(c("V_G", "V_S", "V_W", "V_e"), ".GEBLUP")
  
  ypred_GE <- VC_pred(fit=fit_GE,
                      X=matrix(1, nrow=length(TS), ncol=1),
                      V_list=list(G[TS, CS],
                                  K.soil[TS, CS],
                                  K.weather[TS, CS]))
  
  Ypred <- rbind(Ypred,
                 data.frame(
                   split_name=split_name,
                   method="GE",
                   id=TS,
                   obs=y[TS],
                   pred=ypred_GE
                 ))
  
  r_GE <- ifelse(class(fit_GE) == "regress",
                 cor(ypred_GE, y_TS, use="complete"),
                 NA)
  
  rm(fit_GE)
  gc()
  
  # GxEBLUP
  cat("\t", paste("GxE-BLUP, on", Sys.time()), "\n")
  
  GKs_CS <- G_CS * Ks_CS
  GKw_CS <- G_CS * Kw_CS
  
  fit_GxE <- try(VC_fit(y_CS ~ 1, ~ G_CS + Ks_CS + GKs_CS + Kw_CS + GKw_CS))
  V.GxEBLUP <- fit_GxE$sigma
  names(V.GxEBLUP) <- paste0(c("V_G", "V_S", "V_GxS", "V_W", "V_GxW", "V_e"), ".GxEBLUP")
  
  ypred_GxE <- VC_pred(fit=fit_GxE,
                       X=matrix(1, nrow=length(TS), ncol=1),
                       V_list=list(
                         G[TS, CS],
                         K.soil[TS, CS],
                         G[TS, CS] * K.soil[TS, CS],
                         K.weather[TS, CS],
                         G[TS, CS] * K.weather[TS, CS]
                       ))
  
  Ypred <- rbind(Ypred,
                 data.frame(
                   split_name=split_name,
                   method="GxE",
                   id=TS,
                   obs=y[TS],
                   pred=ypred_GxE
                 ))
  
  r_GxE <- ifelse(class(fit_GxE) == "regress",
                  cor(ypred_GxE, y_TS, use="complete"),
                  NA)
  
  rm(fit_GxE, GKw_CS, GKs_CS, Kw_CS, Ks_CS)
  gc()
  
  # CNNBLUP
  if (length(cnn_ECs) > 0) {
    
    cat("\t", paste("CNN-BLUP, on", Sys.time()), "\n")
    
    Kc_CS <- K.cnn[CS, CS]
    GKc_CS <- G_CS * Kc_CS
    
    fit_cnn <- try(VC_fit(y_CS ~ 1, ~ G_CS + Kc_CS + GKc_CS))
    V.CNNBLUP <- fit_cnn$sigma
    names(V.CNNBLUP) <- paste0(c("V_G", "V_C", "V_GxC", "V_e"), ".CNNBLUP")
    
    ypred_CNN <- VC_pred(fit=fit_cnn,
                         X=matrix(1, nrow=length(TS), ncol=1),
                         V_list=list(G[TS, CS],
                                     K.cnn[TS, CS],
                                     G[TS, CS] * K.cnn[TS, CS]))
    
    Ypred <- rbind(Ypred,
                   data.frame(
                     split_name=split_name,
                     method="CNN",
                     id=TS,
                     obs=y[TS],
                     pred=ypred_CNN
                   ))
    
    r_CNN <- ifelse(class(fit_cnn) == "regress",
                    cor(ypred_CNN, y_TS, use="complete"),
                    NA)
    
    rm(fit_cnn, G_CS, Kc_CS, GKc_CS)
    gc()
    
    out <- cbind(
      data.frame(
        split_name=split_name,
        r_GxE=r_GxE,
        r_GE=r_GE,
        r_E=r_E,
        r_G=r_G,
        r_CNN=r_CNN,
        stringsAsFactors=FALSE
      ),
      t(c(V.GxEBLUP, V.GEBLUP, V.EBLUP, V.GBLUP, V.CNNBLUP))
    )
    
  } else {
    
    rm(G_CS)
    gc()
    
    out <- cbind(
      data.frame(
        split_name=split_name,
        r_GxE=r_GxE,
        r_GE=r_GE,
        r_E=r_E,
        r_G=r_G,
        stringsAsFactors=FALSE
      ),
      t(c(V.GxEBLUP, V.GEBLUP, V.EBLUP, V.GBLUP))
    )
    
  }
  
  # Output
  CV <- rbind(CV, out)
  write.table(CV, CV_file, row.names=FALSE, quote=FALSE)
  saveRDS(CV, sub(".txt", ".rds", CV_file))
  
  write.table(Ypred, Ypred_file, row.names=FALSE, quote=FALSE)
  saveRDS(Ypred, sub(".txt", ".rds", Ypred_file))
  
}
