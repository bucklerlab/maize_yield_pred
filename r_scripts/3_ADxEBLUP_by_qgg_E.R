# Title     : Clustering genotypes for cross-validation
# Created by: Guillaume Ramstein (gr226@cornell.edu)
# Created on: 5/30/19

#--------------------------------------------------------
# Script parameters
#--------------------------------------------------------
overwrite <- FALSE

# Working directory
setwd("~/gxe_prediction/")

# Input
input_file <- "G2F_input.csv"

VCF_file <- "G2FHybrids_RecipRem_2_22_2018_Anna_Rogers_genos.vcf"
GDS_file <- "G2FHybrids_RecipRem_2_22_2018_Anna_Rogers_genos.gds"

split_files <- c(
  "E_dwnSample293_24Apr2020"="Train_val_test_sets_E_dwnSample293_24Apr2020.json"
)

# Model parameters
scale_ECs <- TRUE

soil_ECs <- c(
  "latitude", "longitude", "altitude", "irrigated", "density",
  "clay", "sand", "silt",
  "Ca_sat", "H_sat", "K_sat", "Mg_sat", "Na_sat",
  "salts", "pH",
  "Ca", "Mg", "P", "N", "LOI", "K", "Na", "S",
  "pH_buffer",
  "N_A",
  "K_acre", "N_acre", "P_acre",
  "irrigation",
  "awc", "caco3", "cec7", "claytotal",
  "dbovendry", "ec", "gypsum", "ksat",
  "om", "ph1to1h2o", "sandtotal", "silttotal",
  "slope")

weather_ECs <- c("day_of_year",
                 "day_length",
                 "max_temp", "min_temp",
                 "precip",
                 "radn",
                 "vapor_pressure",
                 "CTT")

w <- 3

n_cores <- 20

# Output
store_ERM <- FALSE
ERM_files <- paste0("ERM_weather-w=", w, ".rds")

#--------------------------------------------------------
# Functions
#--------------------------------------------------------
# Functions
library(data.table)
library(foreach)
library(dplyr)
library(ggplot2)
library(rjson)
library(qgg)

library(SNPRelate)

# Environmental relationship matrices
make_ERM <- function(E_list, w, summary_functions=NULL) {
  
  foreach(E=E_list, .combine="+") %do% {
    
    # Time bins
    windows <- cut_interval(1:nrow(E), length=w)
    
    if (length(unique(windows)) == 1) {
      Z <- matrix(1, nrow=nrow(E), ncol=1)
    } else {
      Z <- model.matrix(~ windows)
    }
    
    # Average by time bin
    EC <- crossprod(Z, E) %>% t %>% scale %>% t %>% na.omit
    
    # Summary by time bin
    if (! is.null(summary_functions) ) {
      
      EC_summary <- foreach(summary_function=summary_functions) %do% {
        
        summary_by_window <- by(E, windows, function(x) apply(x, 2, summary_function))
        
        do.call(rbind, summary_by_window) %>% t %>% scale %>% t %>% na.omit
        
      } %>% as.list
      
      EC <- do.call(rbind, append(list(EC), EC_summary))
      
    }
    
    # Environmental relationship matrix
    return(crossprod(EC))
    
  }
  
}

# Mean imputation
mean_impute <- function(x) replace(x, is.na(x), mean(x, na.rm = TRUE))

#########################################################
# Input
#########################################################
# Input data
input_DF <- read.csv(input_file, row.names=1, header=TRUE, stringsAsFactors=FALSE)

# Genotypic data
geno <- as.matrix(input_DF[, grep("^PC", colnames(input_DF))])
rownames(geno) <- rownames(input_DF)

# Genomic relationship matrix
G <- tcrossprod(geno)
G <- G / mean(diag(G))

gc()

# Dominance relationship matrix
if (! file.exists(GDS_file)) {
  
  snpgdsVCF2GDS(VCF_file, GDS_file)
  
}

GDS <- snpgdsOpen(GDS_file)

X <- snpgdsGetGeno(GDS)
rownames(X) <- snpgdsSummary(GDS)$sample.id

Z <- apply(X*(2-X), 2, mean_impute)

D <- tcrossprod(Z)
D <- D / mean(diag(D))

D <- D[input_DF$pedigree, input_DF$pedigree]
rownames(D) <- rownames(input_DF)
colnames(D) <- rownames(input_DF)

snpgdsClose(GDS)
gc()

# Environmental covariates
env.soil <- scale(as.matrix(input_DF[, soil_ECs]), center=TRUE, scale=scale_ECs)

env.weather <- setNames(
  foreach(weather_EC=weather_ECs) %do% {
    as.matrix(input_DF[, grep(paste0("^", weather_EC), colnames(input_DF))]) %>% t
  }, weather_ECs)

# Environmental relationship matrices
K.soil <- tcrossprod(env.soil)
K.soil <- K.soil/mean(diag(K.soil))

K.weather <- make_ERM(env.weather, w)
K.weather <- K.weather/mean(diag(K.weather))

gc()

# Phenotype
y <- setNames(input_DF$yield, rownames(input_DF))

#########################################################
# Evaluations of BLUP models
#########################################################
for (analysis in names(split_files)) {
  
  cat("\n************************************\n",
      "Analysis:", analysis,
      "\n************************************\n")
  
  # File names
  split_file <- split_files[analysis]
  CV_file <- paste0("ADEBLUP-CV_results-w=", w, "-", analysis, ".txt")
  Ypred_file <- paste0("ADEBLUP-Ypred-w=", w, "-", analysis, ".txt")
  
  # Training/Testing splits
  split_list <- fromJSON(file=split_file)
  
  if (file.exists(CV_file) & file.exists(Ypred_file) & (! overwrite)) {
    
    CV <- read.table(CV_file, header=TRUE, stringsAsFactors=FALSE)
    Ypred <- read.table(Ypred_file, header=TRUE, stringsAsFactors=FALSE)
    
    split_names <- setdiff(names(split_list), CV$split_name)
    
  } else {
    
    CV <- data.frame()
    Ypred <- data.frame()
    
    split_names <- names(split_list)
    
  }
  
  # Cross-validation by genotype and environment clusters
  for (split_name in split_names) {
    
    cat("Cluster:", split_name, "\n")
    
    # Calibration set
    CS <- format(do.call(c, split_list[[split_name]][c("train", "val", "test")]), scientific=FALSE, trim=TRUE)
    y_CS <- y[CS]
    Q_CS <- matrix(1, nrow=length(CS), ncol=1)
    
    # Testing set
    TS <- format(split_list[[split_name]][["test"]], scientific=FALSE, trim=TRUE)
    y_TS <- y[TS]
    
    TS.id <- match(TS, CS)
    
    # GBLUP
    cat("\t", paste("GBLUP, on", Sys.time()), "\n")
    
    G_CS <- G[CS, CS]
    D_CS <- D[CS, CS]
    
    fit_G <- try(
      greml(y=y_CS,
            X=Q_CS,
            GRM=list(G=G_CS, D=D_CS), 
            validate=list(TS.id),
            ncores=n_cores)
    )
    
    V.GBLUP <- setNames(fit_G$theta, paste0(c("V_G", "V_D", "V_e"), ".GBLUP"))
    
    ypred_G <- fit_G$ypred
    
    Ypred <- rbind(Ypred,
                   data.frame(
                     split_name=split_name,
                     method="AD",
                     id=TS,
                     obs=y_TS,
                     pred=ypred_G
                   ))
    
    r_G <- ifelse(class(fit_G) == "list",
                  cor(ypred_G, y_TS, use="complete"),
                  NA)
    
    rm(fit_G)
    gc()
    
    # GEBLUP
    cat("\t", paste("GE-BLUP, on", Sys.time()), "\n")
    
    Ks_CS <- K.soil[CS, CS]
    Kw_CS <- K.weather[CS, CS]
    
    fit_GE <- try(
      greml(y=y_CS,
            X=Q_CS,
            GRM=list(G=G_CS, D=D_CS, S=Ks_CS, W=Kw_CS), 
            validate=list(TS.id),
            ncores=n_cores)
    )
    
    V.GEBLUP <- setNames(fit_GE$theta, paste0(c("V_G", "V_D", "V_S", "V_W", "V_e"), ".GEBLUP"))
    
    ypred_GE <- fit_GE$ypred
    
    Ypred <- rbind(Ypred,
                   data.frame(
                     split_name=split_name,
                     method="GE",
                     id=TS,
                     obs=y[TS],
                     pred=ypred_GE
                   ))
    
    r_GE <- ifelse(class(fit_GE) == "list",
                   cor(ypred_GE, y_TS, use="complete"),
                   NA)
    
    rm(fit_GE)
    gc()
    
    # GxEBLUP
    cat("\t", paste("GxE-BLUP, on", Sys.time()), "\n")
    
    GKs_CS <- G_CS * Ks_CS
    GKw_CS <- G_CS * Kw_CS
    
    DKs_CS <- D_CS * Ks_CS
    DKw_CS <- D_CS * Kw_CS
    
    fit_GxE <- try(
      greml(y=y_CS,
            X=Q_CS,
            GRM=list(G=G_CS, D=D_CS, S=Ks_CS, GxS=GKs_CS, DxS=DKs_CS, W=Kw_CS, GxW=GKw_CS, DxW=DKw_CS), 
            validate=list(TS.id),
            ncores=n_cores)
    )
    
    V.GxEBLUP <- setNames(fit_GxE$theta, paste0(c("V_G", "V_D", "V_S", "V_GxS", "V_DxS", "V_W", "V_GxW", "V_DxW", "V_e"), ".GxEBLUP"))
    
    ypred_GxE <- fit_GxE$ypred
    
    Ypred <- rbind(Ypred,
                   data.frame(
                     split_name=split_name,
                     method="GxE",
                     id=TS,
                     obs=y[TS],
                     pred=ypred_GxE
                   ))
    
    r_GxE <- ifelse(class(fit_GxE) == "list",
                    cor(ypred_GxE, y_TS, use="complete"),
                    NA)
    
    rm(fit_GxE, GKw_CS, GKs_CS, DKw_CS, DKs_CS, Kw_CS, Ks_CS, G_CS, D_CS)
    gc()
    
    out <- Reduce(cbind,
                  list(
                    data.frame(
                      split_name=split_name,
                      r_GxE=r_GxE,
                      r_GE=r_GE,
                      r_G=r_G,
                      stringsAsFactors=FALSE
                    ),
                    V.GxEBLUP,
                    V.GEBLUP,
                    V.GBLUP
                  )
    )
    
    # Output
    CV <- rbind(CV, out)
    write.table(CV, CV_file, row.names=FALSE, quote=FALSE)
    saveRDS(CV, sub(".txt", ".rds", CV_file))
    
    write.table(Ypred, Ypred_file, row.names=FALSE, quote=FALSE)
    saveRDS(Ypred, sub(".txt", ".rds", Ypred_file))
    
  }
  
}
