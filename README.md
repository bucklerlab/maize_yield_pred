#  Predicting phenotypes from genetic, environment, management, and historical data using CNNs #

If you use this repository or the ideas contained in it please kindly cite:

# Washburn, J.D., Cimen, E., Ramstein, G., Reeves, T., O�Briant, P., McLean, G., Cooper, M., Hammer, G., Buckler, E.S. Predicting phenotypes from genetic, environment, management, and historical data using CNNs. Theoretical and Applied Genetics doi://doi.org/10.1007/s00122-021-03943-7 #

### How do I get set up? ###

#  Please note: These scripts are provided for use in understanding and reproducing the results of the manuscript. They are NOT designed as a user friendly software package.

1) Install the following:

* Python 3.6.9
* Tensorflow 2.1.0
* Jupyter Notebook
* tf-keras-vis

2) Clone this repository

3) Download the data from the various sites listed in the manuscript and format by running scripts 1-4.5.  OR 
   Download the pre-formated data and untar it in place of the current "data" directory (recommended). link: https://data.cyverse.org/dav-anon/iplant/home/washjake/CNN_data_for_ms11May2021.tar.gz
   
4) Run whichever version of script 5 you would like depending on training data input desired. Comment out different sections within the scripts for different training scenarios.

5) run script 7, 7b, 7c to construct saliency maps. Script 7 can be skipped if you only want to look at previously run maps from the data download in step 3 above.

6) run scripts 8, 8b, 8c to examine the results of runs from step 4.

Please contact Jacob.Washburn@usda.gov with questions.